<?php

namespace App\Http\Controllers;

use App\Admin;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $users = User::all();
        return view('admin.index', compact('users'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $users = new User;
        $users->name = $request->name;
        $users->email = $request->email;
        $users->password = bcrypt($request->password);
        $users->save();

        return redirect('/admin')->with('success', 'Updated successfully');
    }

    public function edit($id)
    {
        $admins = Admin::findOrFail($id);
        return view('admin.edit', compact('admins'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'confirmed',
        ]);

        $admins = Admin::findOrFail($id);
        $admins->name = $request->name;
        $admins->currency = $request->currency;
        $admins->email = $request->email;
        if (filled($request->password))
        {
            $admins->password = bcrypt($request->password);
        }
        $admins->save();

        return redirect('/admin')->with('success', 'Updated successfully');

    }

    public function destroy($id)
    {
        $users = User::findOrFail($id);
        $users->delete();
        return redirect('/admin')->with('success', 'Deleted successfully');
    }

    public function useredit($id)
    {
        $users = User::findOrFail($id);
        return view('admin.edit-user', compact('users'));
    }

    public function userupdate(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'confirmed',
        ]);

        $users = User::findOrFail($id);
        $users->name = $request->name;
        $users->email = $request->email;
        if (filled($request->password))
        {
            $users->password = bcrypt($request->password);
        }
        $users->save();

        return redirect('/admin')->with('success', 'Updated successfully');

    }
}
