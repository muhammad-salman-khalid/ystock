<?php

namespace App\Http\Controllers;
use App\Models\Customer;
use App\Models\Receivable;
use App\Models\Sale;
use App\Models\SoldProducts;
use App\Models\Product;
use App\Repositories\Repository;
use App\Http\Requests\StoreName;
use Response;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class AgingController extends Controller
{
  public function aging_view($id)
  {

    $currentdata = date("Y-m-d");

    $data = DB::select(DB::raw("SELECT SUM(invoice_history.amount)as paidamount, sales.status as status, customers.name as name ,sales.id as invoiceid , sales.invoice_date as invoiceDate , sales.due_date as due_date , sales.total as total FROM sales
INNER JOIN customers ON customers.id = sales.customer_id
INNER JOIN invoice_history ON invoice_history.sales_id = sales.id
WHERE sales.customer_id = $id
GROUP BY invoice_history.sales_id"));
foreach($data as $key =>$item )
{
    $iinvoice_date = Carbon::parse($item->invoiceDate);
    $days_15 = $iinvoice_date->addDays(15);
    $data[$key]->d15 = DB::table('invoice_history')->whereBetween('created_at',array($item->invoiceDate,$iinvoice_date))->where('sales_id',$item->invoiceid)->sum('amount');
    $iinvoice_date2 = Carbon::parse($item->invoiceDate);
    $days_155 = $iinvoice_date2->addDays(15);
    $data[$key]->d30 = DB::table('invoice_history')->whereBetween('created_at',array($days_155,$iinvoice_date->addDays(15)))->where('sales_id',$item->invoiceid)->sum('amount');
}
    return view('report.aging_view',compact('data'));
  }
  public function aging_view_sup($id)
  {

    $currentdata = date("Y-m-d");

    $data = DB::select(DB::raw("SELECT SUM(purchase_history.amount)as paidamount, purchases.status as status, suppliers.name as name ,purchases.id as invoiceid , purchases.invoice_date as invoiceDate , purchases.due_date as due_date , purchases.total as total FROM purchases
  INNER JOIN suppliers ON suppliers.id = purchases.supplier_id
  INNER JOIN purchase_history ON purchase_history.purchase_id = purchases.id
  WHERE purchases.supplier_id = 1
  GROUP BY purchase_history.purchase_id"));
  foreach($data as $key =>$item )
  {
    $iinvoice_date = Carbon::parse($item->invoiceDate);
    $days_15 = $iinvoice_date->addDays(15);
    $data[$key]->d15 = DB::table('purchase_history')->whereBetween('created_at',array($item->invoiceDate,$iinvoice_date))->where('purchase_id',$item->invoiceid)->sum('amount');
    $iinvoice_date2 = Carbon::parse($item->invoiceDate);
    $days_155 = $iinvoice_date2->addDays(15);
    $data[$key]->d30 = DB::table('purchase_history')->whereBetween('created_at',array($days_155,$iinvoice_date->addDays(15)))->where('purchase_id',$item->invoiceid)->sum('amount');
  }
    return view('report.aging_view_sup',compact('data'));
  }

}
