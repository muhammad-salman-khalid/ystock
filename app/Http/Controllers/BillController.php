<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAmount;
use App\Models\Bill;
use Illuminate\Http\Request;
use App\Repositories\Repository;

class BillController extends Controller
{
    protected $model;

    public function __construct(Bill $bill)
    {
        $this->middleware('auth');
        $this->model = new Repository($bill);
    }

    public function index()
    {
        $bills = $this->model->all();
        return view('expense.index', compact('bills'));
    }

    public function store(StoreAmount $request)
    {
        $fileNameToStore = '';

        if ($request->hasFile('file')) {
            $fileNameWithExt = $request->file('file')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extenstion = $request->file('file')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extenstion;
            $path = $request->file('file')->storeAs('public/images', $fileNameToStore );
        }

        $bills = new Bill();
        $bills->amount = $request->amount;
        $bills->invoice = $request->invoice;
        $bills->file = $fileNameToStore;
        $bills->expense_id= $request->expense_id;
        $bills->vendor_id = $request->vendor_id;
        $bills->save();
        return redirect()->back()->with('success', 'Added successfully');
    }

    public function edit($id)
    {
        $bills = $this->model->find($id);
        if ($bills->expense->user_id == auth()->user()->id)
        {
            return view('bill.edit', compact('bills'));
        }
        return redirect('/home');
    }

    public function update(Request $request, $id)
    {
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        return redirect('/bill')->with('success', 'Updated successfully');
    }

    public function destroy($id)
    {
        $this->model->delete($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
}
