<?php

namespace App\Http\Controllers;
use App\Models\Category;
use Illuminate\Http\Request;
use Response;
class CategoryController extends Controller
{
      public function index(Request $request)
    {
          parse_str($request->data,$catdata);
        $category = new Category();
        $category->name = $catdata['cat_name'];
        $category->description = $catdata['cat_description'];
        $category->user_id = auth()->user()->id;
        $category->save();
      return Response::json("1");
    }

}
