<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Receivable;
use App\Models\Sale;
use App\Models\SoldProducts;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Repositories\Repository;
use App\Models\Supplier;
use App\Http\Requests\StoreName;
use Response;
use db;

class CustomerController extends Controller
{
    protected $model;

    public function __construct(Customer $customer)
    {
        $this->middleware('auth');
        $this->model = new Repository($customer);
    }

    public function index()
    {
        $customers = Customer::with(['sale', 'receivable'])->where('user_id', auth()->user()->id)->get();
        return view('customer.index', compact('customers'));
    }

    public function add(Request $request)
    {
      $customer = new Customer();
      $customer->name = $request->name;
      $customer->email = $request->email;
      $customer->address = $request->address;
      $customer->phone = $request->phone;
      $customer->user_id = auth()->user()->id;
      $customer->save();
    return Response::json("1");
    }
    public function list()
    {
      $list = Customer::all()->where('user_id', auth()->user()->id)->toarray();
        return view('customer.list', compact('list'));
    }
    public function show($id)
    {
        $sales = SoldProducts::with('product')->get();
        $receivables = Receivable::with('product')->get();
        $customers = $this->model->show($id);
        if ($customers->user_id == auth()->user()->id)
        {
            return view('customer.show', compact('customers', 'sales', 'receivables'));
        }
        return redirect('/home');
    }

    public function edit($id)
    {
        $customers = $this->model->find($id);
        if ($customers->user_id == auth()->user()->id)
        {
            return view('customer.edit', compact('customers'));
        }
        return redirect('/home');
    }

    public function update(Request $request, $id)
    {
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        return redirect('/customer-list')->with('success', 'Updated successfully');
    }

    public function destroy($id)
    {
        $this->model->delete($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
    public function bulkcustomer()
    {
        return view ('customer.bulk');
    }
    public function bulkcustomeradd(Request $request)
    {
      $pro = $request->pro['customer'];
      foreach($pro as  $value) {
        $customer = new Customer();
        $customer->name = $value['name'];
        $customer->email = $value['email'];
        $customer->address = $value['address'];
        $customer->phone = $value['phone'];
        $customer->user_id = auth()->user()->id;
        $customer->save();
      }
        return Response::json("1");
    }

public function fetchall(){

  $list['customers'] = Customer::all()->where('user_id', auth()->user()->id)->toarray();
   $prod = Product::all()->where('user_id', auth()->user()->id)->toarray();
  foreach($prod as $item){
    $products[]=$item;
  }
  $list['products']= $products;
   $sup = Supplier::all()->where('user_id', auth()->user()->id)->toarray();
$sup1=array();
  foreach($sup as $item){
    $sup1[] = $item;
  }
  $list['supplier']= $sup1;
    return Response::json($list);
   }
   public function aging_list(){
$all_data = array();
$sales = array();
 $payable = 0;
     $customers_data = DB::table('customers')->get();
     foreach($customers_data as $item){
       $all_data[$item->id]['name'] = $item->name;
       $all_data[$item->id]['cusid'] = $item->id;
       $sales_data = DB::table('sales')->where('customer_id',$item->id)->get();
       $sales_data_total = 0;
       $payable=0;
       foreach($sales_data as $item2){
         // print_r($sales_data);exit;
           $sales_data_total += $item2->total;
           $invoice_history_data = DB::table('invoice_history')->where('sales_id',$item2->id)->get();

           foreach($invoice_history_data as $item3){
                $payable += $item3->amount;
           }
           $all_data[$item->id]['payable'] = $payable;

       }
$all_data[$item->id]['total'] = $sales_data_total;


       $amount_total= DB::table('sales')->selectRaw('sum(total) as total')->where('customer_id',$item->id)->first();

     }
     // print_r($all_data);exit;
     $new_data = array();
     $i = 0;
     foreach($all_data as $key =>$item ){
        if($item['total']!= 0){
          $temp['id'] = $key;
          $temp['cusid'] = $item['cusid'];
          $temp['name'] = $item['name'];
          $temp['total'] = $item['total'];

            if($item['payable'] == 0){
              // $new_data[$i]['payable'] = 0;
              $temp['payable'] = 0;
            }else{
              // $new_data[$i]['payable'] = $item['payable'];
              $temp['payable']  = $item['payable'];
            }
            // $new_data[$i]['total'] = $item['total'];
            $sales[] = (object)$temp;
            $i++;
        }

     }
     // $sales = (object)$new_data;
     // print_r($sales);exit;

        return view('report.aging_list', compact('sales'));

      }
}
