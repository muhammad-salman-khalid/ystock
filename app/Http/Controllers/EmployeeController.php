<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Payroll;
use Illuminate\Http\Request;
use App\Repositories\Repository;
use App\Http\Requests\StoreName;

class EmployeeController extends Controller
{
    protected $model;

    public function __construct(Employee $employee)
    {
        $this->middleware('auth');
        $this->model = new Repository($employee);
    }

    public function index()
    {
        $payrolls = Payroll::with('employee')->get();
        $employees = Employee::with(['payroll'])->where('user_id', auth()->user()->id)->get();
        return view('employee.index', compact('employees', 'payrolls'));
    }

    public function store(StoreName $request)
    {
        $this->model->create($request->only($this->model->getModel()->fillable));
        return redirect()->back()->with('success', 'Added successfully');
    }

    public function show($id)
    {
        $payrolls = Payroll::all();
        $employees = $this->model->show($id);
        if ($employees->user_id == auth()->user()->id)
        {
            return view('employee.show', compact('employees', 'payrolls'));
        }
        return redirect('/home');
    }

    public function edit($id)
    {
        $employees = $this->model->find($id);
        if ($employees->user_id == auth()->user()->id)
        {
            return view('employee.edit', compact('employees'));
        }
        return redirect('/home');
    }

    public function update(Request $request, $id)
    {
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        return redirect('/employee')->with('success', 'Updated successfully');
    }

    public function destroy($id)
    {
        $this->model->delete($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
}
