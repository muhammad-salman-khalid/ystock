<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Models\Expense;
use App\Models\VendorPayable;
use Illuminate\Http\Request;
use App\Repositories\Repository;
use App\Http\Requests\StoreName;
use Response;

class ExpenseController extends Controller
{
    protected $model;

    public function __construct(Expense $expense)
    {
        $this->middleware('auth');
        $this->model = new Repository($expense);
    }

    public function index()
    {
        $bills = Bill::with(['expense', 'vendor'])->get();
        $expenses = Expense::with(['bill', 'vendorpayable'])->where('user_id', auth()->user()->id)->get();
        return view('expense.index', compact('expenses', 'bills'));
    }

    public function store(StoreName $request)
    {
        $this->model->create($request->only($this->model->getModel()->fillable));
        return redirect()->back()->with('success', 'Added successfully');
    }

    public function show($id)
    {
        $bills = Bill::with('vendor')->get();
        $vendorpayables = VendorPayable::with('vendor')->get();
        $ranks = Bill::with('expense')
            ->groupBy('expense_id')
            ->selectRaw('SUM(amount) AS amount, expense_id')
            ->orderBy('amount', 'desc')
            ->get();
        $expenses = $this->model->show($id);
        if ($expenses->user_id == auth()->user()->id)
        {
            return view('expense.show', compact('expenses', 'bills', 'ranks', 'vendorpayables'));
        }
        return redirect('/home');
    }

    public function edit($id)
    {
        $expenses = $this->model->find($id);
        if ($expenses->user_id == auth()->user()->id)
        {
            return view('expense.edit', compact('expenses'));
        }
        return redirect('/home');
    }

    public function update(Request $request, $id)
    {
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        return redirect('/expense')->with('success', 'Updated successfully');
    }

    public function destroy($id)
    {
        $this->model->delete($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
    public function getexpensecategory(){
        $expense = \App\Models\Category::all();
        return $expense;
    }
    public function expenseform(Request $request){
        parse_str($request->data,$expdata);
        
        $expense = new Expense();        
        $expense->name = $expdata['name'];
        $expense->category_id= $expdata['category_id'];
        $expense->description = $expdata['description'];
        $expense->exp_amount= $expdata['expense-amount'];
        $expense->date= $expdata['expense_date'];
        
        $expense->user_id = auth()->user()->id;
        $expense->save();
      return Response::json("1");
    }
}
