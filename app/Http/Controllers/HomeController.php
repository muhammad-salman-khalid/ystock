<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Employee;
use App\Models\Product;
use App\Models\Purchase;
use App\Models\PurchaseProducts;
use App\Models\Sale;
use App\Models\SoldProducts;
use App\Models\Supplier;
use App\Models\Vendor;
use DB;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $all_purchases = DB::table('products')
            ->where('user_id', auth()->user()->id)
            ->join('purchaseproducts', 'purchaseproducts.product_id', '=', 'products.id')
            ->select(DB::raw('SUM(purchaseproducts.qty * purchaseproducts.rate) AS purchase'))
            ->get();

        $all_sales = DB::table('products')
            ->where('user_id', auth()->user()->id)
//            ->join('sales', 'sales.product_id', '=', 'products.id')
            ->join('soldProducts', 'soldProducts.product_id', '=', 'products.id')
            ->select(DB::raw('SUM(soldProducts.qty * soldProducts.rate) AS sale'))
            ->get();

        $expenses = DB::table('expenses')
            ->where('user_id', auth()->user()->id)
            ->join('bills', 'bills.expense_id', '=', 'expenses.id')
            ->select(DB::raw('SUM(bills.amount) AS expense'))
            ->get();

        $payrolls = DB::table('employees')
            ->where('user_id', auth()->user()->id)
            ->join('payrolls', 'payrolls.employee_id', '=', 'employees.id')
            ->select(DB::raw('SUM(payrolls.basic + payrolls.allowance - payrolls.deduction) AS payroll'))
            ->get();

        // $graph =  DB::table('products')
        //     ->where('user_id', auth()->user()->id)
        //     ->join('soldProducts', 'soldProducts.product_id', '=', 'products.id')
        //     ->join('purchases', 'purchases.product_id', '=', 'products.id')
        //     ->select(DB::raw('MONTH(soldProducts.created_at) AS month, YEAR(soldProducts.created_at) as year, SUM(soldProducts.qty * soldProducts.rate) AS sale, SUM(purchases.quantity * purchases.rate) AS purchase'))
        //     ->groupBy('year', 'month')
        //     ->get();

        $receivables = DB::table('customers')
            ->where('user_id', auth()->user()->id)
            ->join('receivables', 'receivables.customer_id', '=', 'customers.id')
            ->select(DB::raw('SUM(receivables.amount) AS receivable'))
            ->get();

//        print_r($receivables);exit;
        $payables_supplier = DB::table('suppliers')
            ->where('user_id', auth()->user()->id)
            ->join('payables', 'payables.supplier_id', '=', 'suppliers.id')
            ->select(DB::raw('SUM(payables.amount) AS amount'))
            ->get();

        $payables_vendor = DB::table('vendors')
            ->where('user_id', auth()->user()->id)
            ->join('vendor_payables', 'vendor_payables.vendor_id', '=', 'vendors.id')
            ->select(DB::raw('SUM(vendor_payables.amount) AS amount'))
            ->get();

        $products = Product::where('user_id', auth()->user()->id)->count('name');
        $purchases = PurchaseProducts::with('product')->orderBy('created_at', 'desc')->take(5)->get();
        $sales = SoldProducts::with('product')->orderBy('created_at', 'desc')->take(5)->get();
//        $sales = DB::table('soldProducts')->orderBy('created_at', 'desc')->take(5)->get();

        $employees = Employee::where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->take(5)->get();

        return view('home', compact('all_purchases', 'all_sales',
            'expenses', 'payrolls', 'graph', 'products', 'purchases', 'sales',
            'customers', 'suppliers', 'vendors', 'employees', 'receivables',
            'payables_supplier', 'payables_vendor'));
    }
}
