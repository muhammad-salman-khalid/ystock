<?php

namespace App\Http\Controllers;
use db;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
  public function index()
  {

    $data = DB::select(DB::raw("SELECT name , id , stock from products"));
    return view('inventory.index',compact('data'));
  }
}
