<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\Purchase;
use App\Models\Sale;
use App\Models\SoldProducts;
use Illuminate\Http\Request;
use App\Repositories\Repository;
use App\Http\Requests\StoreName;
use DB;
use Response;

class InvoiceController extends Controller
{
  public function viewinvoice($id)
  {
    $invoice_data = [];
    $invoice_data['invoice_details'] = DB::table('sales')
    ->join('customers','sales.customer_id', '=','customers.id')
    ->select('customers.phone as phone','customers.address as address','customers.name as cname','sales.total as total','sales.term as term','sales.invoice_date as inv_date','sales.due_date as inv_due','sales.invoice_message as message','sales.statement_message as statement')
    ->whereRaw("sales.id =". $id )
    ->get();
    $invoice_data['invoice_products'] = DB::table('soldproducts')
    ->join('sales','sales.id','=','soldproducts.sales_id')
    ->join('products','products.id','=','soldproducts.product_id')
    ->select('products.name as P_name','soldproducts.qty as qty','soldproducts.rate as rate','soldproducts.description as desc','soldproducts.amount as amount')
    ->whereRaw("soldproducts.sales_id =". $id )
    ->get();
        return view('invoicep.preview', compact('invoice_data'));
  }
}
