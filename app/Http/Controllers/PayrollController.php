<?php

namespace App\Http\Controllers;

use App\Models\Payroll;
use Illuminate\Http\Request;
use App\Repositories\Repository;

class PayrollController extends Controller
{
    protected $model;

    public function __construct(Payroll $payroll)
    {
        $this->middleware('auth');
        $this->model = new Repository($payroll);
    }

    public function store(Request $request)
    {
        foreach ($request->employee_id as $key => $val) {
            if (is_array($request->checked) && in_array($val, $request->checked)) {
                $this->model->create([
                'basic' => $request->basic[$key],
                'allowance' => $request->allowance[$key] ? : 0,
                'deduction' => $request->deduction[$key] ? : 0,
                'employee_id' => $request->employee_id[$key],
                ]);
            }
        }
        return redirect()->back()->with('success', 'Added successfully');
    }

    public function edit($id)
    {
        $payrolls = $this->model->find($id);
        if ($payrolls->employee->user_id == auth()->user()->id)
        {
            return view('payroll.edit', compact('payrolls'));
        }
        return redirect('/home');
    }

    public function update(Request $request, $id)
    {
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        return redirect('/employee')->with('success', 'Updated successfully');
    }

    public function destroy($id)
    {
        $this->model->delete($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
}
