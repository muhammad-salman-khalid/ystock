<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\PurchaseProducts;
use App\Models\Sale;
use App\Models\SoldProducts;
use Illuminate\Http\Request;
use App\Repositories\Repository;
use App\Http\Requests\StoreName;
use Response;



class ProductController extends Controller
{
    protected $model;

    public function __construct(Product $product)
    {
        $this->middleware('auth');
        $this->model = new Repository($product);
    }

    public function index()
    {
        $products = Product::with(['purchase', 'sale'])->where('user_id', auth()->user()->id)->get();
        return view('product.index', compact('products'));
    }
    public function list()
    {
        $list = Product::all()->where('user_id', auth()->user()->id)->toarray();

        return view('product.list', compact('list'));
    }



    public function add(Request $request)
    {
        $product = new Product();
        $product->name = $request->name;
        $product->description = $request->description;
        $product->costprice = $request->costprice;
        $product->sellingprice = $request->sellingprice;
        $product->category_id = $request->category_id;
        $product->user_id = auth()->user()->id;
        $product->save();
      return Response::json("1");
    }

    public function show($id)
    {
        $purchases = PurchaseProducts::with('supplier')->get();
        $sales = SoldProducts::with(['customer'])->get();
//        print_r($sales);exit;
        $ranks = SoldProducts::with('product')
            ->groupBy('product_id')
            ->selectRaw('SUM(qty) AS quantity, product_id')
            ->orderBy('qty', 'desc')
            ->get();
        $products = $this->model->show($id);
        if ($products->user_id == auth()->user()->id)
        {
            return view('product.show', compact('products', 'purchases', 'sales', 'ranks'));
        }
        return redirect('/home');
    }

    public function edit($id)
    {
        $products = $this->model->find($id);
        if ($products->user_id == auth()->user()->id)
        {
            return view('product.edit', compact('products'));
        }
        return redirect('/product-list');
    }


    public function update(Request $request, $id)
    {
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        return redirect('/product-list')->with('success', 'Updated successfully');
    }

    public function destroy($id)
    {
        $this->model->delete($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
    public function import()
    {

        return view('product.bulk');
    }
    public function boot()
    {

        return view('product.boot');
    }
    public function savebulk(Request $request ){
      $pro = $request->pro['products'];
      foreach($pro as  $value) {
        $product = new Product();
        $product->name = $value['name'];
        $product->sellingprice = $value['sellingprice'];
        $product->costprice = $value['costprice'];
        $product->description = $value['description'];
        $product->user_id = auth()->user()->id;
        $product->save();
      }
    return Response::json("1");

}
}
