<?php
namespace App\Http\Controllers;

use App\Http\Requests\StoreRate;
use App\Models\Purchase;
use App\Models\PurchaseProducts;
use App\Models\AmountPurchase;
use Illuminate\Http\Request;
use App\Repositories\Repository;
use db;

class PurchaseController extends Controller
{
    protected $model;

    public function __construct(Purchase $purchase)
    {
        $this->middleware('auth');
        $this->model = new Repository($purchase);
    }

    public function index()
    {
      {
  $purchase = DB::select(DB::raw("SELECT sum(purchase_history.amount) as amount , purchases.invoice_message as invoice_message, purchases.id as id , purchases.status as status, purchase_history.amount as paidamount,
  suppliers.name as name , purchases.status as status ,purchases.id as invoiceid , purchases.total as total , purchases.invoice_date as invoiceDate, purchases.due_date as due_date from purchases
  LEFT OUTER JOIN purchase_history ON purchases.id = purchase_history.purchase_id
  LEFT OUTER JOIN suppliers ON suppliers.id = purchases.supplier_id
  GROUP BY purchases.id"));
          return view('purchase.index', compact('purchase'));
      }
    }

    public function store(StoreRate $request)
    {
        $fileNameToStore = '';

        if ($request->hasFile('file')) {
            $fileNameWithExt = $request->file('file')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extenstion = $request->file('file')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extenstion;
            $path = $request->file('file')->storeAs('public/images', $fileNameToStore );
        }

        $purchases = new Purchase;
        $purchases->rate = $request->rate;
        $purchases->quantity = $request->quantity;
        $purchases->invoice = $request->invoice;
        $purchases->note = $request->note;
        $purchases->file = $fileNameToStore;
        $purchases->product_id = $request->product_id;
        $purchases->supplier_id = $request->supplier_id;
        $purchases->save();
        return redirect()->back()->with('success', 'Added successfully');
    }
    public function purchase_save(Request $request){
        $images = array();
        // print_r($request->allFiles());exit;
            foreach($request->allFiles() as $key => $file){
                if($request->hasFile($key)){
                    $file_name = $request->file($key)->getClientOriginalName();
                        $uploadedFile = $request->file($key);
                    if ($uploadedFile->isValid()){
                        $uploadedFile->move('public/', $file_name );
                        $images[] = $file_name;
                    }
                }
            }
            $form_data = json_decode($request->form_data);
//print_r($form_data);exit;
        $purchase = new Purchase();
        $purchase->supplier_id = $form_data->supplier_id;
        $purchase->total = $form_data->total;
        $purchase->term = $form_data->term;
        $purchase->invoice_date = $form_data->invoice_date;
        $purchase->due_date = $form_data->due_date;
        $purchase->payment_method = 0;
        $purchase->invoice_message  = $form_data->invoice_message ;
        $purchase->statement_message  = $form_data->statement_message ;
        $purchase->files = implode(',',$images);
//        $purchase->product_id = $request->product_id;
      if($form_data->payment_method == 1 ){
        $purchase->status = 2;
      }
      else{
        $purchase->status = 1;
      }
        $inserted_id = $purchase->save();

          $purchase1 = new AmountPurchase();
          if($form_data->payment_method != 0){
          if($form_data->payment_method == 1){
              $form_data->amount_paid = $form_data->total;
          }
          else{
              $form_data->amount_paid = $form_data->amount_paid;
          }
              $purchase1->purchase_id = $purchase->id;
              $purchase1->amount = $form_data->amount_paid;
              $purchase1->user_id = auth()->user()->id;
              // print_r($purchase1);exit;
                $purchase1->save();
        }
        $purchaseData = array();
        $purchaseProduct = new PurchaseProducts();
//        $i = 0 ;
            foreach($form_data->products as $key => $value){

                $purchaseProduct = new PurchaseProducts();
                $purchaseProduct->description = $value->description;
                $purchaseProduct->qty = $value->qty;
                $purchaseProduct->rate = $value->rate;
                $purchaseProduct->supplier_id = $form_data->supplier_id;
                $purchaseProduct->amount = $value->amount;
                $purchaseProduct->product_id = $value->id;
                $purchaseProduct->purchases_id = $purchase->id;
                $purchaseProduct->save();
                $productstock = DB::select(DB::raw("SELECT id , stock FROM products WHERE id = $value->id"));
                $updatestock = intVal($productstock[0]->stock) + intVal($value->qty);
                DB::select(DB::raw("UPDATE products SET stock = $updatestock WHERE id = $value->id"));
  }
            $obj = array();
            $obj['staus'] ='ok';
         return json_encode($obj);
    }
    public function edit($id)
    {
        $purchases = $this->model->find($id);
        if ($purchases->product->user_id == auth()->user()->id)
        {
            return view('purchase.edit', compact('purchases'));
        }
        return redirect('/home');
    }
    public function update(Request $request, $id)
    {
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        return redirect('/purchase')->with('success', 'Updated successfully');
    }
    public function destroy($id)
    {
        $this->model->delete($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
    public function paymentadd(Request $request){


              $formdata =array();
          parse_str($request->data,$formdata);
          $check = Purchase:: where('id',$formdata['invoiceid'])->first();
          if($check->status == 1){
          $amountaddd = new AmountPurchase();
        $amountaddd->purchase_id = $formdata['invoiceid'];
        $amountaddd->amount = $formdata['add_amount'];
        $amountaddd->user_id = $formdata['user_id'];
          $amountaddd->save();
          $status= DB::table('purchase_history')->selectRaw('sum(amount) as amountt')->where('purchase_id',$formdata['invoiceid'])->first();
          // print_r();exit;
          if(( $check->total - $status->amountt) == 0){
            $sales = DB::table('purchases')->where('id', $formdata['invoiceid'])->update(array('status'=> 2));
          }
          $result['status']= 'ok';
          return $result;
        }
        else {
          $result['status']= 'no';
          return $result;
        }
    }
}
