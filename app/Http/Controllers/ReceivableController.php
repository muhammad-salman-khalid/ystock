<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAmount;
use App\Models\Receivable;
use Illuminate\Http\Request;
use App\Repositories\Repository;

class ReceivableController extends Controller
{
    protected $model;

    public function __construct(Receivable $receivable)
    {
        $this->middleware('auth');
        $this->model = new Repository($receivable);
    }

    public function store(StoreAmount $request)
    {
        $this->model->create($request->only($this->model->getModel()->fillable));
        return redirect()->back()->with('success', 'Added successfully');
    }

    public function edit($id)
    {
        $receivables = $this->model->find($id);
        if ($receivables->product->user_id == auth()->user()->id)
        {
            return view('receivable.edit', compact('receivables'));
        }
        return redirect('/home');
    }

    public function update(Request $request, $id)
    {
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        return redirect('/customer')->with('success', 'Updated successfully');
    }

    public function destroy($id)
    {
        $this->model->delete($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
}
