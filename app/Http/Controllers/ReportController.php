<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Product;
use App\Models\Supplier;
use App\Models\SoldProducts;
use App\Models\Vendor;
use DB;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('report.index');
    }

    public function profit()
    {
        $purchases = DB::table('products')
            ->where('user_id', auth()->user()->id)
            ->join('purchases', 'purchases.product_id', '=', 'products.id')
            ->select(DB::raw('SUM(purchases.quantity * purchases.rate) AS purchase'))
            ->get();

        $sales = DB::table('products')
            ->where('user_id', auth()->user()->id)
            ->join('soldproducts', 'soldproducts.product_id', '=', 'products.id')
            ->select(DB::raw('SUM(soldproducts.qty * soldproducts.rate) AS sale'))
            ->get();

        $expenses = DB::table('expenses')
            ->where('user_id', auth()->user()->id)
            ->join('bills', 'bills.expense_id', '=', 'expenses.id')
            ->select(DB::raw('SUM(bills.amount) AS expense'))
            ->get();

        $payrolls = DB::table('employees')
            ->where('user_id', auth()->user()->id)
            ->join('payrolls', 'payrolls.employee_id', '=', 'employees.id')
            ->select(DB::raw('SUM(payrolls.basic + payrolls.allowance - payrolls.deduction) AS payroll'))
            ->get();

        return view('report.profit', compact('purchases', 'sales', 'expenses', 'payrolls'));
    }

    public function monthlyReport()
    {
        $purchase = DB::table('products')
            ->where('user_id', auth()->user()->id)
            ->join('purchases', 'purchases.product_id', '=', 'products.id')
            ->select(DB::raw('MONTHNAME(purchases.created_at) AS month,
                MONTH(purchases.created_at) AS m,
                YEAR(purchases.created_at) AS year'))
            ->groupBy('month', 'year')
            ->get();

        $sale = DB::table('products')
            ->where('user_id', auth()->user()->id)
            ->join('soldproducts', 'soldproducts.product_id', '=', 'products.id')
            ->select(DB::raw('MONTHNAME(soldproducts.created_at) AS month,
                MONTH(soldproducts.created_at) AS m,
                YEAR(soldproducts.created_at) AS year'))
            ->groupBy('month', 'year')
            ->get();

        $expense = DB::table('expenses')
            ->where('user_id', auth()->user()->id)
            ->join('bills', 'bills.expense_id', '=', 'expenses.id')
            ->select(DB::raw('MONTHNAME(bills.created_at) AS month,
                MONTH(bills.created_at) AS m,
                YEAR(bills.created_at) AS year'))
            ->groupBy('month', 'year')
            ->get();

        $payroll = DB::table('employees')
            ->where('user_id', auth()->user()->id)
            ->join('payrolls', 'payrolls.employee_id', '=', 'employees.id')
            ->select(DB::raw('MONTHNAME(payrolls.created_at) AS month,
                MONTH(payrolls.created_at) AS m,
                YEAR(payrolls.created_at) AS year'))
            ->groupBy('month', 'year')
            ->get();

        $allMonths = $purchase->concat($sale)->concat($expense)->concat($payroll)->unique()->sortBy('m')->sortByDesc('year');

        $purchases = DB::table('products')
            ->where('user_id', auth()->user()->id)
            ->join('purchases', 'purchases.product_id', '=', 'products.id')
            ->select(DB::raw('MONTHNAME(purchases.created_at) AS month,
                YEAR(purchases.created_at) AS year,
                SUM(purchases.rate * purchases.quantity) AS amount'))
            ->groupBy('month', 'year')
            ->get();

        $sales = DB::table('products')
            ->where('user_id', auth()->user()->id)
            ->join('soldproducts', 'soldproducts.product_id', '=', 'products.id')
            ->select(DB::raw('MONTHNAME(soldproducts.created_at) AS month,
                YEAR(soldproducts.created_at) AS year,
                SUM(soldproducts.rate * soldproducts.qty) AS amount'))
            ->groupBy('month', 'year')
            ->get();

        $expenses = DB::table('expenses')
            ->where('user_id', auth()->user()->id)
            ->join('bills', 'bills.expense_id', '=', 'expenses.id')
            ->select(DB::raw('MONTHNAME(bills.created_at) AS month,
                YEAR(bills.created_at) AS year,
                SUM(bills.amount) AS amount'))
            ->groupBy('month', 'year')
            ->get();

        $payrolls = DB::table('employees')
            ->where('user_id', auth()->user()->id)
            ->join('payrolls', 'payrolls.employee_id', '=', 'employees.id')
            ->select(DB::raw('MONTHNAME(payrolls.created_at) AS month,
                YEAR(payrolls.created_at) AS year,
                SUM(payrolls.basic + payrolls.allowance - payrolls.deduction) AS amount'))
            ->groupBy('month', 'year')
            ->get();

        return view('report.monthly-report', compact('purchases', 'sales', 'expenses', 'payrolls', 'allMonths'));
    }

    public function productReport()
    {
        $all_purchases = DB::table('products')
            ->where('user_id', auth()->user()->id)
            ->join('purchases', 'purchases.product_id', '=', 'products.id')
            ->select(DB::raw('SUM(purchases.quantity * purchases.rate) AS purchase, products.id AS product'))
            ->groupBy('product')
            ->get();

        $all_sales = DB::table('products')
            ->where('user_id', auth()->user()->id)
            ->join('soldproducts', 'soldproducts.product_id', '=', 'products.id')
            ->select(DB::raw('SUM(soldproducts.qty * soldproducts.rate) AS sale, products.id AS product'))
            ->groupBy('product')
            ->get();

        $products = Product::where('user_id', auth()->user()->id)->get();

        return view('report.product-report', compact('profits', 'all_purchases', 'all_sales', 'products'));
    }

    public function customerReview()
    {
        $profits = DB::table('customers')
            ->where('user_id', auth()->user()->id)
            ->join('soldproducts', 'soldproducts.customer_id', '=', 'customers.id')
            ->select(DB::raw('MONTHNAME(soldproducts.created_at) AS month,
                YEAR(soldproducts.created_at) AS year,
                SUM(soldproducts.rate * soldproducts.qty) AS amount,
                customers.name AS customer'))
            ->groupBy('month', 'year', 'customer')
            ->get();

        $customers = Customer::with('sale')->where('user_id', auth()->user()->id)->orderBy('name')->get();

        return view('report.customer-review', compact('profits', 'customers'));
    }

    public function supplierReview()
    {
        $profits = DB::table('suppliers')
            ->where('user_id', auth()->user()->id)
            ->join('purchases', 'purchases.supplier_id', '=', 'suppliers.id')
            ->select(DB::raw('MONTHNAME(purchases.created_at) AS month,
                YEAR(purchases.created_at) AS year,
                SUM(purchases.rate * purchases.quantity) AS amount,
                suppliers.name AS supplier'))
            ->groupBy('month', 'year', 'supplier')
            ->get();

        $suppliers = Supplier::with('purchase')->where('user_id', auth()->user()->id)->orderBy('name')->get();

        return view('report.supplier-review', compact('profits', 'suppliers'));
    }

    public function vendorReview()
    {
        $profits = DB::table('vendors')
            ->where('user_id', auth()->user()->id)
            ->join('bills', 'bills.vendor_id', '=', 'vendors.id')
            ->select(DB::raw('MONTHNAME(bills.created_at) AS month,
                YEAR(bills.created_at) AS year,
                SUM(bills.amount) AS amount,
                vendors.name AS vendor'))
            ->groupBy('month', 'year', 'vendor')
            ->get();

        $vendors = Vendor::with('bill')->where('user_id', auth()->user()->id)->orderBy('name')->get();

        return view('report.vendor-review', compact('profits', 'vendors'));
    }
}
