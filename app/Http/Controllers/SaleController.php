<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRate;
use App\Models\Sale;
use App\Models\Amount;
use App\Models\SoldProducts;
use Illuminate\Http\Request;
use App\Repositories\Repository;
use db;

class SaleController extends Controller
{
    protected $model;

    public function __construct(Sale $sale)
    {
        $this->middleware('auth');
        $this->model = new Repository($sale);
    }

    public function index()
    {
$sales = DB::select(DB::raw("SELECT sum(invoice_history.amount) as amount , sales.invoice_message as invoice_message, sales.id as id , sales.status as status, invoice_history.amount as paidamount,
customers.name as name , sales.status as status ,sales.id as invoiceid , sales.total as total , sales.invoice_date as invoiceDate, sales.due_date as due_date from sales
LEFT OUTER JOIN invoice_history ON sales.id = invoice_history.sales_id
LEFT OUTER JOIN customers ON customers.id = sales.customer_id
GROUP BY sales.id"));
        return view('sale.index', compact('sales'));
    }

    public function store(StoreRate $request)
    {
        $fileNameToStore = '';

        if ($request->hasFile('file')) {
            $fileNameWithExt = $request->file('file')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extenstion = $request->file('file')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extenstion;
            $path = $request->file('file')->storeAs('public/images', $fileNameToStore );
        }

        $sales = new Sale();
        $sales->rate = $request->rate;
        $sales->quantity = $request->quantity;
        $sales->invoice = $request->invoice;
        $sales->note = $request->note;
        $sales->file = $fileNameToStore;
        $sales->product_id = $request->product_id;
        $sales->customer_id = $request->customer_id;

        $inserted_id = $sales->save();
        return redirect()->back()->with('success', 'Added successfully');
    }
    public function sale_save(Request $request){
        $images = array();
            foreach($request->allFiles() as $key => $file){
                if($request->hasFile($key)){
                    $file_name = $request->file($key)->getClientOriginalName();
                        $uploadedFile = $request->file($key);
                    if ($uploadedFile->isValid()){
                        $uploadedFile->move('public/', $file_name );
                        $images[] = $file_name;
                    }
                }
            }
            $form_data = json_decode($request->form_data);

        $sales = new Sale();
        $sales->customer_id = $form_data->customer_id;
        $sales->total = $form_data->total;
        $sales->term = $form_data->term;
        $sales->invoice_date = $form_data->invoice_date;
        $sales->due_date = $form_data->due_date;
        $sales->payment_method = $form_data->payment_method;
        $sales->invoice_message  = $form_data->invoice_message ;
        $sales->statement_message  = $form_data->statement_message ;
        $sales->files = implode(',',$images);
//        $sales->product_id = $request->product_id;
      if($form_data->payment_method == 1 ){
        $sales->status = 2;
      }
      else{
        $sales->status = 1;
      }
        $inserted_id = $sales->save();
        if($form_data->payment_method != 0){
          $amount = new Amount();
          if($form_data->payment_method == 1){

              $form_data->amount_paid = $form_data->total;
          }
          else{
              $form_data->amount_paid = $form_data->amount_paid;
          }

              $amount->sales_id = $sales->id;
              $amount->amount = $form_data->amount_paid;
              $amount->user_id = auth()->user()->id;
                $amount->save();
        }
        $soldData = array();
        $soldProduct = new SoldProducts();
//        $i = 0 ;
            foreach($form_data->products as $key => $value){
                $soldProduct = new SoldProducts();
                $soldProduct->description = $value->description;
                $soldProduct->qty = $value->qty;
                $soldProduct->rate = $value->rate;
                $soldProduct->customer_id = $form_data->customer_id;
                $soldProduct->amount = $value->amount;
                $soldProduct->product_id = $value->id;
                $soldProduct->sales_id = $sales->id;
                $soldProduct->save();

            }

            $obj = array();
            $obj['staus'] ='ok';

         return json_encode($obj);
    }
    public function edit($id)
    {
        $sales = $this->model->find($id);
        if ($sales->product->user_id == auth()->user()->id)
        {
            return view('sale.edit', compact('sales'));
        }
        return redirect('/home');
    }

    public function update(Request $request, $id)
    {
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        return redirect('/sale')->with('success', 'Updated successfully');
    }

    public function destroy($id)
    {
        $this->model->delete($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
    public function paymentadd(Request $request){


              $formdata =array();
          parse_str($request->data,$formdata);
          $check = Sale:: where('id',$formdata['invoiceid'])->first();
          if($check->status == 1){
          $amountaddd = new Amount();
        $amountaddd->sales_id = $formdata['invoiceid'];
        $amountaddd->amount = $formdata['add_amount'];
        $amountaddd->user_id = $formdata['user_id'];
          $amountaddd->save();
          $status= DB::table('invoice_history')->selectRaw('sum(amount) as amountt')->where('sales_id',$formdata['invoiceid'])->first();
          // print_r();exit;
          if(( $check->total - $status->amountt) == 0){
            $sales = DB::table('sales')->where('id', $formdata['invoiceid'])->update(array('status'=> 2));
          }
          $result['status']= 'ok';
          return $result;
        }
        else {
          $result['status']= 'no';
          return $result;
        }
    }
}
