<?php

namespace App\Http\Controllers;

use App\Models\Payable;
use App\Models\Purchase;
use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Repositories\Repository;
use App\Http\Requests\StoreName;
use Response;
use db;

class SupplierController extends Controller
{
    protected $model;

    public function __construct(Supplier $supplier)
    {
        $this->middleware('auth');
        $this->model = new Repository($supplier);
    }

    public function index()
    {
        $suppliers = Supplier::with(['purchase', 'payable'])->where('user_id', auth()->user()->id)->get();
        return view('supplier.index', compact('suppliers'));
    }
    public function list()
    {

      $list = Supplier::all()->where('user_id', auth()->user()->id)->toarray();
        return view('supplier.list', compact('list'));

    }

    public function store(StoreName $request)
    {
        $this->model->create($request->only($this->model->getModel()->fillable));
        return redirect()->back()->with('success', 'Added successfully');
    }

    public function show($id)
    {
        $purchases = Purchase::with('product')->get();
        $payables = Payable::with('product')->get();
        $suppliers = $this->model->show($id);
        if ($suppliers->user_id == auth()->user()->id)
        {
            return view('supplier.show', compact('suppliers', 'purchases', 'payables'));
        }
        return redirect('/home');
    }

    public function edit($id)
    {
        $suppliers = $this->model->find($id);
        if ($suppliers->user_id == auth()->user()->id)
        {
            return view('supplier.edit', compact('suppliers'));
        }
        return redirect('/home');
    }

    public function update(Request $request, $id)
    {
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        return redirect('/supplier')->with('success', 'Updated successfully');
    }

    public function destroy($id)
    {
        $this->model->delete($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
    public function showbulk()
    {
      return view('supplier.bulk');
    }
    public function addbulk(Request $request)
    {
      $pro = $request->pro['supplier'];
      foreach($pro as  $value) {
        $Supplier = new Supplier();
        $Supplier->name = $value['name'];
        $Supplier->email = $value['email'];
        $Supplier->address = $value['address'];
        $Supplier->phone = $value['phone'];
        $Supplier->user_id = auth()->user()->id;
        $Supplier->save();
      }
        return Response::json("1");
    }
    public function aging_list_sup(){
 $all_data = array();
 $sales = array();
  $payable = 0;
      $customers_data = DB::table('suppliers')->get();
      foreach($customers_data as $item){
        $all_data[$item->id]['name'] = $item->name;
        $all_data[$item->id]['cusid'] = $item->id;
        $sales_data = DB::table('purchases')->where('supplier_id',$item->id)->get();
        $sales_data_total = 0;
          $payable=0;
        foreach($sales_data as $item2){
          // print_r($sales_data);exit;
            $sales_data_total += $item2->total;
            $invoice_history_data = DB::table('purchase_history')->where('purchase_id',$item2->id)->get();

            foreach($invoice_history_data as $item3){
                 $payable += $item3->amount;
            }
            $all_data[$item->id]['payable'] = $payable;

        }
 $all_data[$item->id]['total'] = $sales_data_total;


        $amount_total= DB::table('purchases')->selectRaw('sum(total) as total')->where('supplier_id',$item->id)->first();

      }
      // print_r($all_data);exit;
      $new_data = array();
      $i = 0;
      foreach($all_data as $key =>$item ){
         if($item['total']!= 0){
           $temp['id'] = $key;
           $temp['cusid'] = $item['cusid'];
           $temp['name'] = $item['name'];
           $temp['total'] = $item['total'];

             if($item['payable'] == 0){
               // $new_data[$i]['payable'] = 0;
               $temp['payable'] = 0;
             }else{
               // $new_data[$i]['payable'] = $item['payable'];
               $temp['payable']  = $item['payable'];
             }
             // $new_data[$i]['total'] = $item['total'];
             $sales[] = (object)$temp;
             $i++;
         }

      }
      // $sales = (object)$new_data;
      // print_r($sales);exit;

         return view('report.aging_list_sup', compact('sales'));

       }
}
