<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit($id)
    {
        $users = User::findOrFail($id);
        if ($users->id == auth()->user()->id)
        {
            return view('user.edit', compact('users'));
        }
        return redirect('/home');
    }

    public function update(Request $request, $id)
    {
        $users = User::findOrFail($id);
        $users->name = $request->name;
        $users->email = $request->email;
        if (filled($request->password))
        {
            $users->password = bcrypt($request->password);
        }
        $users->save();

        return redirect('/home')->with('success', 'Updated successfully');
    }
}
