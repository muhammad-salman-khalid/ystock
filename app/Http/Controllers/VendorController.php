<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Models\Vendor;
use App\Models\VendorPayable;
use Illuminate\Http\Request;
use App\Repositories\Repository;
use App\Http\Requests\StoreName;

class VendorController extends Controller
{
    protected $model;

    public function __construct(Vendor $vendor)
    {
        $this->middleware('auth');
        $this->model = new Repository($vendor);
    }

    public function index()
    {
        $vendors = Vendor::with(['bill', 'vendorpayable'])->where('user_id', auth()->user()->id)->get();
        return view('vendor.index', compact('vendors'));
    }

    public function store(StoreName $request)
    {
        $this->model->create($request->only($this->model->getModel()->fillable));
        return redirect()->back()->with('success', 'Added successfully');
    }

    public function show($id)
    {
        $bills = Bill::with('expense')->get();
        $vendorpayables = VendorPayable::all();
        $vendors = $this->model->show($id);
        if ($vendors->user_id == auth()->user()->id)
        {
            return view('vendor.show', compact('vendors', 'bills', 'vendorpayables'));
        }
        return redirect('/home');
    }

    public function edit($id)
    {
        $vendors = $this->model->find($id);
        if ($vendors->user_id == auth()->user()->id)
        {
            return view('vendor.edit', compact('vendors'));
        }
        return redirect('/home');
    }

    public function update(Request $request, $id)
    {
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        return redirect('/vendor')->with('success', 'Updated successfully');
    }

    public function destroy($id)
    {
        $this->model->delete($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
}
