<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAmount;
use App\Models\VendorPayable;
use Illuminate\Http\Request;
use App\Repositories\Repository;

class VendorPayableController extends Controller
{
    protected $model;

    public function __construct(VendorPayable $payable)
    {
        $this->middleware('auth');
        $this->model = new Repository($payable);
    }

    public function store(StoreAmount $request)
    {
        $this->model->create($request->only($this->model->getModel()->fillable));
        return redirect()->back()->with('success', 'Added successfully');
    }

    public function edit($id)
    {
        $payables = $this->model->find($id);
        if ($payables->vendor->user_id == auth()->user()->id)
        {
            return view('vendorpayment.edit', compact('payables'));
        }
        return redirect('/home');
    }

    public function update(Request $request, $id)
    {
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        return redirect('/vendor')->with('success', 'Updated successfully');
    }

    public function destroy($id)
    {
        $this->model->delete($id);
        return redirect()->back()->with('success', 'Deleted successfully');
    }
}
