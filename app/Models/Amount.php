<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Amount extends Model
{
         protected $table = 'invoice_history';

    protected $fillable = ['id', 'sales_id', 'amount','user_id'];
    
   
      public function sale(){
        
        return $this->belongsTo(Sale::class);
    }

}
