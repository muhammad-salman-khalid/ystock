<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmountPurchase extends Model
{
         protected $table = 'purchase_history';

    protected $fillable = ['id', 'purchase_id', 'amount','user_id'];


      public function purchase(){

        return $this->belongsTo(Purchase::class);
    }

}
