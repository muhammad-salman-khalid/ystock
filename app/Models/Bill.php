<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $fillable = ['amount', 'invoice', 'file', 'expense_id', 'vendor_id'];

    public function expense()
    {
        return $this->belongsTo(Expense::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }
}
