<?php

namespace App\Models;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
        protected $fillable = ['name ', 'description'];

}
