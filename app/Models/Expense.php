<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = ['name', 'description', 'exp_amount' , 'category_id' , 'user_id' , 'date'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bill()
    {
        return $this->hasMany(Bill::class);
    }

 
    public function vendorpayable()
    {
        return $this->hasMany(VendorPayable::class);
    }
}
