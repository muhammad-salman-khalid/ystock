<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{
    protected $fillable = ['basic', 'allowance', 'deduction', 'employee_id'];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
