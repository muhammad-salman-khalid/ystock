<?php

namespace App\Models;
use App\Models\ProductCategory;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'description', 'category_id' , 'user_id', 'sellingprice','costprice'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function purchase()
    {
        return $this->hasMany(Purchase::class);
    }
      public function productcategory()
    {
        return $this->belongsTo(ProductCategory::class);
    }
    public function sale()
    {
        return $this->hasMany(SoldProducts::class);
    }

    public function receivable()
    {
        return $this->hasMany(Receivable::class);
    }

    public function payable()
    {
        return $this->hasMany(Payable::class);
    }
    public function soldProducts()
    {
        return $this->hasMany(SoldProducts::class);
    }
}
