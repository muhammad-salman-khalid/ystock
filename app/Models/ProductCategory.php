<?php

namespace App\Models;
use App\User;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
        protected $fillable = ['name ', 'description'];

            public function product()
    {
        return $this->hasMany(Product::class);
    }
}
