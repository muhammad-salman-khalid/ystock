<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable = ['invoice_date', 'invoice_message','statement_message', 'files', 'customer_id', 'due_date','term', 'total','payment_method',];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }
     public function purchaseproducts()
    {
        return $this->belongsTo(Purchaseproducts::class);
    }
}
