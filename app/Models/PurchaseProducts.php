<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseProducts extends Model
{
    protected $fillable = ['product_id', 'desc','qty', 'rate', 'amount'];
     protected $table = 'purchaseproducts';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }
    public function purchase(){

        return $this->belongsTo(Purchase::class);
    }
}
