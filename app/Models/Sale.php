<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = ['invoice_date', 'invoice_message','statement_message', 'files', 'customer_id', 'due_date','term', 'total','payment_method',];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
     public function soldProducts()
    {
        return $this->belongsTo(SoldProducts::class);
    }
}
