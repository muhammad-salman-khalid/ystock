<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SoldProducts extends Model
{
    protected $fillable = ['product_id', 'desc','qty', 'rate', 'amount'];
     protected $table = 'soldProducts';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
    public function sale(){
        
        return $this->belongsTo(Sale::class);
    }
}
