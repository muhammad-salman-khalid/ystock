<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'address', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function purchase()
    {
        return $this->hasMany(Purchase::class);
    }

    public function payable()
    {
        return $this->hasMany(Payable::class);
    }
}
