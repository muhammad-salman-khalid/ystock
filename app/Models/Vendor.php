<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'address', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bill()
    {
        return $this->hasMany(Bill::class);
    }

    public function vendorpayable()
    {
        return $this->hasMany(VendorPayable::class);
    }
}
