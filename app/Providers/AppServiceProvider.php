<?php

namespace App\Providers;

use App\Admin;
use App\Models\Customer;
use App\Models\Employee;
use App\Models\Expense;
use App\Models\PaymentMethod;
use App\Models\Product;
use App\Models\Supplier;
use App\Models\Vendor;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->share('paymentmethods', PaymentMethod::orderBy('name')->get());
        view()->share('allproducts', Product::orderBy('name')->get());
        view()->share('allexpenses', Expense::orderBy('name')->get());

        view()->share('allsuppliers', Supplier::orderBy('name')->get());
        view()->share('allcustomers', Customer::orderBy('name')->get());
        view()->share('allvendors', Vendor::orderBy('name')->get());

        view()->share('allemployees', Employee::orderBy('name')->get());

        view()->share('currency', Admin::where('currency', '!=', null)->first());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
