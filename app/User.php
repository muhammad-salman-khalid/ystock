<?php

namespace App;

use App\Models\Customer;
use App\Models\Employee;
use App\Models\Expense;
use App\Models\Product;
use App\Models\Supplier;
use App\Models\Vendor;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function product()
    {
        return $this->hasMany(Product::class);
    }

    public function expense()
    {
        return $this->hasMany(Expense::class);
    }

    public function supplier()
    {
        return $this->hasMany(Supplier::class);
    }

    public function vendor()
    {
        return $this->hasMany(Vendor::class);
    }

    public function customer()
    {
        return $this->hasMany(Customer::class);
    }

    public function employee()
    {
        return $this->hasMany(Employee::class);
    }
}
