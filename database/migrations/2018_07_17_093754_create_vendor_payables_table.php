<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorPayablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_payables', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedDecimal('amount', 12, 2);
            $table->string('method', 12)->nullable();
            $table->string('invoice', 12)->nullable();
            $table->integer('expense_id')->unsigned();
            $table->integer('vendor_id')->unsigned();
            $table->timestamps();
            $table->foreign('expense_id')->references('id')->on('expenses')->onDelete('cascade');
            $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_payables');
    }
}
