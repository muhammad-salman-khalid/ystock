-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 18, 2018 at 03:08 AM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `i4090040_wp1`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_home_post`
--

CREATE TABLE `wp_home_post` (
  `post_id` int(11) NOT NULL,
  `post_img` varchar(255) NOT NULL,
  `post_number` varchar(255) NOT NULL,
  `post_type` varchar(255) NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `post_content` varchar(255) NOT NULL,
  `post_links` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_home_post`
--

INSERT INTO `wp_home_post` (`post_id`, `post_img`, `post_number`, `post_type`, `post_title`, `post_content`, `post_links`) VALUES
(1, 'pro1.jpg', '01', 'GENRE TYPE', 'Beauty and the Beast', 'Short text about this item lorem ipsum dolor Short text about this item lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'myBtn'),
(2, 'pro2.jpg', '02', 'GENRE TYPE', 'Rays of light', 'Short text about this item lorem ipsum dolor Short text about this item lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', 'Btnmy'),
(3, 'pro3.jpg', '03', 'GENRE TYPE', 'Atmos', 'Short text about this item lorem ipsum dolor Short text about this item lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', ''),
(4, 'pro4.jpg', '04', 'GENRE TYPE', 'From Nothing', 'Short text about this item lorem ipsum dolor Short text about this item lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', ''),
(5, 'pro5.jpg', '05', 'GENRE TYPE', 'Supremacy', 'Short text about this item lorem ipsum dolor Short text about this item lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_home_post`
--
ALTER TABLE `wp_home_post`
  ADD PRIMARY KEY (`post_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_home_post`
--
ALTER TABLE `wp_home_post`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
