@extends('layouts.admin')

@section('content')
    <div class="container mt-5">
        <div class="card card-body">
            <h4>{{ __('All users') }}</h4>
            <hr>
            <table class="table table-striped" id="users">
                <thead>
                    <tr>
                        <th>{{ __('Name') }}</th>
                        <th>{{ __('E-mail') }}</th>
                        <th>{{ __('Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                <form method="POST" action="{{ action('AdminController@destroy', ['id' => $user->id]) }}" class="confirmation">
                                    <input type="hidden" name="_method" value="DELETE" />
                                    @csrf
                                    <a href="{{ url('/admin/user/' .$user->id. '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                    <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        @include('auth.register')

    </div>
@endsection

@section('scripts')
    <script>
        $('#users').DataTable();
        $(".confirmation").on("submit", function(){
            return confirm("This will delete the item. Click OK to confirm.");
        });
    </script>
@endsection
