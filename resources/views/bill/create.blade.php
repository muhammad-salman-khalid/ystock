<div class="modal fade" id="new-bill" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="font-weight-bold">{{ __('Create bill') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ action('BillController@store') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <label for="name">{{ __('Expense name') }}</label>

                        <select name="expense_id" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0" required>
                            <option value="">{{ __('Select expense') }}</option>
                            @foreach ($allexpenses->where('user_id', auth()->user()->id) as $expense)
                                <option value="{{ $expense->id }}">{{ $expense->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('expense_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('expense_id') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="name">{{ __('From vendor') }}</label>

                        <select name="vendor_id" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0" required>
                            <option value="">{{ __('Select vendor') }}</option>
                            @foreach ($allvendors->where('user_id', auth()->user()->id) as $vendor)
                                <option value="{{ $vendor->id }}">{{ $vendor->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('vendor_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('vendor_id') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="amount">{{ __('Amount') }}</label>

                        <input id="amount" type="text" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{ old('amount') }}" required>
                        @if ($errors->has('amount'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('amount') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="invoice">{{ __('Invoice') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                        <input id="invoice" type="text" class="form-control{{ $errors->has('invoice') ? ' is-invalid' : '' }}" name="invoice" value="{{ old('invoice') }}">
                        @if ($errors->has('invoice'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('invoice') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="name">{{ __('Upload file') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                        <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                            <input type="file" name="file" id="file">
                            @if ($errors->has('file'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
