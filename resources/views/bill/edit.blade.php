@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card card-body">
            <h4 class="font-weight-bold">{{ __('Edit bill') }}</h4>
            <hr>

            <form method="POST" action="{{ action('BillController@update', ['id' => $bills->id]) }}">
                @csrf

                <input name="_method" type="hidden" value="PATCH">

                <div class="form-group">
                    <label for="name">{{ __('Expense name') }}</label>

                    <select name="expense_id" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0" required>
                        <option value="{{ $bills->expense_id }}">{{ $bills->expense->name }}</option>
                        @foreach ($allexpenses->where('user_id', auth()->user()->id) as $expense)
                            <option value="{{ $expense->id }}">{{ $expense->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('expense_id'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('expense_id') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="name">{{ __('From vendor') }}</label>

                    <select name="vendor_id" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0" required>
                        <option value="{{ $bills->vendor_id }}">{{ $bills->vendor->name }}</option>
                        @foreach ($allvendors->where('user_id', auth()->user()->id) as $vendor)
                            <option value="{{ $vendor->id }}">{{ $vendor->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('vendor_id'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('vendor_id') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="amount">{{ __('Amount') }}</label>

                    <input id="amount" type="text" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{ $bills->amount }}" required>
                    @if ($errors->has('amount'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('amount') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="invoice">{{ __('Invoice') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                    <input id="invoice" type="text" class="form-control{{ $errors->has('invoice') ? ' is-invalid' : '' }}" name="invoice" value="{{ $bills->invoice }}">
                    @if ($errors->has('invoice'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('invoice') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
                    <a href="{{ url('/home') }}" class="btn btn-secondary">{{ __('Home') }}</a>
                </div>

            </form>
        </div>
    </div>
@endsection
