@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="card card-body">
        <h4 class="font-weight-bold">{{ __('Customers') }}</h4>
        <hr>
        <table class="table" id="customers">
            <thead>
                <tr>
                    <th>{{ __('Name') }}</th>
                    <th>{{ __('Receivable') }}</th>
                    <th>{{ __('Paid') }}</th>
                    <th>{{ __('Due') }}</th>
                    <th>{{ __('Action') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($customers as $customer)

                    <?php
                    $receivable = 0;
                    $total = 0;

                    foreach ($customer->sale as $s)
                    {
                        $receivable = $s->quantity * $s->rate;
                        $total += $receivable;
                    }
                    ?>
                    <tr>
                        <td><a href="{{ url('/customer/' . $customer->id) }}">{{ $customer->name }}</a></td>
                        <td>{{ $currency->currency . number_format($total, 2, '.', ',') }}</td>
                        <td>{{ $currency->currency . number_format($customer->receivable->sum('amount'), 2, '.', ',') }}</td>
                        <td>{{ $currency->currency . number_format(($total - $customer->receivable->sum('amount')), 2, '.', ',') }}</td>
                        <td>
                            <form method="POST" action="{{ action('CustomerController@destroy', ['id' => $customer->id]) }}" class="confirmation">
                                <input type="hidden" name="_method" value="DELETE" />
                                @csrf
                                <a href="{{ url('/customer/' . $customer->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#customers").DataTable();
    $(".confirmation").on("submit", function(){
        return confirm("This will delete the item. Click OK to confirm.");
    });
</script>
@endsection
