@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card card-body mb-4">
            <h4 class="font-weight-bold">{{ $customers->name }}</h4>
            <hr>
            <p><span class="text-muted">{{ __('Email: ') }}</span>{{ $customers->email }}</p>
            <p><span class="text-muted">{{ __('Phone: ') }}</span>{{ $customers->phone }}</p>
            <p><span class="text-muted">{{ __('Address: ') }}</span>{{ $customers->address }}</p>
        </div>

        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">{{ __('Sale') }}</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">{{ __('Payment') }}</a>
            </div>
        </nav>
        <div class="card">
            <div class="card-body">
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <h4 class="my-3">{{ __('Sale history to ' . $customers->name) }}</h4>
                        <hr>
                        <table class="table" id="sales">
                            <thead>
                            <tr>
                                <th>{{ __('Date') }}</th>
                                <th>{{ __('Product') }}</th>
                                <th>{{ __('Quantity') }}</th>
                                <th>{{ __('Rate/product') }}</th>
                                <th>{{ __('Total') }}</th>
                                <th>{{ __('Invoice') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sales as $sale)
                                @if($sale->customer_id == $customers->id)
                                    <tr>
                                        <td>{{ date('d M, Y', strtotime($sale->created_at)) }}</td>
                                        <td><a href="{{ url('/product/' . $sale->product->id) }}">{{ $sale->product->name }}</a></td>
                                        <td>{{ $sale->qty }}</td>
                                        <td>{{ $currency->currency . number_format($sale->rate, 2, '.', ',') }}</td>
                                        <td>{{ $currency->currency . number_format(($sale->qty * $sale->rate), 2, '.', ',')}}</td>
                                        <td>{{ $sale->invoice }}</td>
                                        <td>
                                            <form method="POST" action="{{ action('SaleController@destroy', ['id' => $sale->id]) }}" class="confirmation">
                                                <input type="hidden" name="_method" value="DELETE" />
                                                @csrf
                                                <a href="{{ url('/sale/' . $sale->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                                <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <h4 class="my-3">{{ __('Payment history from ' . $customers->name) }}</h4>
                        <hr>
                        <table class="table" id="receivable">
                            <thead>
                            <tr>
                                <th>{{ __('Date') }}</th>
                                <th>{{ __('Product') }}</th>
                                <th>{{ __('Amount') }}</th>
                                <th>{{ __('Method') }}</th>
                                <th>{{ __('Invoice') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($receivables as $receivable)
                                @if($receivable->customer_id == $customers->id)
                                    <tr>
                                        <td>{{ date('d M, Y', strtotime($receivable->created_at)) }}</td>
                                        <td><a href="{{ url('/product/' . $receivable->product->id) }}">{{ $receivable->product->name }}</a></td>
                                        <td>{{ $currency->currency . $receivable->amount }}</td>
                                        <td>{{ $receivable->method }}</td>
                                        <td>{{ $receivable->invoice }}</td>
                                        <td>
                                            <form method="POST" action="{{ action('ReceivableController@destroy', ['id' => $receivable->id]) }}" class="confirmation">
                                                <input type="hidden" name="_method" value="DELETE" />
                                                @csrf
                                                <a href="{{ url('/receivable/' . $receivable->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                                <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $("#sales").DataTable();
        $("#receivable").DataTable();
        $(".confirmation").on("submit", function(){
            return confirm("This will delete the item. Click OK to confirm.");
        });
    </script>
@endsection
