@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card card-body">
            <h4 class="font-weight-bold">{{ __('Edit employee') }}</h4>
            <hr>

            <form method="POST" action="{{ action('EmployeeController@update', ['id' => $employees->id]) }}">
                @csrf

                <input name="_method" type="hidden" value="PATCH">

                <div class="form-group">
                    <label for="name">{{ __('Employee name') }}</label>

                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $employees->name }}" required>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="basic">{{ __('Basic salary') }}</label>

                    <input id="basic" type="text" class="form-control{{ $errors->has('basic') ? ' is-invalid' : '' }}" name="basic" value="{{ $employees->basic }}" required>
                    @if ($errors->has('basic'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('basic') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="email">{{ __('E-mail address') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $employees->email }}">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="phone">{{ __('Phone number') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                    <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ $employees->phone }}">
                    @if ($errors->has('phone'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="address">{{ __('Address') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                    <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ $employees->address }}">
                    @if ($errors->has('address'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                    @endif
                </div>

                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
                    <a href="{{ url('/home') }}" class="btn btn-secondary">{{ __('Home') }}</a>
                </div>

            </form>
        </div>
    </div>
@endsection
