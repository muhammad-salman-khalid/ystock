@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">{{ __('Employee') }}</a>
            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">{{ __('Payroll') }}</a>
        </div>
    </nav>
    <div class="card card-body">
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <h4 class="font-weight-bold">{{ __('Employees') }}</h4>
                <hr>
                <table class="table" id="employees">
                    <thead>
                    <tr>
                        <th>{{ __('Name') }}</th>
                        <th>{{ __('Basic') }}</th>
                        <th>{{ __('Allowance') }}</th>
                        <th>{{ __('Deduction') }}</th>
                        <th>{{ __('Net pay') }}</th>
                        <th>{{ __('Action') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($employees as $employee)
                        <tr>
                            <td><a href="{{ url('/employee/' . $employee->id) }}">{{ $employee->name }}</a></td>
                            <td>{{ $currency->currency . number_format($employee->payroll->sum('basic'), 2, '.', ',') }}</td>
                            <td>{{ $currency->currency . number_format($employee->payroll->sum('allowance'), 2, '.', ',') }}</td>
                            <td>{{ $currency->currency . number_format($employee->payroll->sum('deduction'), 2, '.', ',') }}</td>
                            <td>{{ $currency->currency . number_format(($employee->payroll->sum('basic') + $employee->payroll->sum('allowance') - $employee->payroll->sum('deduction')), 2, '.', ',' ) }}</td>
                            <td>
                                <form method="POST" action="{{ action('EmployeeController@destroy', ['id' => $employee->id]) }}" class="confirmation">
                                    <input type="hidden" name="_method" value="DELETE" />
                                    @csrf
                                    <a href="{{ url('/employee/' . $employee->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                    <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                <h4 class="font-weight-bold">{{ __('Payroll') }}</h4>
                <hr>
                <table class="table" id="payrolls">
                    <thead>
                    <tr>
                        <th>{{ __('Date') }}</th>
                        <th>{{ __('Employee') }}</th>
                        <th>{{ __('Basic') }}</th>
                        <th>{{ __('Allowance') }}</th>
                        <th>{{ __('Deduction') }}</th>
                        <th>{{ __('Net pay') }}</th>
                        <th>{{ __('Action') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($payrolls as $payroll)
                        @if($payroll->employee->user_id == auth()->user()->id)
                        <tr>
                            <td>{{ date('d M, Y', strtotime($payroll->created_at)) }}</td>
                            <td><a href="{{ url('/employee/' . $payroll->employee->id) }}">{{ $payroll->employee->name }}</a></td>
                            <td>{{ $currency->currency . $payroll->basic }}</td>
                            <td>{{ $currency->currency . $payroll->allowance }}</td>
                            <td>{{ $currency->currency . $payroll->deduction }}</td>
                            <td>{{ $currency->currency . number_format(($payroll->allowance + $payroll->basic - $payroll->deduction), 2, '.', ',') }}</td>
                            <td>
                                <form method="POST" action="{{ action('PayrollController@destroy', ['id' => $payroll->id]) }}" class="confirmation">
                                    <input type="hidden" name="_method" value="DELETE" />
                                    @csrf
                                    <a href="{{ url('/payroll/' . $payroll->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                    <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#employees").DataTable();
    $("#payrolls").DataTable();
    $(".confirmation").on("submit", function(){
        return confirm("This will delete the item. Click OK to confirm.");
    });
</script>
@endsection
