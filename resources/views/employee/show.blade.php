@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card card-body mb-4">
            <h4 class="font-weight-bold">{{ $employees->name }}</h4>
            <hr>
            <p><span class="text-muted">{{ __('Email: ') }}</span>{{ $employees->email }}</p>
            <p><span class="text-muted">{{ __('Phone: ') }}</span>{{ $employees->phone }}</p>
            <p><span class="text-muted">{{ __('Address: ') }}</span>{{ $employees->address }}</p>
            <p><span class="text-muted">{{ __('Basic salary: ') }}</span>{{ $currency->currency. $employees->basic }}</p>
        </div>

        <div class="card card-body">
            <h4 class="my-3">{{ __('Payroll of ' . $employees->name) }}</h4>
            <hr>
            <table class="table" id="employees">
                <thead>
                <tr>
                    <th>{{ __('Date') }}</th>
                    <th>{{ __('Basic') }}</th>
                    <th>{{ __('Allowance') }}</th>
                    <th>{{ __('Deduction') }}</th>
                    <th>{{ __('Net pay') }}</th>
                    <th>{{ __('Action') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($payrolls as $payroll)
                    @if($payroll->employee_id == $employees->id)
                        <tr>
                            <td>{{ date('d M, Y', strtotime($payroll->created_at)) }}</td>
                            <td>{{ $currency->currency . $payroll->basic }}</td>
                            <td>{{ $currency->currency . $payroll->allowance }}</td>
                            <td>{{ $currency->currency . $payroll->deduction }}</td>
                            <td>{{ $currency->currency . number_format(($payroll->allowance + $payroll->basic - $payroll->deduction), 2, '.', ',') }}</td>
                            <td>
                                <form method="POST" action="{{ action('PayrollController@destroy', ['id' => $payroll->id]) }}" class="confirmation">
                                    <input type="hidden" name="_method" value="DELETE" />
                                    @csrf
                                    <a href="{{ url('/payroll/' . $payroll->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                    <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $("#employees").DataTable();
        $(".confirmation").on("submit", function(){
            return confirm("This will delete the item. Click OK to confirm.");
        });
    </script>
@endsection
