<div class="modal fade" id="new-expense" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="font-weight-bold">{{ __('Create expense') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form onsubmit="return submit_expense_form();return false;" role="form" id="expense_form" method="post">
                    @csrf
                    <div class="row">
                    <div class="form-group col-md-6">
                        <label for="name">{{ __('Expense Name') }}</label>

                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                        <div class="form-group col-md-6">
                            <label for="categorylist">{{ __('Expense Category') }}</label><br>
                                <div class="dropb">
                                <input class="drp categorylist" id="clist" type="text" name="categorylist" required placeholder="Choose Category">

                             </div>
                            
                        @if ($errors->has('categorylist'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('categorylist') }}</strong>
                            </span>
                        @endif
                    </div>
                    </div>
                    <div class="row"> 
                    <div class="form-group col-md-6">
                        <label for="description">{{ __('Expense Description') }}<small class="font-italic ml-1">{{ __('(optional, max 20 char)') }}</small></label>
                        <textarea id="description" cols="30" rows="5" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description">
                            
                        </textarea>
                        
                        @if ($errors->has('description'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                        <div class="form-group col-md-6">
                            <div class="row">
                            <label for="e_amount">{{ __('Enter Expense Amount') }}</label>

                        <input id="e_amount" type="text" class="form-control{{ $errors->has('e_amount') ? ' is-invalid' : '' }}" name="expense-amount" value="{{ old('e_amount') }}" required>
                        @if ($errors->has('e_amount'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('e_amount') }}</strong>
                            </span>
                         @endif
                            </div>
                            <div class="row">
                                <label>Expense Date</label><br>

                     <input type="text" class="datepicker calistt" id="expense_date" name="expense_date" placeholder="yyyy-mm-dd">

			
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<script>
 

   $(document).ready(function(){
        show_category_list();
      
    });
    
  
    function expensecat_autocomplete() {
        $("#clist").autocomplete({
            source: expcategorylist,
            minLength: 0,
            //                 max:5,
            //                 scroll:true,
            select: function(e, ui) {
                debugger;
                if (ui.item.value == 'add') {
                    $('a[data-target="#new-cateogry"]').trigger('click');
                    //                         $('#close-btn-invoice').trigger('click');
                    $(this).val('');
                    $("#cat_name").val('');
                    $("#cat_description").val('');
                    this.setAttribute('categry_id', 0);
                } else {
                    var temp = getSupplier(ui.item.value);
                    // invoice_data.customer_id = ui.item.value;
                    $("#cat_name").val(temp.email);
                    $("#cat_description").val(temp.address);
                    $(this).val(ui.item.label);
                    this.setAttribute('categry_id', ui.item.value);
                    return false;
                }
            },
            change: function(event, ui) {
                if (!ui.item) {
                    this.value = '';
                } else {
                    // return your label here
                }
            },
            focus: function(event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            response: function(e, ui) {
                ui.content.unshift({
                    label: "Add New",
                    value: "add"
                });
            }
        }).focus(function() {
            if (this.value == "") {
                $(this).autocomplete("search");
            }
        });
    }

</script>

  