@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="card card-body">
        <h4 class="font-weight-bold">{{ __('Edit expense') }}</h4>
        <hr>

        <form method="POST" action="{{ action('ExpenseController@update', ['id' => $expenses->id]) }}">
            @csrf

            <input name="_method" type="hidden" value="PATCH">

            <div class="form-group">
                <label for="name">{{ __('Expense Name') }}</label>

                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $expenses->name }}" required>
                @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="description">{{ __('Expense Description') }}<small class="font-italic ml-1">{{ __('(optional, max 20 char)') }}</small></label>

                <input id="description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ $expenses->description }}">
                @if ($errors->has('description'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="e_amount">{{ __('Expense Amount') }}<small class="font-italic ml-1">{{ __('(optional, max 20 char)') }}</small></label>

                <input id="e_amount" type="text" class="form-control{{ $errors->has('e_amount') ? ' is-invalid' : '' }}" name="e_amount" value="{{ $expenses->exp_amount }}">
                @if ($errors->has('e_amount'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('e_amount') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="categorylist">{{ __('Expense Category') }}</label><br>

                <select id="clist" class="categorylist" name="categorylist">

                </select>
                @if ($errors->has('categorylist'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('categorylist') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label>Expense Date</label><br>

                <input type="text" class="datepicker categorylist" id="expense_date" name="expense_date" value="{{ $expenses->date }} " placeholder="yyyy-mm-dd">
            </div>
            <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
                <a href="{{ url('/home') }}" class="btn btn-secondary">{{ __('Home') }}</a>
            </div>

        </form>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        show_category_list();

    });

</script>
@endsection
