@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Expense</a>
        </div>
    </nav>
    <div class="card">
        <div class="card-body">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <h4 class="font-weight-bold">{{ __('Expenses') }}</h4>
                    <hr>
                    <table class="table" id="expenses">
                        <thead>
                        <tr>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Description') }}</th>
                            <th>{{ __('Expenses Amount') }}</th>
                            <th>{{ __('Category') }}</th>
                            <th>{{ __('Date') }}</th>
                            <th>{{ __('Actions')}}</th>
                        
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($expenses as $expense)
                            <tr>
                                <td>{{ $expense->name }}</a></td>
                                <td>{{ $expense->description }}</td>
                                <td>{{ $currency->currency . round($expense->exp_amount, 2) }}</td>
                                <td>{{ $expense->category_id }}</td>
                                <td>{{ $expense->date }}</td>
                                <td>
                                    <form method="POST" action="{{ action('ExpenseController@destroy', ['id' => $expense->id]) }}" class="confirmation">
                                        <input type="hidden" name="_method" value="DELETE" />
                                        @csrf
                                        <a href="{{ url('/expense/' . $expense->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                        <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
              
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#expenses").DataTable();
    $(".confirmation").on("submit", function(){
        return confirm("This will delete the item. Click OK to confirm.");
    });
</script>
@endsection
