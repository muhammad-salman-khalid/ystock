@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Expense</a>
            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Bill</a>
        </div>
    </nav>
    <div class="card">
        <div class="card-body">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <h4 class="font-weight-bold">{{ __('Expenses') }}</h4>
                    <hr>
                    <table class="table" id="expenses">
                        <thead>
                        <tr>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Description') }}</th>
                            <th>{{ __('Bill') }}</th>
                            <th>{{ __('Payment') }}</th>
                            <th>{{ __('Due') }}</th>
                            <th>{{ __('Action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($expenses as $expense)
                            <tr>
                                <td><a href="{{ url('/expense/' . $expense->id) }}">{{ $expense->name }}</a></td>
                                <td>{{ $expense->description }}</td>
                                <td>{{ $currency->currency . number_format($expense->bill->sum('amount'), 2, '.', ',') }}</td>
                                <td>{{ $currency->currency . number_format($expense->vendorpayable->sum('amount'), 2, '.', ',') }}</td>
                                <td>{{ $currency->currency . number_format(($expense->bill->sum('amount') - $expense->vendorpayable->sum('amount')), 2, '.', ',') }}</td>
                                <td>
                                    <form method="POST" action="{{ action('ExpenseController@destroy', ['id' => $expense->id]) }}" class="confirmation">
                                        <input type="hidden" name="_method" value="DELETE" />
                                        @csrf
                                        <a href="{{ url('/expense/' . $expense->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                        <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <h4 class="font-weight-bold">{{ __('Bills') }}</h4>
                    <hr>
                    <table class="table" id="bills">
                        <thead>
                        <tr>
                            <th>{{ __('Date') }}</th>
                            <th>{{ __('Expense') }}</th>
                            <th>{{ __('Vendor') }}</th>
                            <th>{{ __('Amount') }}</th>
                            <th>{{ __('Invoice') }}</th>
                            <th>{{ __('Action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($bills as $bill)
                            @if($bill->expense->user_id == auth()->user()->id)
                                <tr>
                                    <td>{{ date('d M, Y', strtotime($bill->created_at)) }}</td>
                                    <td><a href="{{ url('/expense/'. $bill->expense->id) }}">{{ $bill->expense->name }}</a></td>
                                    <td><a href="{{ url('/vendor/' . $bill->vendor->id) }}">{{ $bill->vendor->name }}</a></td>
                                    <td>{{ $currency->currency . number_format($bill->amount, 2, '.', ',') }}</td>
                                    <td>{{ $bill->invoice }}</td>
                                    <td>
                                        <form method="POST" action="{{ action('BillController@destroy', ['id' => $bill->id]) }}" class="confirmation">
                                            <input type="hidden" name="_method" value="DELETE" />
                                            @csrf
                                            <a href="{{ url('/storage/images/'.$bill->file) }}" title=""><i class="fa fa-eye text-primary mr-2"></i></a>
                                            <a href="{{ url('/bill/' . $bill->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                            <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#expenses").DataTable();
    $("#bills").DataTable();
    $(".confirmation").on("submit", function(){
        return confirm("This will delete the item. Click OK to confirm.");
    });
</script>
@endsection
