@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card card-body mb-4">
            <h4 class="font-weight-bold">{{ $expenses->name }}</h4>
            <small>{{ $expenses->description }}</small>
            @foreach($ranks as $key => $value)
                @if($value->expense->user_id == auth()->user()->id)
                    @if($value->expense_id == $expenses->id)
                        <?php $position = $key + 1; ?>
                        <p class="text-danger font-weight-bold my-2">
                            {{ 'This item ranks ' . $position . ' in expense.' }}
                        </p>
                    @endif
                @endif
            @endforeach
        </div>

        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">{{ __('Billing') }}</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">{{ __('Payment') }}</a>
            </div>
        </nav>
        <div class="card">
            <div class="card-body">
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <h4>{{ __('Billing history of ' . $expenses->name) }}</h4>
                        <hr>
                        <table class="table" id="bills">
                            <thead>
                            <tr>
                                <th>{{ __('Date') }}</th>
                                <th>{{ __('Vendor') }}</th>
                                <th>{{ __('Amount') }}</th>
                                <th>{{ __('Invoice') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($bills as $bill)
                                @if($bill->expense_id == $expenses->id)
                                    <tr>
                                        <td>{{ date('d M, Y', strtotime($bill->created_at)) }}</td>
                                        <td><a href="{{ url('/vendor/' . $bill->vendor->id) }}">{{ $bill->vendor->name }}</a></td>
                                        <td>{{ $currency->currency . $bill->amount }}</td>
                                        <td>{{ $bill->invoice }}</td>
                                        <td>
                                            <form method="POST" action="{{ action('BillController@destroy', ['id' => $bill->id]) }}" class="confirmation">
                                                <input type="hidden" name="_method" value="DELETE" />
                                                @csrf
                                                <a href="{{ url('/bill/' . $bill->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                                <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <h4>{{ __('Payment history for ' . $expenses->name) }}</h4>
                        <hr>
                        <table class="table" id="vendorpayable">
                            <thead>
                            <tr>
                                <th>{{ __('Date') }}</th>
                                <th>{{ __('Vendor') }}</th>
                                <th>{{ __('Amount') }}</th>
                                <th>{{ __('Method') }}</th>
                                <th>{{ __('Invoice') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($vendorpayables as $vendorpayable)
                                @if($vendorpayable->expense_id == $expenses->id)
                                    <tr>
                                        <td>{{ date('d M, Y', strtotime($vendorpayable->created_at)) }}</td>
                                        <td><a href="{{ url('/vendor/' . $vendorpayable->vendor->id) }}">{{ $vendorpayable->vendor->name }}</a></td>
                                        <td>{{ $currency->currency . $vendorpayable->amount }}</td>
                                        <td>{{ $vendorpayable->method }}</td>
                                        <td>{{ $vendorpayable->invoice }}</td>
                                        <td>
                                            <form method="POST" action="{{ action('VendorPayableController@destroy', ['id' => $vendorpayable->id]) }}" class="confirmation">
                                                <input type="hidden" name="_method" value="DELETE" />
                                                @csrf
                                                <a href="{{ url('/vendorpayment/' . $vendorpayable->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                                <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $("#bills").DataTable();
        $("#vendorpayable").DataTable();
        $(".confirmation").on("submit", function(){
            return confirm("This will delete the item. Click OK to confirm.");
        });
    </script>
@endsection