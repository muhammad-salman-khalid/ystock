@extends('layouts.app')

@section('content')

    <?php
        foreach($all_purchases as $item) {
            $sum_purchase = $item->purchase;
        }
        foreach($all_sales as $item) {
            $sum_sale = $item->sale;
        }
        foreach ($expenses as $item) {
            $sum_expense = $item->expense;
        }
        foreach ($payrolls as $item) {
            $sum_payroll = $item->payroll;
        }
        foreach($receivables as $item) {
            $sum_receivable = $item->receivable;
        }
        foreach($payables_supplier as $item) {
            $sum_payable = $item->amount;
        }
        foreach($payables_vendor as $item) {
            $sum_p = $item->amount;
        }
    ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-sm-6 mb-4">
                <div class="card card-body font-weight-bold">
                    <div class="row">
                        <div class="col pt-2"><i class="fa fa-chart-bar fa-4x text-danger"></i></div>
                        <div class="col text-right">
                            <p class="text-uppercase text-danger">{{ __('Net Profit') }}</p>
                            <h4>{{ $currency->currency . number_format(($sum_sale - ($sum_purchase + $sum_expense + $sum_payroll)), 2, '.', ',') }}</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 mb-4">
                <div class="card card-body font-weight-bold">
                    <div class="row">
                        <div class="col pt-2"><i class="fas fa-archive fa-4x text-success"></i></div>
                        <div class="col text-right">
                            <p class="text-uppercase text-success">{{ __('Total Products') }}</p>
                            <h4>{{ $products }}</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 mb-4">
                <div class="card card-body font-weight-bold">
                    <div class="row">
                        <div class="col pt-2"><i class="fas fa-chart-line fa-4x text-info"></i></div>
                        <div class="col text-right">
                            <p class="text-uppercase text-info">{{ __('Total Sale') }}</p>
                            <h4>{{ $currency->currency . number_format($sum_sale, 2, '.', ',') }}</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 mb-4">
                <div class="card card-body font-weight-bold">
                    <div class="row">
                        <div class="col pt-2"><i class="fas fa-dolly fa-4x text-warning"></i></div>
                        <div class="col text-right">
                            <p class="text-uppercase text-warning">{{ __('Total Purchase') }}</p>
                            <h4>{{ $currency->currency . number_format($sum_purchase, 2, '.', ',') }}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-6 mb-4">
                <div id="curve_chart" style="height: 220px"></div>
            </div>
            <div class="col-md-6 col-sm-6 mb-4">
                <div id="piechart" style="height: 220px"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-6 mb-4">
                <div class="card card-body">
                    <p class="font-weight-bold text-uppercase">{{ __('Recent Purchases') }}</p>
                    <hr>
                    <div class="row font-weight-bold">
                        <div class="col">{{ __('Date') }}</div>
                        <div class="col">{{ __('Product') }}</div>
                        <div class="col">{{ __('Cost') }}</div>
                    </div>
                    <hr>
                    @foreach($purchases as $p)
                        @if($p->product->user_id == auth()->user()->id)
                            <div class="row">
                                <div class="col">
                                    {{ date('d M, Y', strtotime($p->created_at)) }}
                                </div>
                                <div class="col">
                                    {{ $p->product->name }}
                                </div>
                                <div class="col">
                                    {{ $currency->currency . number_format($p->qty * $p->rate, 2, '.', ',') }}
                                </div>
                            </div>
                            <hr>
                        @endif
                    @endforeach
                    <a href="{{ url('/purchase') }}">{{ __('View All') }}<i class="fa fa-arrow-right ml-1"></i></a>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 mb-4">
                <div class="card card-body">
                    <p class="font-weight-bold text-uppercase">{{ __('Recent Sales') }}</p>
                    <hr>
                    <div class="row font-weight-bold">
                        <div class="col">{{ __('Date') }}</div>
                        <div class="col">{{ __('Product') }}</div>
                        <div class="col">{{ __('Price') }}</div>
                    </div>
                    <hr>
                    @foreach($sales as $s)
                        @if($s->product->user_id == auth()->user()->id)
                            <div class="row">
                                <div class="col">

                                    {{ date('d M, Y', strtotime($s->created_at)) }}
                                </div>
                                <div class="col">
                                    {{ $s->product->name }}
                                </div>
                                <div class="col">
                                    {{ $currency->currency . number_format($s->qty * $s->rate, 2, '.', ',') }}
                                </div>
                            </div>
                            <hr>
                        @endif
                    @endforeach
                    <a href="{{ url('/sale') }}">{{ __('View All') }}<i class="fa fa-arrow-right ml-1"></i></a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-6 mb-4">
                <div class="card card-body">
                    <p class="font-weight-bold text-uppercase">{{ __('New Employees') }}</p>
                    <hr>
                    <div class="row font-weight-bold">
                        <div class="col">{{ __('Name') }}</div>
                        <div class="col">{{ __('Joined at') }}</div>
                        <div class="col">{{ __('Basic salary') }}</div>
                    </div>
                    <hr>
                    @foreach($employees as $s)
                        <div class="row">
                            <div class="col">
                                {{ $s->name }}
                            </div>
                            <div class="col">
                                {{ date('d M, Y', strtotime($s->created_at)) }}
                            </div>
                            <div class="col">
                                {{ $currency->currency . $s->basic }}
                            </div>
                        </div>
                        <hr>
                    @endforeach
                    <a href="{{ url('/employee') }}">{{ __('View All') }}<i class="fa fa-arrow-right ml-1"></i></a>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 mb-4">
                <div class="card card-body mb-4">
                    <p class="text-uppercase font-weight-bold text-success">{{ __('Receivable from customers') }}</p>
                    <h4>{{ $currency->currency . number_format($sum_sale - $sum_receivable, 2, '.', ',') }}</h4>
                </div>
                <div class="card card-body mb-4">
                    <p class="text-uppercase font-weight-bold text-info">{{ __('Payable to suppliers') }}</p>
                    <h4>{{ $currency->currency . number_format($sum_purchase - $sum_payable, 2, '.', ',') }}</h4>
                </div>
                <div class="card card-body mb-4">
                    <p class="text-uppercase font-weight-bold text-muted">{{ __('Payable to vendors') }}</p>
                    <h4>{{ $currency->currency . number_format($sum_expense - $sum_p, 2, '.', ',') }}</h4>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 mb-4">
                <div class="card card-body mb-4">
                    <p class="text-uppercase font-weight-bold">{{ __('Bills/Office Expense') }}</p>
                    <h4>{{ $currency->currency . number_format($sum_expense, 2, '.', ',') }}</h4>
                </div>
                <div class="card card-body">
                    <p class="text-uppercase font-weight-bold">{{ __('Payroll Expense') }}</p>
                    <h4>{{ $currency->currency . number_format($sum_payroll, 2, '.', ',') }}</h4>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
    var data1;
    var someStr;
    var data;

  google.charts.load('current', {'packages':['corechart'], callback: drawChart1});

function drawChart1() {
  var data = google.visualization.arrayToDataTable([
    ['Year', 'Sales', 'Expenses'],
    ['2015',  1000,      400],
    ['2016',  1170,      460],
    ['2017',  660,       1120],
    ['2018',  1030,      540]
  ]);

  var options = {
    title: 'Company Performance',
    hAxis: {title: 'Year',  titleTextStyle: {color: '#333'}},
    vAxis: {minValue: 0}
  };

  var chart = new google.visualization.AreaChart(document.getElementById('curve_chart'));
  chart.draw(data, options);
}



    </script>

    <script>

    google.charts.load('current', {'packages':['corechart'], callback: drawChart});

function drawChart() {
 someStr = '["JAN","1088626"],["FEB","1478093"],["MAR","1232870"],["APR","1151634"],["MAY","1083623"],["JUN","740591"],["JUL","769227"],["AUG","1162995"],["SEP","951794"],["OCT","884736"],["NOV","500902"],["DEC","1221438"]';

 data1 = JSON.parse('[' + someStr.replace(/(["'])(\d+)\1/g,"$2") + ']');

// data array - add two column headings to data1
data1.splice(0, 0, ['month', 'number']);

// data table
 data = google.visualization.arrayToDataTable(data1);

// chart options
var options = {
  title: 'Total Sales',
  is3D: true
};

// chart
var chart = new google.visualization.PieChart(document.getElementById('piechart'));
chart.draw(data, options);
}

$(window).resize(function(){
drawChart1();
drawChart();
});

    </script>


@endsection
