@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="card card-body">
        <h4 class="font-weight-bold">{{ __('Inventory') }}</h4>
        <hr>
        <table class="table" id="stock">
            <thead>
                <tr>
                    <th>{{ __('Id') }}</th>
                    <th>{{ __('Product Name') }}</th>
                    <th>{{ __('Stock') }}</th>
                    <th>{{ __('Add/Update Stock') }}</th>
                </tr>
            </thead>

              <tbody>
                  @foreach($data as $product)
                <tr>
                <td id="td-{{$product->id}}-productid">{{ $product->id }}</a></td>
                <td id="td-{{$product->id}}-productname">{{ $product->name }}</a></td>
                <td id="td-{{$product->id}}-productstock">{{ $product->stock }}</a></td>
                <td>
                    <form>
                        @csrf
                        <a data-toggle="modal" onclick="open_model({{$product->id}})"  data-target="#stock1" title="Add Stock"><i class="fas fa-plus-circle mr-2"></i></a>
                    </form>
                </td>
              </tr>
              @endforeach
              </tbody>

        </table>
    </div>
</div>
@include('inventory.stockmodal')
@endsection
@section('scripts')
<script>
$('#stock').DataTable();
</script>
<script>
function open_model(obj){
  debugger;
    var id = parseInt($('#td-'+obj+'-productid').text());
    var name =$('#td-'+obj+'-productname').text();
    $('#name000').text(name);
    // $('#pay-cus-name').val(name);
    // $('#pay-total').val(total1);
    // $('#invoice_date1').val(invoice_date.getDate()+' '+months[invoice_date.getMonth()]+', '+invoice_date.getFullYear());
    // $('#days_left').text('( Due in '+diffDays+' Days)');
    // $('#due_date1').val(due_date.getDate()+' '+months[due_date.getMonth()]+', '+due_date.getFullYear());
}
</script>

@endsection
