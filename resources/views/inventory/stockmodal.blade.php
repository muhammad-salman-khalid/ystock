<div class="modal fade" id="stock1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="font-weight-bold">{{ __('Add Stock For ') }}<span id="name000"></span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body label1">
                <form id="stockaddajax" onsubmit="return submit_form_data_stock();return false; " method="POST">
                    <input type="number" name="stock" class="form-control" placeholder="Add Stock">
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                        <button type="button" id="paymentclose" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
