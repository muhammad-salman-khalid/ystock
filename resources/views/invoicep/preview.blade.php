
@extends('layouts.app')
@section('content')
<div class="content p-4">

              <h2 class="mb-4">Invoice</h2>

  <div class="card">
      <div class="card-body">
          <div class="row py-5">
              <div class="col-md-10 offset-md-1">
                  <div class="row">
                      <div class="col-md">
                          <h1 class="text-uppercase">Invoice</h1>
                      </div>
                      <div class="col-md text-md-right">
                          <img src="https://www.1eent.com/content/images/static/Employers-Cover/0092647220284c189b52282be549d546.jpg" style="width: 170px;height: 45px;">
                          <p class="mt-2 mb-0">
                              <!-- 123 Business Street<br>
                              Toronto, ON -->
                          </p>
                      </div>
                  </div>

                  <hr class="my-5">

                  <div class="row">
                      <div class="col-md">
                          <h5 class="text-uppercase">Date</h5>
                          <p class="mb-0">{{$invoice_data['invoice_details'][0]->inv_date}}</p>
                      </div>
                      <div class="col-md text-md-center">
                          <h5 class="text-uppercase">Invoice No.</h5>
                          <p class="mb-0">234252342</p>
                      </div>
                      <div class="col-md text-md-right">
                          <h5 class="text-uppercase">Invoice To</h5>
                          <p class="mb-0">
                              {{$invoice_data['invoice_details'][0]->cname}}<br>
                              {{$invoice_data['invoice_details'][0]->address}}<br>
                              {{$invoice_data['invoice_details'][0]->phone}}<br>

                          </p>
                      </div>
                  </div>

                  <hr class="my-5">

                  <table class="table table-borderless mb-0">
                      <thead>
                      <tr class="border-bottom text-uppercase">
                          <th scope="col">Product Name</th>
                          <th scope="col">qty</th>
                          <th scope="col">Rate</th>
                          <th scope="col" class="text-right">Total</th>
                      </tr>
                      </thead>
                      <tbody>
                        @foreach($invoice_data['invoice_products'] as $product)
                    <tr>
                        <td>{{$product->P_name}}</td>
                        <td>{{$product->qty}}</td>
                        <td>{{$product->rate}}</td>
                        <td class="text-right">{{$product->amount}}</td>
                    </tr>
                    @endforeach
                      <!-- <tr class="bg-light font-weight-bold">
                          <td></td>
                          <td></td>
                          <td class="text-uppercase">Subtotal</td>
                          <td class="text-right">{{$invoice_data['invoice_details'][0]->total}}</td>
                      </tr> -->
                      <!-- <tr class="bg-light font-weight-bold">
                          <td></td>
                          <td></td>
                          <td class="text-uppercase">Tax</td>
                          <td class="text-right">$156.00</td>
                      </tr> -->
                      <tr class="bg-light font-weight-bold">
                          <td></td>
                          <td></td>
                          <td class="text-uppercase">Total</td>
                          <td class="text-right">{{$invoice_data['invoice_details'][0]->total}}</td>
                      </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
      </div>
  @endsection
