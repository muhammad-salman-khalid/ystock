<nav class="navbar navbar-expand navbar-dark bg-info">
    <a class="sidebar-toggle mr-3" href="#"><i class="fa fa-bars"></i></a>
    <a class="navbar-brand" href="{{ url('/home') }}">
        {{ config('app.name', 'Laravel') }}
    </a>

    <div class="navbar-collapse collapse">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item"><a href="#" class="nav-link"><i class="fa fa-bell"></i> 3</a></li>
            <li class="nav-item dropdown">
                <a href="#" id="add_items" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus-circle mr-2"></i>{{ __('Add new') }}</a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="add_items" style="width: 200px">
                    <a class="dropdown-item" data-toggle="modal"  href="#new-product">{{ __('Product') }}</a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#new-expense">{{ __('Expense') }}</a>
                    <a class="dropdown-item mt-3" data-toggle="modal" data-target="#new-customer">{{ __('Customer') }}</a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#new-supplier">{{ __('Supplier') }}</a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#new-vendor">{{ __('Vendor') }}</a>
                    <a class="dropdown-item mt-3" data-toggle="modal" data-target="#new-sale">{{ __('Sale invoice') }}</a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#new-purchase">{{ __('Purchase order') }}</a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#new-bill">{{ __('Bill') }}</a>
                    <a class="dropdown-item mt-3" data-toggle="modal" data-target="#new-receivable">{{ __('Receive payment') }}</a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#new-payable">{{ __('Supplier payment') }}</a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#new-vendorpayable">{{ __('Vendor payment') }}</a>
                    <a class="dropdown-item mt-3" data-toggle="modal" data-target="#new-employee">{{ __('Employee') }}</a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#new-payroll">{{ __('Run payroll') }}</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a href="#" id="dd_user" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user mr-2"></i>{{ __(auth()->user()->name) }}</a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd_user">
                    <a href="{{ url('/user/'. auth()->user()->id . '/edit') }}" class="dropdown-item">{{ __('Settings') }}</a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </div>
</nav>

@include('product.create')
@include('expense.create')
@include('supplier.create')
@include('customer.create')
@include('vendor.create')
@include('purchase.create')
@include('sale.create')
@include('bill.create')
@include('payable.create')
@include('receivable.create')
@include('vendorpayment.create')
@include('employee.create')
@include('payroll.create')
@include('sale.payment')
