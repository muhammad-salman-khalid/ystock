<div class="sidebar sidebar-dark bg-dark">
    <ul class="list-unstyled">
        <li {{ (curren_page("home")) ? 'class=active' : '' }}><a href="{{ url('/home') }}"><i class="fa fa-home mr-2"></i>{{ __('Home') }}</a></li>

        <li class="dropdown open">
<a style="color:white;"><i class="fas fa-pencil-alt mr-2"></i>{{__('Products')}}</a>
<ul style="display: block ; list-style:none;     margin-left: -40px;">
<li  style="color: white; cursor : pointer;"><a data-toggle="modal" id="add-product-btn" data-target="#new-product"><i class="fas fa-plus-circle mr-2"></i>{{__('Add Products')}}</a></li>
<li {{ (curren_page("product-list")) ? 'class=active' : '' }}><a href="{{url('/product-list')}}"> <i class="far fa-arrow-alt-circle-right mr-2"></i>{{__('Products List')}}</a></li>
</ul>

</li>

  <li class="dropdown open">
<a style="color:white;"><i class="fas fa-pencil-alt mr-2"></i>{{__('Customers')}}</a>
<ul style="display: block ; list-style:none; margin-left: -40px;">
  <li style="color: white; cursor : pointer;"><a data-toggle="modal" id="add-customer-btn" data-target="#new-customer"><i class="fas fa-plus-circle mr-2"></i>{{__('Add Customers')}}</a></li>
  <li {{ (curren_page("customer-list")) ? 'class=active' : '' }}><a href="{{url('/customer-list')}}"> <i class="far fa-arrow-alt-circle-right mr-2"></i>{{__('Customers List')}}</a></li>
  <li {{ (curren_page("aging_list")) ? 'class=active' : '' }}><a href="{{url('/aging_list')}}"> <i class="far fa-arrow-alt-circle-right mr-2"></i>{{__('Customers Aging Summary')}}</a></li>
</ul>
  </li>

  <li class="dropdown open">
<a style="color:white;"><i class="fas fa-pencil-alt mr-2"></i>{{__('Suppliers')}}</a>
<ul style="display: block ; list-style:none;     margin-left: -40px;">
<li style="color: white; cursor : pointer;"><a data-toggle="modal" data-target="#new-supplier"><i class="fas fa-plus-circle mr-2"></i>{{__('Add Supplier')}}</a></li>
<li {{ (curren_page("Suppliers-list")) ? 'class=active' : '' }}><a href="{{url('/Suppliers-list')}}"> <i class="far fa-arrow-alt-circle-right mr-2"></i>{{__('Supplier List')}}</a></li>
<li {{ (curren_page("aging_list_sup")) ? 'class=active' : '' }}><a href="{{url('/aging_list_sup')}}"> <i class="far fa-arrow-alt-circle-right mr-2"></i>{{__('Supplier Aging Summary')}}</a></li>

</ul>
</li>

<li class="dropdown open">
<a style="color:white;"><i class="fas fa-pencil-alt mr-2"></i>{{__('Sales')}}</a>
<ul style="display: block ; list-style:none;     margin-left: -40px;">
<li style="color: white; cursor : pointer;"><a data-toggle="modal" data-target="#new-sale"><i class="fas fa-plus-circle mr-2"></i>{{__('Sales invoice')}}</a></li>
<li {{ (curren_page("sale")) ? 'class=active' : '' }}><a href="{{url('/sale')}}"> <i class="far fa-arrow-alt-circle-right mr-2"></i>{{__('View All Sales')}}</a></li>
</ul>
</li>
<li class="dropdown open">
<a style="color:white;"><i class="fas fa-pencil-alt mr-2"></i>{{__('Purchases')}}</a>
<ul style="display: block ; list-style:none;     margin-left: -40px;">
<li style="color: white; cursor : pointer;"><a data-toggle="modal" data-target="#new-purchase"><i class="fas fa-plus-circle mr-2"></i>{{__('New Purchase')}}</a></li>
<li {{ (curren_page("sale")) ? 'class=active' : '' }}><a href="{{url('/purchase')}}"> <i class="far fa-arrow-alt-circle-right mr-2"></i>{{__('View All Purchases')}}</a></li>
</ul>
</li>

<li class="dropdown open">
<a style="color:white;"><i class="fas fa-pencil-alt mr-2"></i>{{__('Reports')}}</a>
<ul style="display: block ; list-style:none;     margin-left: -40px;">
<li {{ (curren_page("report")) ? 'class=active' : '' }}><a href="{{url('/report')}}"> <i class="far fa-arrow-alt-circle-right mr-2"></i>{{__('All Reports')}}</a></li>
</ul>
</li>

<li class="dropdown open">
<a style="color:white;"><i class="fas fa-pencil-alt mr-2"></i>{{__('Inventory')}}</a>
<ul style="display: block ; list-style:none;     margin-left: -40px;">
<li {{ (curren_page("manage_inventory")) ? 'class=active' : '' }}><a href="{{url('/manage_inventory')}}"> <i class="far fa-arrow-alt-circle-right mr-2"></i>{{__('Manage Inventory')}}</a></li>
</ul>
</li>

    </ul>
</div>
@include('product.create')
@include('customer.create')
@include('supplier.create')
