<div class="modal fade" id="new-payable" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="font-weight-bold">{{ __('Supplier payment') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ action('PayableController@store') }}">
                    @csrf

                    <div class="form-group">
                        <label for="name">{{ __('Product name') }}</label>

                        <select name="product_id" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0" required>
                            <option value="">{{ __('Select product') }}</option>
                            @foreach ($allproducts->where('user_id', auth()->user()->id) as $product)
                                <option value="{{ $product->id }}">{{ $product->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('product_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('product_id') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="name">{{ __('To supplier') }}</label>

                        <select name="supplier_id" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0" required>
                            <option value="">{{ __('Select supplier') }}</option>
                            @foreach ($allsuppliers->where('user_id', auth()->user()->id) as $supplier)
                                <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('supplier_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('supplier_id') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="amount">{{ __('Amount') }}</label>

                        <input id="amount" type="text" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{ old('amount') }}" required>
                        @if ($errors->has('amount'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('amount') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="name">{{ __('Payment method') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                        <select name="method" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0">
                            <option value="">{{ __('Select payment method') }}</option>
                            @foreach ($paymentmethods as $m)
                                <option value="{{ $m->name }}">{{ $m->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('method'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('method') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="invoice">{{ __('Invoice') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                        <input id="invoice" type="text" class="form-control{{ $errors->has('invoice') ? ' is-invalid' : '' }}" name="invoice" value="{{ old('invoice') }}">
                        @if ($errors->has('invoice'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('invoice') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
