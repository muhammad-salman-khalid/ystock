@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card card-body">
            <h4 class="font-weight-bold">{{ __('Supplier payment') }}</h4>
            <hr>

            <form method="POST" action="{{ action('PayableController@update', ['id' => $payables->id]) }}">
                @csrf

                <input name="_method" type="hidden" value="PATCH">

                <div class="form-group">
                    <label for="name">{{ __('Product name') }}</label>

                    <select name="product_id" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0" required>
                        <option value="{{ $payables->product_id }}">{{ $payables->product->name }}</option>$payables
                        @foreach ($allproducts->where('user_id', auth()->user()->id) as $product)
                            <option value="{{ $product->id }}">{{ $product->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('product_id'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('product_id') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="name">{{ __('To supplier') }}</label>

                    <select name="supplier_id" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0" required>
                        <option value="{{ $payables->supplier_id }}">{{ $payables->supplier->name }}</option>
                        @foreach ($allsuppliers->where('user_id', auth()->user()->id) as $supplier)
                            <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('supplier_id'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('supplier_id') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="amount">{{ __('Amount') }}</label>

                    <input id="amount" type="text" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{ $payables->amount }}" required>
                    @if ($errors->has('amount'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('amount') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="name">{{ __('Payment method') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                    <select name="method" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0">
                        <option value="{{ $payables->method }}">{{ $payables->method }}</option>
                        @foreach ($paymentmethods as $m)
                            <option value="{{ $m->name }}">{{ $m->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('method'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('method') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="invoice">{{ __('Invoice') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                    <input id="invoice" type="text" class="form-control{{ $errors->has('invoice') ? ' is-invalid' : '' }}" name="invoice" value="{{ $payables->invoice }}">
                    @if ($errors->has('invoice'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('invoice') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
                    <a href="{{ url('/home') }}" class="btn btn-secondary">{{ __('Home') }}</a>
                </div>

            </form>
        </div>
    </div>
@endsection
