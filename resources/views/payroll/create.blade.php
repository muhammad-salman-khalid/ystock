<div class="modal fade" id="new-payroll" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="font-weight-bold">{{ __('Run payroll') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ action('PayrollController@store') }}">
                    @csrf

                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>{{ __('Employee name') }}</th>
                                <th>{{ __('Basic salary') }}</th>
                                <th>{{ __('Allowance') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></th>
                                <th>{{ __('Deduction') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($allemployees->where('user_id', auth()->user()->id) as $employee)
                                <tr>
                                    <td><input type="checkbox" name="checked[]" value="{{ $employee->id }}" class="mt-2"></td>
                                    <td><input type="text" class="form-control-plaintext" value="{{ $employee->name }}" readonly></td>
                                    <td><input type="text" name="basic[]" class="form-control-plaintext" value="{{ $currency->currency. $employee->basic }}" readonly></td>
                                    <td>
                                        <input type="text" class="form-control{{ $errors->has('allowance') ? ' is-invalid' : '' }}" name="allowance[]" value="{{ old('allowance') }}">
                                        @if ($errors->has('allowance'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('allowance') }}</strong>
                                            </span>
                                        @endif
                                    </td>
                                    <td>
                                        <input type="text" class="form-control{{ $errors->has('deduction') ? ' is-invalid' : '' }}" name="deduction[]" value="{{ old('deduction') }}">
                                        @if ($errors->has('deduction'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('deduction') }}</strong>
                                            </span>
                                        @endif
                                    </td>

                                    <input type="hidden" name="employee_id[]" value="{{ $employee->id }}">
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
