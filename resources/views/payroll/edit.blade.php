@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card card-body">
            <h4 class="font-weight-bold">{{ __('Edit payroll entry') }}</h4>
            <hr>

            <form method="POST" action="{{ action('PayrollController@update', ['id' => $payrolls->id]) }}">
                @csrf

                <input name="_method" type="hidden" value="PATCH">

                <div class="form-group">
                    <label for="name">{{ __('Employee Name') }}</label>

                    <input type="text" class="form-control" name="employee_id" value="{{ $payrolls->employee->name }}" disabled>
                </div>

                <div class="form-group">
                    <label for="name">{{ __('Basic salary') }}</label>

                    <input type="text" class="form-control" name="basic" value="{{ $payrolls->basic }}" disabled>
                </div>

                <div class="form-group">
                    <label for="name">{{ __('Allowance') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                    <input type="text" class="form-control" name="allowance" value="{{ $payrolls->allowance }}">
                    @if ($errors->has('allowance'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('allowance') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="name">{{ __('Deduction') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                    <input type="text" class="form-control" name="deduction" value="{{ $payrolls->deduction }}">
                    @if ($errors->has('deduction'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('deduction') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
                    <a href="{{ url('/home') }}" class="btn btn-secondary">{{ __('Home') }}</a>
                </div>

            </form>
        </div>
    </div>
@endsection
