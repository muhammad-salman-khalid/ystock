<div class="modal hide fade" id="new-product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="font-weight-bold">{{ __('Create product') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="productaddajax" onsubmit=" return submit_form_data_pro('productaddajax','addproduct');return false; "method="POST">
                    @csrf

                    <div class="form-group">
                        <label for="name">{{ __('Product Name') }}</label>

                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                        <div class="form-group">
                            <label for="procategorylist">{{ __('Product Category') }}</label><br>
                                
                                <input class="drp calistt procategorylistt" id="procategorylist" type="text" name="procategorylist" required placeholder="Choose Category">

                             
                          
                        @if ($errors->has('categorylist'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('categorylist') }}</strong>
                            </span>
                        @endif
                        </div>
                    <div class="form-group">
                        <label for="description">{{ __('Selling Price') }}</label>

                        <input id="sellingprice"  type="text" class="form-control{{ $errors->has('sellingprice') ? ' is-invalid' : '' }}" name="sellingprice" value="{{ old('sellingprice') }}">
                        @if ($errors->has('sellingprice'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('sellingprice') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="costprice">{{ __('Cost Price') }}</label>

                        <input id="costprice" onkeyup="pricevalidation();" type="text" class="form-control{{ $errors->has('costprice') ? ' is-invalid' : '' }}" name="costprice" value="{{ old('costprice') }}">
                        @if ($errors->has('costprice'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('costprice') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="description">{{ __('Product Description') }}<small class="font-italic ml-1">{{ __('(optional, max 20 char)') }}</small></label>

                        <input id="description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ old('description') }}">
                        @if ($errors->has('description'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>

                      
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                        <a href="{{URL::to('/bulkadd')}}" class="btn btn-danger" role="button" >{{ __('Bulk Add') }}</a>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
  function pricevalidation(){
    debugger;
    var selling_price = $('#sellingprice').val();
    var cost_price = $('#costprice').val();

    if (parseInt(cost_price) >= parseInt(selling_price)){
      alert("Selling Price Should be Greater than Cost Price");
       $('#costprice').val('');

    }

  }
 

   $(document).ready(function(){
       show_pcategory_list();
      
    });
function procat_autocomplete() {
        $("#procategorylist").autocomplete({
            source: prodcategorylist,
            minLength: 0,
            //                 max:5,
            //                 scroll:true,
            select: function(e, ui) {
                debugger;
                if (ui.item.value == 'add') {
                    $('a[data-target="#new-procategory"]').trigger('click');
                    //                         $('#close-btn-invoice').trigger('click');
                    $(this).val('');
                    $("#cat_name").val('');
                    $("#cat_description").val('');
                    this.setAttribute('pcategry_id', 0);
                } else {
                    var temp = getSupplier(ui.item.value);
                    // invoice_data.customer_id = ui.item.value;
                    $("#cat_name").val(temp.email);
                    $("#cat_description").val(temp.address);
                    $(this).val(ui.item.label);
                    this.setAttribute('pcategry_id', ui.item.value);
                    return false;
                }
            },
            change: function(event, ui) {
                if (!ui.item) {
                    this.value = '';
                } else {
                    // return your label here
                }
            },
            focus: function(event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            response: function(e, ui) {
                ui.content.unshift({
                    label: "Add New",
                    value: "add"
                });
            }
        }).focus(function() {
            if (this.value == "") {
                $(this).autocomplete("search");
            }
        });
    }
</script>
