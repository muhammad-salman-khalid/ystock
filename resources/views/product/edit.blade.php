@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card card-body">
            <h4 class="font-weight-bold">{{ __('Edit product') }}</h4>
            <hr>

            <form method="POST" action="{{ action('ProductController@update', ['id' => $products->id]) }}">
                @csrf

                <input name="_method" type="hidden" value="PATCH">

                <div class="form-group">
                    <label for="name">{{ __('Product Name') }}</label>

                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $products->name }}" required>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="description">{{ __('Product Description') }}<small class="font-italic ml-1">{{ __('(optional, max 20 char)') }}</small></label>

                    <input id="description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ $products->description }}">
                    @if ($errors->has('description'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="sellingprice">{{ __('SellingPrice') }}</label>

                    <input id="sellingprice" type="text" class="form-control{{ $errors->has('sellingprice') ? ' is-invalid' : '' }}" name="sellingprice" value="{{ $products->sellingprice }}">
                    @if ($errors->has('SellingPrice'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('SellingPrice') }}</strong>
                            </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="costprice">{{ __('CostPrice') }}</label>

                    <input id="costprice" type="text" class="form-control{{ $errors->has('costprice') ? ' is-invalid' : '' }}" name="costprice" value="{{ $products->costprice }}">
                    @if ($errors->has('costprice'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('costprice') }}</strong>
                            </span>
                    @endif
                </div>
                <div class="form-group">
                            <label for="procategorylist">{{ __('Product Category') }}</label><br>

                            <select id="procategorylist" class="calistt procategorylistt" name="procategorylist">
                            <option></option>
                            </select>
                        @if ($errors->has('categorylist'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('categorylist') }}</strong>
                            </span>
                        @endif
                    </div>
                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
                    <a href="{{ url('/product-list') }}" class="btn btn-secondary">{{ __('Back') }}</a>
                </div>

            </form>
        </div>
    </div>
@endsection
@section('scripts')

<script>
       $(document).ready(function(){
       show_pcategory_list();
      
    });

</script>
    </script>
    @endsection
