@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="card card-body">
        <h4 class="font-weight-bold">{{ __('Stock') }}</h4>
        <hr>
        <table class="table" id="products">
            <thead>
                <tr>
                    <th>{{ __('Product') }}</th>
                    <th>{{ __('Purchase') }}</th>
                    <th>{{ __('Sale') }}</th>
                    <th>{{ __('In Stock') }}</th>
                    <th>{{ __('Action') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                    <tr>
                        <td><a href="{{ url('/product/' . $product->id) }}">{{ $product->name }}</a></td>
                        <td>{{ $product->purchase->sum('quantity') }}</td>
                        <td>{{ $product->sale->sum('quantity') }}</td>
                        <td>{{ $product->purchase->sum('quantity') - $product->sale->sum('quantity') }}</td>
                        <td>
                            <form method="POST" action="{{ action('ProductController@destroy', ['id' => $product->id]) }}" class="confirmation">
                                <input type="hidden" name="_method" value="DELETE" />
                                @csrf
                                <a href="{{ url('/product/' . $product->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#products").DataTable();
    $(".confirmation").on("submit", function(){
        return confirm("This will delete the item. Click OK to confirm.");
    });
</script>
@endsection
