<?php

 ?>

@extends('layouts.app')

@section('content')


<div class="container-fluid">
    <div class="card card-body">
        <h4 class="font-weight-bold">{{ __('PRODUCTS LIST') }}</h4>
        <hr>
        <table class="table" id="products">
            <thead>
                <tr>
                    <th>{{ __('Id') }}</th>
                    <th>{{ __('Name') }}</th>
                    <th>{{ __('Description') }}</th>
                    <th>{{ __('Selling Price') }}</th>
                    <th>{{ __('Cost Price') }}</th>
                    <th>{{ __('Category') }}</th>
                    <th>{{ __('Action') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($list as $product)
                    <tr>
                        <td>{{$product['id']}}</td>
                        <td><a href="{{ url('/product/' . $product['id']) }}">{{$product['name']}}</td>
                        <td>{{$product['description']}}</td>
                        <td>{{$product['sellingprice']}}</td>
                        <td>{{$product['costprice']}}</td>
                        <td>{{$product['cat_name']}}</td>
                        <td>
                        <form method="POST" action="{{ action('ProductController@destroy', ['id' => $product['id']]) }}" class="confirmation">
                            <input type="hidden" name="_method" value="DELETE" />
                            @csrf
                            <a href="{{ url('/product/' . $product['id'] . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                            <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                        </form>
                      </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $("#products").DataTable({
      responsive: true,
      stateSave: true,
      dom: 'lBfrtip',
      "buttons": [
        {
        extend: 'print',
         className: "btn btn-sm btn-default",
        text: '<i class="fa fa-print"></i> Print Record',
        init: function( api, node, config) {
        $(node).removeClass('btn-default')
        },
         messageTop: false,
        footer: true,
        title:'Products List',
                 exportOptions: {
             columns: [ 0, 1, 2, 3, 4 ]
         },
        customize: function ( win ) {
            $(win.document.body)
                .css( 'font-size', '10pt' );
                // .prepend(
                //     '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                // );

            $(win.document.body).find( 'table' )
                .addClass( 'compact' )
                .css( 'font-size', 'inherit' );

                 $(win.document.body).find('table').css('width','100%');
        }

        },
        {
        extend: 'excel',
          className: "btn btn-sm btn-default",
        text: 'Export as Excel',
        init: function( api, node, config) {
        $(node).removeClass('btn-default')
        },
        messageTop: false,
        footer: true,
        title:'Products List',
        },
        {
        extend: 'pdfHtml5',
         className: "btn btn-sm btn-default",
        text: 'Export as Pdf',
        init: function( api, node, config) {
        $(node).removeClass('btn-default')
        },
        orientation: 'landscape',
        pageSize: 'A4',
        messageTop: false,
        footer: true,
        orientation: 'landscape',
        title:'Products List',
              exportOptions: {
             columns: [ 0, 1, 2, 3, 4 ]
         },
        customize: function(doc) {
             doc.styles.tableHeader.alignment = 'left';
                     doc.defaultStyle.alignment = 'left';
                  doc.content[1].table.widths =
        Array(doc.content[1].table.body[0].length + 1).join('*').split('');
        }
        },

        ],
    });
    $(".confirmation").on("submit", function(){
        return confirm("This will delete the item. Click OK to confirm.");
    });
</script>
@endsection
