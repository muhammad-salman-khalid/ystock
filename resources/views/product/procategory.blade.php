<div class="modal fade" id="new-procategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="font-weight-bold">{{ __('Create Product Category') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form onsubmit="return pro_category_form();return false;" role="form" id="pro_category_form" method="post" >
                    @csrf

                    <div class="form-group">
                        <label for="cat_name">{{ __('Category Name') }}</label>

                        <input id="cat_name" type="text" class="form-control{{ $errors->has('cat_name') ? ' is-invalid' : '' }}" name="cat_name" value="{{ old('cat_name') }}" required>
                        @if ($errors->has('cat_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('cat_name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="cat_description">{{ __('Category Description') }}<small class="font-italic ml-1">{{ __('(optional, max 20 char)') }}</small></label>

                        <input id="cat_description" type="text" class="form-control{{ $errors->has('cat_description') ? ' is-invalid' : '' }}" name="cat_description" value="{{ old('cat_description') }}">
                        @if ($errors->has('cat_description'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('cat_description') }}</strong>
                            </span>
                        @endif
                    </div>

                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                        <button type="button" id="closet" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
