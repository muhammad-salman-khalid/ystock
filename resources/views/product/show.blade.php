@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="card card-body mb-4">
            <h4 class="font-weight-bold">{{ $products->name }}</h4>
            <small>{{ $products->description }}</small>
            <?php $total_sell = $products->sale->sum('qty'); ?>
            @foreach($ranks as $key => $value)
                @if($value->product->user_id == auth()->user()->id)
                    @if($value->product_id == $products->id)
                        <?php $position = $key + 1; ?>
                        <p class="text-danger font-weight-bold my-2">
                            {{ 'This item was sold ' . $total_sell . ' times and ranks ' . $position . ' in selling.' }}
                        </p>
                    @endif
                @endif
            @endforeach
        </div>

        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">{{ __('Purchase') }}</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">{{ __('Sale') }}</a>
            </div>
        </nav>
        <div class="card card-body">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <h4 class="my-3">{{ __('Purchase history of ' . $products->name) }}</h4>
                    <hr>
                    <table class="table" id="purchases">
                        <thead>
                        <tr>
                            <th>{{ __('Date') }}</th>
                            <th>{{ __('Supplier') }}</th>
                            <th>{{ __('Quantity') }}</th>
                            <th>{{ __('Rate/product') }}</th>
                            <th>{{ __('Total') }}</th>
                            <th>{{ __('Invoice') }}</th>
                            <th>{{ __('Action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($purchases as $purchase)
                                @if($purchase->product_id == $products->id)
                                    <tr>
                                        <td>{{ date('d M, Y', strtotime($purchase->created_at)) }}</td>
                                        <td><a href="{{ url('/supplier/' . $purchase->supplier->id) }}">{{ $purchase->supplier->name }}</a></td>
                                        <td>{{ $purchase->qty }}</td>
                                        <td>{{ $currency->currency . $purchase->rate }}</td>
                                        <td>{{ $currency->currency . ($purchase->qty * $purchase->rate) }}</td>
                                        <td>{{ $purchase->invoice }}</td>
                                        <td>
                                            <form method="POST" action="{{ action('PurchaseController@destroy', ['id' => $purchase->id]) }}" class="confirmation">
                                                <input type="hidden" name="_method" value="DELETE" />
                                                @csrf
                                                <a href="{{ url('/storage/images/'.$purchase->file) }}" title=""><i class="fa fa-eye text-primary mr-2"></i></a>
                                                <a href="{{ url('/purchase/' . $purchase->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                                <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <h4 class="my-3">{{ __('Sale history of ' . $products->name) }}</h4>
                    <hr>
                    <table class="table" id="sales">
                        <thead>
                        <tr>
                            <th>{{ __('Date') }}</th>
                            <th>{{ __('Customer') }}</th>
                            <th>{{ __('Quantity') }}</th>
                            <th>{{ __('Rate/product') }}</th>
                            <th>{{ __('Total') }}</th>
                            <th>{{ __('Invoice') }}</th>
                            <th>{{ __('Action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sales as $sale)
                        <!--{{$sale}}-->
                            @if($sale->product_id == $products->id)
                                <tr>
                                    <td>{{ date('d M, Y', strtotime($sale->created_at)) }}</td>
                                    <td><a href="{{ url('/customer/' . $sale->customer->id) }}">{{ $sale->customer->name }}</a></td>
                                    <td>{{ $sale->qty }}</td>
                                    <td>{{ $currency->currency . $sale->rate }}</td>
                                    <td>{{ $currency->currency . ($sale->qty * $sale->rate) }}</td>
                                    <td>{{ $sale->invoice }}</td>
                                    <td>
                                        <form method="POST" action="{{ action('SaleController@destroy', ['id' => $sale->id]) }}" class="confirmation">
                                            <input type="hidden" name="_method" value="DELETE" />
                                            @csrf
                                            <a href="{{ url('/storage/images/'.$sale->file) }}" title=""><i class="fa fa-eye text-primary mr-2"></i></a>
                                            <a href="{{ url('/sale/' . $sale->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                            <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $("#purchases").DataTable();
        $("#sales").DataTable();
        $(".confirmation").on("submit", function(){
            return confirm("This will delete the item. Click OK to confirm.");
        });
    </script>
@endsection
