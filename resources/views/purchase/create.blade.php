<style>
.ui-autocomplete{
    z-index:9999;
}
.ui-autocomplete {
   max-height: 150px;
   overflow-y: scroll;
   overflow-x: hidden;
 }
#new-customer{
    z-index:9999;
}
.modal {
  overflow-y:auto;
}
.ui-autocomplete {
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 9999;
  float: left;
  display: none;
  min-width: 140px;
  _width: 140px;
  padding: 4px 0;
  margin: 2px 0 0 0;
  list-style: none;
  background-color: #ffffff;
  border-color: #ccc;
  border-color: rgba(0, 0, 0, 0.2);
  border-style: solid;
  border-width: 1px;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  border-radius: 5px;
  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -webkit-background-clip: padding-box;
  -moz-background-clip: padding;
  background-clip: padding-box;
  *border-right-width: 2px;
  *border-bottom-width: 2px;

  .ui-menu-item > a.ui-corner-all {
    display: block;
    padding: 3px 15px;
    clear: both;
    font-weight: normal;
    line-height: 18px;
    color: #555555;
    white-space: nowrap;

    &.ui-state-hover, &.ui-state-active {
      color: #ffffff;
      text-decoration: none;
      background-color: #0088cc;
      border-radius: 0px;
      -webkit-border-radius: 0px;
      -moz-border-radius: 0px;
      background-image: none;
    }
  }

}
#upload_prev {
	border:thin solid #000;
	width: 65%;
	padding:0.5em 1em 1.5em 1em;
}

#upload_prev span {
    display: flex;
	padding: 0 5px;
	font-size:12px;
}

p.close {
    margin: 0 5px 0 5px;
    cursor: pointer;
	padding-bottom:0;
}
</style>
<div class="modal fade" id="new-purchase" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" id="dailog_style" role="document">
        <div class="modal-content" id="content_style">
            <div class="modal-header" id="invoice-mheader">
                <a href="#"><i class="fa fa-history"></i>  </a>
			<h3 class="title">Purchase</h3>
<!--                <h4 class="font-weight-bold">{{ __('Create sale invoice') }}</h4>-->
    <button type="button" class="close" id="close-btn-invoice" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
            </div>
            <div class="modal-body">
               <div class="container-fluid">
                   <form method="POST"  onsubmit=" submit_form_data_purchase(this,'purchase-save');return false;" action="{{ action('SaleController@store') }}" enctype="multipart/form-data">
                    @csrf
  	<div class="row">
   		 <div class="col-md-8">
		<!--DROP DOWN FOR CHOOSE AND ADD NEW CUSTOMER-->
			 <div class="dropb">
    					  <input class="drp" id="supplierTags" type="text" required placeholder="Choose Supplier">

			 </div>
			 <!-- ADD EMAIL --->
			 <div class="emailblock">
			 	<div class="emaild">
                                    <input type="email" id="supplier-email" required name="email" placeholder="Enter Email">
			 	</div>
                             <input type="checkbox" name="send_later"  value="1" class="slater"> Send Later
			</div>
		</div> <!-- End of col-md-8 -->
    <div class="col-md-4" >

        <p class="totalp"><span class="balnc" >BALANCE DUE </span><br> <span class="price" >PRs <span class="bp-due">0.00</span></span></p>

	</div><!-- End of col-md-4 -->

  </div><!-- End of 1st Row -->

				<div class="row spac"> <!-- Start of 2st Row -->
					<div class="col-md-3">
						<label>Billing Address</label>
                                                <textarea cols="30" rows="4" id="supplier-address" name="customer_address"></textarea>
					</div><!-- End of 1 col-md-3 -->
					<div id="termsp" class="col-md-3">
						<label>Terms</label>
			<div class="reciept">
                            <select onchange="date()" class="Term" id="termpt" name="term">
                                            <option value="0">Pay due with Receipt</option>
                                            <option value="15">Net 15</option>
                                             <option value="30">Net 30</option>
                                        </select>
 					 		</div><!-- End of dropdown -->

					</div><!-- End of 2 col-md-3 -->
					<div class="col-md-3">

						<label>Invoice Date</label>

                                                    <input type="text"  id="invoice_pdate" class="datepicker" name="invoice_date" placeholder="mm/dd/yy">
                                                </div><!-- End of 3 col-md-3 -->

					<div class="col-md-3">
						<label>Due Date</label>

                                                <input type="text" class="datepicker" id="due_pdate" name="due_date" placeholder="mm/dd/yy">

					</div><!-- End of 4 col-md-3 -->
				</div><!-- End of 2st Row -->
			<!-- RADIO BUTTON -->
			<div class="row">
			<div class="col-md-12">
				<h2 class="title2">PAYMENT METHOD</h2>
                                <input type="radio" onclick="javascript:yesnoCheck1();"  name="yespno" id="yespCheck" value="2"> &nbsp;Partial Pay &nbsp; &nbsp;
                                <input type="radio" onclick="javascript:yesnoCheck1();" name="yespno" id="nopCheck" value="1" > &nbsp;Cash Pay&nbsp; &nbsp;
                                <input type="radio" onclick="javascript:yesnoCheck1();" name="yespno" id="nopCheck1" value="0"> &nbsp;Credit Pay
				</div>
				  <div id="ifYesp" style="visibility:hidden">
                                      Enter Details <input type="text" id="accp"  onkeyup="update_pbalance(this.value)"  name="partial_amount">
    			</div>
			</div>
						<!-- RADIO BUTTON -->

			<div class="row clearfix spac">
    	<div class="col-md-12 table-responsive">
			<table class="table table-bordered table-hover table-sortable" id="tabp_logic">
				<thead>
					<tr >
						<th class="text-center">
							#
						</th>
						<th class="text-center">
							Product/Services
						</th>
						<th class="text-center">
							Description
						</th>
						<th class="text-center">
							QTY
						</th>
    					<th class="text-center">
							Cost Price
						</th>
						<th class="text-center">
						Total
						</th>
        				<th class="text-center" >Action
						</th>

					</tr>
				</thead>
				<tbody id="tpbod">
    				<tr id="trp-0" data-id="0" class="hidden">
                                    <td data-name="id" id="tdp-0-0"><span name="id">0</span></td>
						<td data-name="product" id="tdp-0-1">
						    <input type="text" name="product0"   required class="pproductTags" placeholder="Product" class="form-control"/>
						</td>
						<td data-name="desc" id="tdp-0-2">
						    <input type="text" name="desc0" placeholder="Description" class="form-control"/>
						</td >
						<td data-name="qty" id="tdp-0-3" >
						    <input name="qty0" placeholder="Quantity" type="number"  onkeyup="get_pprice(this)" class="form-control">
						</td>
    				<td data-name="rate" id="tdp-0-4">
                                    <input name="rate0" pattern="[0-9]{10}" placeholder="Rate" type="number" onkeyup="get_pprice(this)" class="form-control">
						</td>
						<td data-name="amount" id="tdp-0-5">
                                                    <input readonly name="amount0" placeholder="" class="form-control">
						</td>
                        <td data-name="del" id="tdp-0-6">
                            <button type="button" name="del0" class="btn btn-danger row-remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </td>
					</tr>
				</tbody>
			</table>
		</div><!-- End of col 12  -->
	</div><!-- End of row for table -->
			<div class="tablebtns">
	<a id="addp_row" class="btn btn-primary pull-left btn-style">Add Row</a><!-- Button to add row  -->
	<a id="clearp_row" class="btn btn-primary pull-left btn-style">Clear Row</a><!-- Button to add row  -->
</div><!-- End of tablebtns  -->
			<div class="tabletotals">
			<p class="total" >Total &nbsp; &nbsp; PRs <span id="pp-total">0.00</span></p>
			<p class="total" >Balance Due &nbsp; &nbsp; PRs <span class="bp-due">0.00</span></p>

			</div><!-- End of TABLE TOTAL  -->
			<!-- START OF Last ROW  -->
			<div class="row spac">
			 <div class="col-sm-4">
				 <label>Message Displaying on Invoice</label>
                                 <textarea cols="30" rows="5" name="invoice_message" id="invoice_pmesage"></textarea>
				</div><!-- End of col-md-4  -->
			 <div class="col-sm-4">
				 <label>Message Displaying on Statment</label>
                                 <textarea cols="30" rows="5" name="statement_message" id="statement_pmessage"></textarea>
				</div><!-- End of col-md-4  -->
			 <div class="col-sm-4">

  					<input type="file" multiple >
  						<p>Drag your files here or click in this area.</p>
  <div id="upload_pprev" class="col-sm-6"></div>

				</div><!-- End of col-md-4  -->

			</div>
                        <div class="btnn" >
  							<button type="submit " class="btn btn-primary btn-style">Save Invoice</button>
 						 </div>
			<!-- End of LAST ROW  -->
                        </form>
</div><!-- End of container fluid -->

            </div>
        </div>
    </div>

</div>
<script>
    var newpdate;
    var invoice_pdata= {};
    var ptotal = 0;
    function get_ptotal(){
        debugger;
        ptotal = 0;
        for(var i =0 ;i <$('#tpbod tr').length;i++){

            if($('#tdp-'+i+'-'+'5').find('input').val()!='' && $('#tdp-'+i+'-'+'1').find('input').val()!=''){
                ptotal += parseInt($('#tdp-'+i+'-'+'5').find('input').val());
            }
        }
        $('#pp-total').text(ptotal);
        update_pbalance();
    }
    function get_pprice(obj){
        debugger;
       var rate = 0;
       var row = obj.parentElement.id.split('-')[1];
       if($('#tdp-'+row+'-'+'3').find('input').val()!=''  && $('#tdp-'+row+'-'+'4').find('input').val()!=''){
            rate  = parseInt($('#tdp-'+row+'-'+'3').find('input').val()) * parseInt($('#tdp-'+row+'-'+'4').find('input').val());
            $('#tdp-'+row+'-'+'5').find('input').val(rate);
            get_ptotal();
        }

    }
   function applyp_autocomplete(){
       $( ".pproductTags" ).autocomplete({
                 source: productTags,
                  minLength: 0,
                  open: function( event, ui ) {

                  },
//                max:5,
//                scroll:true,
                 select:function(e,ui){
                     debugger;
                     if(ui.item.value=='add'){
                         $('#add-pproduct-btn').trigger('click');
                        // $('#close-btn-invoice').trigger('click');
                         $("#supplier-email").val('');
                         $(this).val('');
                         this.setAttribute('product_id',0);
                     }else{
                         var temp = {};
                         $(this).val(ui.item.label);
                         this.setAttribute('product_id',ui.item.value);
                         get_ptotal();
                         return false;
                    }
                 },
                 change: function (event, ui) {
                     if (!ui.item) {
                        this.value = ''; }
                     else{
                        // return your label here
                     }
                 },
                 focus: function(event, ui){
                      event.preventDefault();
                        $(this).val(ui.item.label);
                 },
                 response: function (e, ui) {
                  ui.content.unshift(
                    {label:"Add New", value:"add"}
                  );
                }
            }).focus(function(){
        if (this.value == ""){
            $(this).autocomplete("search");
        }
    });

   }
    function getSupplier(id){
        for(var i =0;i<suppliers.length ; i++){
            if(suppliers[i].id == id){
                var temp = {};
                temp.id = suppliers[i].id;
                temp.name = suppliers[i].name;
                temp.email = suppliers[i].email;
                temp.phone = suppliers[i].phone;
                temp.address = suppliers[i].address;
                return temp;
            }
        }
        return false;
    }
    function getPproduct(id){
        for(var i =0;i<products.length ; i++){
            if(products[i].id == id){
                var temp = {};
                temp.id = products[i].id;
                temp.name = products[i].name;
                temp.costprice = products[i].costprice;
                temp.sellingprice = products[i].sellingprice;
                return temp;
            }
        }
        return false;
    }
      function supplier_autocomplete(){
            $( "#supplierTags" ).autocomplete({
                 source: supplierTags,
                 minLength: 0,
//                 max:5,
//                 scroll:true,
                 select:function(e,ui){
                     debugger;
                     if(ui.item.value=='add'){
                         $('a[data-target="#new-supplier"]').trigger('click');
//                         $('#close-btn-invoice').trigger('click');
                         $(this).val('');
                         $("#supplier-email").val('');
                         $("#supplier-address").val('');
                         this.setAttribute('supplier_id',0);
                     }else{
                         var temp = getSupplier(ui.item.value);
                        // invoice_data.customer_id = ui.item.value;
                         $("#supplier-email").val(temp.email);
                         $("#supplier-address").val(temp.address);
                         $(this).val(ui.item.label);
                         this.setAttribute('supplier_id',ui.item.value);
                         return false;
                     }
                 },
                 change: function (event, ui) {
                     if (!ui.item) {
                        this.value = ''; }
                     else{
                        // return your label here
                     }
                 },
                 focus: function(event, ui){
                      event.preventDefault();
                        $(this).val(ui.item.label);
                 },
                     response: function (e, ui) {
                      ui.content.unshift(
                        {label:"Add New", value:"add"}
                      );
                    }
            }).focus(function(){
        if (this.value == ""){
            $(this).autocomplete("search");
        }
    });
}
    $(document).ready(function (){
      $('#upload_prev').hide();
        var myDate = new Date();
        var dateString = new Date(myDate.getTime() - (myDate.getTimezoneOffset() * 60000 ))
                            .toISOString()
                            .split("T")[0];
        $('input[name=yespno]').change(update_pbalance);
            setTimeout(function(){
              applyp_autocomplete();
              supplier_autocomplete();
            },1000);
        $('#new-purchase').on("scroll", function() {
            $('input').trigger('blur');
            $('.datepicker').datepicker("hide");
        });
          $( "#invoice_pdate" ).val(dateString);
          $( "#due_pdate" ).val(dateString);
    });

   var pobj ={};
   var pobj2 ={};
   var products_data =[];
   // var customer_data ={};
   var products = [];
   var suppliers = [];
   var supplierTags = [];
   var pproductTags = [];
   var supplierTags = [];


function update_pbalance(){
   var check =  parseInt($('input[name=yespno]:checked').val());
   if(check == 2){
        value = $('#accp').val();
        var tem_value = ptotal - parseInt(value);
        if(tem_value < 0 || value == ''){
            $(".bp-due").text('0.00');

        }else{
            $(".bp-due").text(tem_value);
        }
    }else{
        $(".bp-due").text('0.00');

    }
}
$(document).ready(function() {

  $('#nopCheck').change(function(){
    debugger;

      $('#due_pdate').val('');
      if(this.checked)
          $('#termsp').hide();
      else
          $('#termsp').show();

  });
  $('#nopCheck1').change(function(){
    debugger;
          $('#termsp').show();
 $("#termpt").select2().select2("val", 0);
  });
  $('#yespCheck').change(function(){
    debugger;
          $('#termsp').show();

  });
  });
function date(){
  debugger;
  var value = $('#termpt :selected').val();
  var myDate = new Date();
//add a day to the date
if(value == 15){
myDate.setDate(myDate.getDate() + 15);
  var dateString = new Date(myDate.getTime() - (myDate.getTimezoneOffset() * 60000 ))
                      .toISOString()
                      .split("T")[0];
  console.log(dateString);
  $('#due_pdate').val(dateString);
}
else if (value == 30) {
  myDate.setDate(myDate.getDate() + 30);
    var dateString = new Date(myDate.getTime() - (myDate.getTimezoneOffset() * 60000 ))
                        .toISOString()
                        .split("T")[0];
    console.log(dateString);
    $('#due_pdate').val(dateString);


}
else if (value == 0) {
  myDate.setDate(myDate.getDate() + 0);
    var dateString = new Date(myDate.getTime() - (myDate.getTimezoneOffset() * 60000 ))
                        .toISOString()
                        .split("T")[0];
    console.log(dateString);
    $('#due_pdate').val(dateString);


}
}

$("input:file").change(function () {
  debugger;
  $('#upload_pprev').show();
    var filenames = '';
    for (var i = 0; i < this.files.length; i++) {
         $("#upload_pprev").append('<span>'+'<div class="filenameupload">' + this.files[i].name +'</div>'+'</span>');
    }
    //$("#files_show").html('<ul>' + filenames + '</ul>');
});
</script>
