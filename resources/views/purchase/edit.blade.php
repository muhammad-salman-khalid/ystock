@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card card-body">
            <h4 class="font-weight-bold">{{ __('Edit purchase order') }}</h4>
            <hr>

            <form method="POST" action="{{ action('PurchaseController@update', ['id' => $purchases->id]) }}">
                @csrf

                <input name="_method" type="hidden" value="PATCH">

                <div class="form-group">
                    <label for="name">{{ __('Product name') }}</label>

                    <select name="product_id" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0" required>
                        <option value="{{ $purchases->product_id }}">{{ $purchases->product->name }}</option>
                        @foreach ($allproducts->where('user_id', auth()->user()->id) as $product)
                            <option value="{{ $product->id }}">{{ $product->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('product_id'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('product_id') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="name">{{ __('From supplier') }}</label>

                    <select name="supplier_id" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0" required>
                        <option value="{{ $purchases->supplier_id }}">{{ $purchases->supplier->name }}</option>
                        @foreach ($allsuppliers->where('user_id', auth()->user()->id) as $supplier)
                            <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('supplier_id'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('supplier_id') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="quantity">{{ __('Quantity') }}</label>

                    <input id="quantity" type="text" class="form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}" name="quantity" value="{{ $purchases->quantity }}" required>
                    @if ($errors->has('quantity'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('quantity') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="rate">{{ __('Rate') }}</label>

                    <input id="rate" type="text" class="form-control{{ $errors->has('rate') ? ' is-invalid' : '' }}" name="rate" value="{{ $purchases->rate }}" required>
                    @if ($errors->has('rate'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('rate') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="invoice">{{ __('Invoice') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                    <input id="invoice" type="text" class="form-control{{ $errors->has('invoice') ? ' is-invalid' : '' }}" name="invoice" value="{{ $purchases->invoice }}">
                    @if ($errors->has('invoice'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('invoice') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="invoice">{{ __('Note') }}<small class="font-italic ml-1">{{ __('(optional, max 20 char)') }}</small></label>

                    <input id="invoice" type="text" class="form-control{{ $errors->has('note') ? ' is-invalid' : '' }}" name="note" value="{{ $purchases->note }}">
                    @if ($errors->has('note'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('note') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
                    <a href="{{ url('/home') }}" class="btn btn-secondary">{{ __('Home') }}</a>
                </div>

            </form>
        </div>
    </div>
@endsection
