@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="card card-body">
        <h4 class="font-weight-bold">{{ __('Sales') }}</h4>
        <hr>
        <table class="table" id="sales">
            <thead>
                <tr>
                    <th>{{ __('No.') }}</th>
                    <th>{{ __('Date') }}</th>
                    <th>{{ __('Customer') }}</th>
                    <th>{{ __('Total') }}</th>
                    <th>{{ __('Payment Recieved') }}</th>
                    <th>{{ __('Balance Due') }}</th>
                    <th>{{ __('Due Date') }}</th>
                    <th>{{ __('Note') }}</th>
                    <th>{{ __('Action') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($purchase as $sale)
                        <tr>
                            <td id="td-{{$sale->id}}-id">{{ $sale->id }}</td>
                            <td id="td-{{$sale->id}}-invoice-date" invoice_date="{{ $sale->invoiceDate }}">{{ date('d M, Y', strtotime($sale->invoiceDate)) }}</td>
                            <td id="td-{{$sale->id}}-name">{{$sale->name}}</td>
                            <td id="td-{{$sale->id}}-total">{{ $currency->currency . round($sale->total, 2) }}</td>
                            <td id="td-{{$sale->id}}-paidamount">{{ $currency->currency . round($sale->amount, 2)}}</td>
                            <td id="td-{{$sale->id}}-check">{{ $currency->currency . round($sale->total - $sale->amount, 2) }}</td>
                             <td id="td-{{$sale->id}}-due-date" due_date="{{ $sale->due_date }}">{{ date('d M, Y', strtotime($sale->due_date)) }}</td>
                            <td >{{ $sale->invoice_message }}</td>

                            <td>
                                <form method="POST" action="{{ action('SaleController@destroy', ['id' => $sale->id]) }}" class="confirmation">
                                    <input type="hidden" name="_method" value="DELETE" />
                                    @csrf

                                    <a href="{{ url('/sale/' . $sale->id . '/edit') }}" title="Edit Invoice"><i class="fa fa-pencil-alt text-success"></i></a>
                                    <button type="submit"  class="btn btn-link " title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                    @if($sale->status == 1)
                                    <a data-toggle="modal" onclick="open_model_p({{$sale->id}})"  id="add-pay-btn-{{$sale->id}}" data-target="#new-amount-p" title="Add Amount"><i class="fas fa-plus-circle mr-2"></i></a>
                                    @endif
                                </form>
                            </td>
                        </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@include('purchase.payment')
@endsection

@section('scripts')
<script>
    $("#sales").DataTable();



    $(".confirmation").on("submit", function(){
        return confirm("This will delete the item. Click OK to confirm.");
    });
    function open_model_p(obj){
      debugger;
        var invoice_date=new Date($('#td-'+obj+'-invoice-date').attr("invoice_date"));
        var due_date=new Date($('#td-'+obj+'-due-date').attr("due_date"))
        var cur_date=new Date();
        var diffDays = Math.round(Math.abs((due_date.getTime() - cur_date.getTime())/(one_day)));

        var id = parseInt($('#td-'+obj+'-id').text());
        var name = $('#td-'+obj+'-name').text();
        var total = $('#td-'+obj+'-total').text();
        var total1 = total.substring(1);
        $('#pay-no-p').val(id);
        $('#pay-sup-name').val(name);
        $('#pay-total-p').val(total1);
        $('#invoice_date1-p').val(invoice_date.getDate()+' '+months[invoice_date.getMonth()]+', '+invoice_date.getFullYear());
        $('#days_left-p').text('( Due in '+diffDays+' Days)');
        $('#due_date1-p').val(due_date.getDate()+' '+months[due_date.getMonth()]+', '+due_date.getFullYear());

    }
</script>
@endsection
