<div class="modal fade" id="new-amount-p" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="font-weight-bold">{{ __('Add Payment') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body label1">
                <form id="payment_supaddajax" onsubmit="return submit_form_data_payment_supp();return false; " method="POST">

                    <div class="row">
                    <div class="form-group col-md-6">
                             <label for="pay-sup-name">{{ __('Supplier Name') }}</label>

                        <input id="pay-sup-name" readonly="readonly" type="text" class="addinv form-control{{ $errors->has('pay-sup-name') ? ' is-invalid' : '' }}" name="customer_name" value="">
                        @if ($errors->has('pay-sup-name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('pay-sup-name') }}</strong>
                        </span>
                        @endif

                    </div>
                    <div class="form-group col-md-6">
                        <label for="pay-no-p">{{ __('Invoice Id') }}</label>

                        <input id="pay-no-p" readonly="readonly" type="text" class="addinv form-control{{ $errors->has('pay-no-p') ? ' is-invalid' : '' }}" name="invoiceid" value="" >
                        @if ($errors->has('pay-no-p'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('pay-no-p') }}</strong>
                        </span>
                        @endif
                    </div>

                    </div>
                    <div class="row">

                            <div class="form-group col-md-6">
                           <label for="pay-total-p">{{ __('Total Amount') }}</label>

                                <input id="pay-total-p" readonly="readonly" type="text" class="addinv form-control{{ $errors->has('pay-total-p') ? ' is-invalid' : '' }}" name="Total" value="" >
                                @if ($errors->has('pay-total-p'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('pay-total-p') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="add_amount">{{ __('Receive Payment') }}</label>

                                <input id="add_amount" type="text" class="form-control{{ $errors->has('add_amount') ? ' is-invalid' : '' }}" name="add_amount" value="" required>
                                @if ($errors->has('add_amount'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('add_amount') }}</strong>
                                </span>
                                @endif
                            </div>
                    </div>
                    <div class="row">
                          <div class="form-group col-md-6">
                                <label for="invoice_date1-p">{{ __('Invoice Date') }}</label>

                                <input id="invoice_date1-p" readonly="readonly" type="text" class="addinv form-control{{ $errors->has('invoice_date1-p') ? ' is-invalid' : '' }}" name="invoice_date" value="">
                                @if ($errors->has('invoice_date1-p'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('invoice_date1-p') }}</strong>
                                </span>
                                @endif
                          </div>
                                <div class="form-group col-md-4">
                                <label for="#due_date1-p">{{ __('Due Date') }}</label>

                                <input id="due_date1-p" readonly="readonly" type="text" class="addinv form-control{{ $errors->has('#due_date1-p') ? ' is-invalid' : '' }}" name="#due_date1-p" value="">

                                @if ($errors->has('#due_date1-p'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('#due_date1-p') }}</strong>
                                </span>
                                @endif

                          </div>
                        <div class="form-group col-md-2">
                                     <p id="days_left-p"> </p>
                                </div>
                    </div>
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                        <button type="button" id="paymentclose" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
