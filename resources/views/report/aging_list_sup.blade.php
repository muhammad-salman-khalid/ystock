
@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="card card-body">
        <h4 class="font-weight-bold">{{ __('Supplier Aging List') }}</h4>
        <hr>
        <table class="table" id="sales">
            <thead>
                <tr>
                    <th>{{ __(' Customer') }}</th>
                    <th>{{ __(' Total Amount') }}</th>
                    <th>{{ __(' Amount Paid') }}</th>
                    <th>{{ __(' Payables') }}</th>
                    <th>{{ __(' View Aging Summary') }}</th>
                </tr>
            </thead>
            <tbody>

                @foreach($sales as $sale)
                <tr>

                    <td><a id="td-{{$sale->id}}-name" href="{{ url('/supplier/' . $sale->cusid) }}">{{ $sale->name }}</a></td>
                    <td id="td-{{$sale->id}}-total">{{ $currency->currency . round($sale->total, 2) }}</td>
                    <td id="td-{{$sale->id}}-receivable">{{ $currency->currency . round($sale->payable, 2) }}</td>
                    <td >{{$currency->currency . round($sale->total - $sale->payable, 2)}}</td>
                    <td><a href="{{ url('/aging_sup/'.$sale->cusid) }}" title="View"><i class="fa fa-eye text-primary mr-2"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $("#sales").DataTable();

</script>
@endsection
