@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="card card-body">
        <h4 class="font-weight-bold">{{ $data[0]->name }} {{ __('Aging List') }}</h4>
        <hr>
        <table class="table" id="sales">
            <thead>
                <tr>
                    <th>{{ __('Invoice Id') }}</th>
                    <th>{{ __('Invoice Date') }}</th>
                    <th>{{ __('Due Date') }}</th>
                    <th>{{ __('Total Amount') }}</th>
                    <th>{{ __('Amount Paid') }}</th>
                    <th>{{ __('Remaining Amount') }}</th>
                    <th>{{ __('1-15') }}</th>
                    <th>{{ __('15-30') }}</th>
                    <th>{{ __('Status') }}</th>

                </tr>
            </thead>
            <tbody>
                @foreach($data as $data)
              <tr>
              <td>{{ $data->invoiceid }}</a></td>
              <td>{{ $data->invoiceDate }}</a></td>
              <td>{{ $data->due_date }}</a></td>
              <td>{{ $data->total }}</a></td>
              <td>{{ $data->paidamount }}</td>
              <td>{{ $data->total-$data->paidamount }} </a></td>
              <td>{{ $data->d15 }}</td>
              <td>{{ $data->d30 }}</td>
              <td>{{ $data->status }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('scripts')
<script>

      $(document).ready(function(){
            $("#sales").DataTable({
              columnDefs: [{targets: 8,
                render: function ( data, type, row ) {
                  var color = 'black';
                  if (data == 2) {
                    data = 'Completed';
                    color = 'green';
                  }
                  else{
                    data = 'Pending';
                    color = 'red';
                  }
                  return '<span style="color:' + color + '">' + data + '</span>';
                }
           }]
            });
      });
</script>
@endsection
