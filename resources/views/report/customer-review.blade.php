@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card card-body">
                <h4 class="text-uppercase mb-5 text-center">{{ __('Customer Review (' . date('Y') . ')') }}</h4>
                <div class="row font-weight-bold border-top py-3">
                    <div class="col-2">{{ __('Customer') }}</div>
                    <div class="col">{{ __('January') }}</div>
                    <div class="col">{{ __('February') }}</div>
                    <div class="col">{{ __('March') }}</div>
                    <div class="col">{{ __('April') }}</div>
                    <div class="col">{{ __('May') }}</div>
                    <div class="col">{{ __('June') }}</div>
                    <div class="col">{{ __('July') }}</div>
                    <div class="col">{{ __('August') }}</div>
                    <div class="col">{{ __('September') }}</div>
                    <div class="col">{{ __('October') }}</div>
                    <div class="col">{{ __('November') }}</div>
                    <div class="col">{{ __('December') }}</div>
                </div>
                @foreach($customers  as $customer)
                    <div class="row border-top py-3">
                        <div class="col-2 text-muted">{{ $customer->name }}</div>

                        <div class="col">
                            @foreach($profits  as $profit)
                                @if($profit->year == date('Y') && $profit->customer == $customer->name && $profit->month == "January")
                                    {{ $currency->currency . number_format($profit->amount, 2, '.', ',') }}
                                @endif
                            @endforeach
                        </div>

                        <div class="col">
                            @foreach($profits  as $profit)
                                @if($profit->year == date('Y') && $profit->customer == $customer->name && $profit->month == "February")
                                    {{ $currency->currency . number_format($profit->amount, 2, '.', ',') }}
                                @endif
                            @endforeach
                        </div>

                        <div class="col">
                            @foreach($profits  as $profit)
                                @if($profit->year == date('Y') && $profit->customer == $customer->name && $profit->month == "March")
                                    {{ $currency->currency . number_format($profit->amount, 2, '.', ',') }}
                                @endif
                            @endforeach
                        </div>

                        <div class="col">
                            @foreach($profits  as $profit)
                                @if($profit->year == date('Y') && $profit->customer == $customer->name && $profit->month == "April")
                                    {{ $currency->currency . number_format($profit->amount, 2, '.', ',') }}
                                @endif
                            @endforeach
                        </div>

                        <div class="col">
                            @foreach($profits  as $profit)
                                @if($profit->year == date('Y') && $profit->customer == $customer->name && $profit->month == "May")
                                    {{ $currency->currency . number_format($profit->amount, 2, '.', ',') }}
                                @endif
                            @endforeach
                        </div>

                        <div class="col">
                            @foreach($profits  as $profit)
                                @if($profit->year == date('Y') && $profit->customer == $customer->name && $profit->month == "June")
                                    {{ $currency->currency . number_format($profit->amount, 2, '.', ',') }}
                                @endif
                            @endforeach
                        </div>

                        <div class="col">
                            @foreach($profits  as $profit)
                                @if($profit->year == date('Y') && $profit->customer == $customer->name && $profit->month == "July")
                                    {{ $currency->currency . number_format($profit->amount, 2, '.', ',') }}
                                @endif
                        @endforeach
                        </div>

                        <div class="col">
                            @foreach($profits  as $profit)
                                @if($profit->year == date('Y') && $profit->customer == $customer->name && $profit->month == "August")
                                    {{ $currency->currency . number_format($profit->amount, 2, '.', ',') }}
                                @endif
                        @endforeach
                        </div>

                        <div class="col">
                            @foreach($profits  as $profit)
                                @if($profit->year == date('Y') && $profit->customer == $customer->name && $profit->month == "September")
                                    {{ $currency->currency . number_format($profit->amount, 2, '.', ',') }}
                                @endif
                            @endforeach
                        </div>

                        <div class="col">
                            @foreach($profits  as $profit)
                                @if($profit->year == date('Y') && $profit->customer == $customer->name && $profit->month == "October")
                                    {{ $currency->currency . number_format($profit->amount, 2, '.', ',') }}
                                @endif
                            @endforeach
                        </div>

                        <div class="col">
                            @foreach($profits  as $profit)
                                @if($profit->year == date('Y') && $profit->customer == $customer->name && $profit->month == "November")
                                    {{ $currency->currency . number_format($profit->amount, 2, '.', ',') }}
                                @endif
                            @endforeach
                        </div>

                        <div class="col">
                            @foreach($profits  as $profit)
                                @if($profit->year == date('Y') && $profit->customer == $customer->name && $profit->month == "December")
                                    {{ $currency->currency . number_format($profit->amount, 2, '.', ',') }}
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
