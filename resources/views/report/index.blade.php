@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card card-body">
            <h4 class="font-weight-bold">{{ __('Reports') }}</h4>
            <hr>

            <div class="row">
                <div class="col pr-5">
                    <div class="row">
                        <div class="col-2">
                            <i class="fa fa-chart-bar fa-4x text-muted"></i>
                        </div>
                        <div class="col-10">
                            <a href="{{ url('/report/profit') }}">{{ __('Profit and Loss') }}</a>
                            <p>{{ __('Shows money you earned and money you spent to see how much you profited.') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col pr-5">
                    <div class="row">
                        <div class="col-2">
                            <i class="fa fa-calendar-alt fa-4x text-muted"></i>
                        </div>
                        <div class="col-10">
                            <a href="{{ url('/report/monthly-report') }}">{{ __('Monthly Report') }}</a>
                            <p>{{ __('Shows how much you are making and how much you are spending in each month.') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col pr-5">
                    <div class="row">
                        <div class="col-2">
                            <i class="fa fa-chart-pie fa-4x text-muted"></i>
                        </div>
                        <div class="col-10">
                            <a href="{{ url('/report/product-report') }}">{{ __('Product Report') }}</a>
                            <p>{{ __('Review your overall purchase cost and sale income for each product item.') }}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col pr-5">
                    <div class="row">
                        <div class="col-2">
                            <i class="fa fa-user-tie fa-4x text-muted"></i>
                        </div>
                        <div class="col-10">
                            <a href="{{ url('/report/customer-review') }}">{{ __('Yearly Customer Review') }}</a>
                            <p>{{ __('Shows how much money you earned this year from from selling products to your customers.') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col pr-5">
                    <div class="row">
                        <div class="col-2">
                            <i class="fas fa-user fa-4x text-muted"></i>
                        </div>
                        <div class="col-10">
                            <a href="{{ url('/report/supplier-review') }}">{{ __('Yearly Supplier Review') }}</a>
                            <p>{{ __('Shows how much money you spend this year to purchase products from your suppliers.') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col pr-5">
                    <div class="row">
                        <div class="col-2">
                            <i class="far fa-user fa-4x text-muted"></i>
                        </div>
                        <div class="col-10">
                            <a href="{{ url('/report/vendor-review') }}">{{ __('Yearly Vendor Review') }}</a>
                            <p>{{ __('Shows how much money you paid this year for bills and other expenses to your vendors.') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
