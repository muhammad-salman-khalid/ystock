@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card card-body">
                <h4 class="text-uppercase mb-5 text-center">{{ __('Monthly Report') }}</h4>
                <div class="row font-weight-bold border-top py-3">
                    <div class="col">{{ __('Month') }}</div>
                    <div class="col">{{ __('Sale') }}</div>
                    <div class="col">{{ __('Purchase') }}</div>
                    <div class="col">{{ __('Expense') }}</div>
                    <div class="col">{{ __('Payroll') }}</div>
                    <div class="col">{{ __('Profit') }}</div>
                </div>
                @foreach ($allMonths as $month)
                    <?php
                        $sale_profit = 0;
                        $purchase_profit = 0;
                        $expense_profit = 0;
                        $payroll_profit = 0;
                    ?>
                    <div class="row border-top py-3">
                        <div class="col text-muted">
                               {{ $month->month . ', ' . $month->year }}
                        </div>

                        <div class="col">
                            @foreach ($sales as $sale)
                                @if(($sale->month . $sale->year) == ($month->month . $month->year))
                                    <?php $sale_profit = $sale->amount; ?>
                                        {{ $currency->currency . number_format($sale->amount, 2, '.', ',') }}
                                @endif
                            @endforeach
                        </div>

                        <div class="col">
                            @foreach ($purchases as $purchase)
                                @if(($purchase->month . $purchase->year) == ($month->month . $month->year))
                                    <?php $purchase_profit = $purchase->amount; ?>
                                    {{ $currency->currency . number_format($purchase->amount, 2, '.', ',') }}
                                @endif
                            @endforeach
                        </div>

                        <div class="col">
                            @foreach ($expenses as $expense)
                                @if(($expense->month . $expense->year) == ($month->month . $month->year))
                                    <?php $expense_profit = $expense->amount; ?>
                                    {{ $currency->currency . number_format($expense->amount, 2, '.', ',') }}
                                @endif
                            @endforeach
                        </div>

                        <div class="col">
                            @foreach ($payrolls as $payroll)
                                @if(($payroll->month . $payroll->year) == ($month->month . $month->year))
                                    <?php $payroll_profit = $payroll->amount; ?>
                                    {{ $currency->currency . number_format($payroll->amount, 2, '.', ',') }}
                                @endif
                            @endforeach
                        </div>

                        <div class="col">
                            <?php $net_profit = $sale_profit - ($purchase_profit + $expense_profit + $payroll_profit); ?>
                            {{ $currency->currency . number_format($net_profit, 2, '.', ',') }}
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection
