@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="col-md-8 col-sm-6">
            <div class="card card-body">
                <h4 class="text-uppercase mb-5 text-center">{{ __('Product Report') }}</h4>
                <div class="row font-weight-bold border-top py-3">
                    <div class="col">
                        {{ __('Product') }}
                    </div>
                    <div class="col">
                        {{ __('Purchase') }}
                    </div>
                    <div class="col">
                        {{ __('Sale') }}
                    </div>
                </div>

                @foreach($products as $value)
                    <div class="row border-top py-3">
                        <div class="col text-muted">
                            {{ $value->name }}
                        </div>
                        <div class="col">
                            @foreach($all_purchases as $p)
                                @if($p->product == $value->id)
                                    {{ $currency->currency . number_format($p->purchase, 2, '.', ',') }}
                                @endif
                            @endforeach
                        </div>
                        <div class="col">
                            @foreach($all_sales as $p)
                                @if($p->product == $value->id)
                                    {{ $currency->currency . number_format($p->sale, 2, '.', ',') }}
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection
