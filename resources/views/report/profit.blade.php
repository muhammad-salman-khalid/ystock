@extends('layouts.app')

@section('content')
    <div class="container-fluid justify-content-center text-center">
        <div class="col-md-6 col-sm-6">
            <div class="card card-body">
                <h4 class="text-uppercase mb-4">{{ __('Profit and Loss Statement') }}</h4>
                <hr>
                <div class="row">
                    <div class="col text-left text-uppercase mb-2">
                        {{ __('Income') }}
                    </div>
                    <div class="col">

                    </div>
                </div>
                <div class="row">
                    <div class="col text-left">
                        {{ __('Product Sale') }}
                    </div>
                    <div class="col text-right">
                        @foreach($sales as $p)
                            {{ $currency->currency . number_format($p->sale, 2, '.', ',') }}
                        @endforeach
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col text-left font-weight-bold">
                        {{ __('Total Income') }}
                    </div>
                    <div class="col text-right font-weight-bold">
                        <?php
                        foreach ($sales as $item) {
                            $sum_sale = $item->sale;
                        }
                        ?>
                        {{ $currency->currency . number_format($sum_sale, 2, '.', ',') }}
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col text-left text-uppercase mb-2">
                        {{ __('Expenses') }}
                    </div>
                    <div class="col">

                    </div>
                </div>
                <div class="row">
                    <div class="col text-left">
                        {{ __('Product Purchase') }}
                    </div>
                    <div class="col text-right">
                        @foreach($purchases as $p)
                            {{ $currency->currency . number_format($p->purchase, 2, '.', ',') }}
                        @endforeach
                    </div>
                </div>

                <div class="row">
                    <div class="col text-left">
                        {{ __('Bills and Office Expenses') }}
                    </div>
                    <div class="col text-right">
                        @foreach($expenses as $e)
                            {{ $currency->currency . number_format($e->expense, 2, '.', ',') }}
                        @endforeach
                    </div>
                </div>

                <div class="row">
                    <div class="col text-left">
                        {{ __('Payroll') }}
                    </div>
                    <div class="col text-right">
                        @foreach($payrolls as $pay)
                            {{ $currency->currency . number_format($pay->payroll, 2, '.', ',') }}
                        @endforeach
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col text-left font-weight-bold">
                        {{ __('Total Expense') }}
                    </div>
                    <div class="col text-right font-weight-bold">
                        <?php
                            foreach ($purchases as $item) {
                                $sum_purchase = $item->purchase;
                            }
                            foreach ($expenses as $item) {
                                $sum_expense = $item->expense;
                            }
                            foreach ($payrolls as $item) {
                                $sum_payroll = $item->payroll;
                            }
                        ?>
                        {{ $currency->currency . number_format(($sum_purchase + $sum_expense + $sum_payroll), 2, '.', ',') }}
                    </div>
                </div>
                <hr>
                <hr>
                <div class="row">
                    <div class="col text-left font-weight-bold">
                        {{ __('Net Profit') }}
                    </div>
                    <div class="col text-right font-weight-bold">
                        {{ $currency->currency . number_format(($sum_sale - ($sum_purchase + $sum_expense + $sum_payroll)), 2, '.', ',') }}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
