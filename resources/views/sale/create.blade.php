<style>
.ui-autocomplete{
    z-index:9999;
}
.ui-autocomplete {
   max-height: 150px;
   overflow-y: scroll;
   overflow-x: hidden;
 }
#new-customer{
    z-index:9999;
}
.modal {
  overflow-y:auto;
}
.ui-autocomplete {
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 9999;
  float: left;
  display: none;
  min-width: 140px;
  _width: 140px;
  padding: 4px 0;
  margin: 2px 0 0 0;
  list-style: none;
  background-color: #ffffff;
  border-color: #ccc;
  border-color: rgba(0, 0, 0, 0.2);
  border-style: solid;
  border-width: 1px;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  border-radius: 5px;
  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -webkit-background-clip: padding-box;
  -moz-background-clip: padding;
  background-clip: padding-box;
  *border-right-width: 2px;
  *border-bottom-width: 2px;

  .ui-menu-item > a.ui-corner-all {
    display: block;
    padding: 3px 15px;
    clear: both;
    font-weight: normal;
    line-height: 18px;
    color: #555555;
    white-space: nowrap;

    &.ui-state-hover, &.ui-state-active {
      color: #ffffff;
      text-decoration: none;
      background-color: #0088cc;
      border-radius: 0px;
      -webkit-border-radius: 0px;
      -moz-border-radius: 0px;
      background-image: none;
    }
  }

}
#upload_prev {
	border:thin solid #000;
	width: 65%;
	padding:0.5em 1em 1.5em 1em;
}

#upload_prev span {
    display: flex;
	padding: 0 5px;
	font-size:12px;
}

p.close {
    margin: 0 5px 0 5px;
    cursor: pointer;
	padding-bottom:0;
}
</style>
<div class="modal fade" id="new-sale" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" id="dailog_style" role="document">
        <div class="modal-content" id="content_style">
            <div class="modal-header" id="invoice-mheader">
                <a href="#"><i class="fa fa-history"></i>  </a>
			<h3 class="title">Invoice</h3>
<!--                <h4 class="font-weight-bold">{{ __('Create sale invoice') }}</h4>-->
    <button type="button" class="close" id="close-btn-invoice" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
            </div>
            <div class="modal-body">
               <div class="container-fluid">
                   <form method="POST"  onsubmit="submit_form_data(this,'sale-save');return false;" action="{{ action('SaleController@store') }}" enctype="multipart/form-data">
                    @csrf
  	<div class="row">
   		 <div class="col-md-8">
		<!--DROP DOWN FOR CHOOSE AND ADD NEW CUSTOMER-->
			 <div class="dropb">
    					  <input class="drp" id="customerTags" type="text" required placeholder="Choose Customer">

			 </div>
			 <!-- ADD EMAIL --->
			 <div class="emailblock">
			 	<div class="emaild">
                                    <input type="email" id="cus-email" required name="email" placeholder="Enter Email">
			 	</div>
                             <input type="checkbox" name="send_later"  value="1" class="slater"> Send Later
			</div>
		</div> <!-- End of col-md-8 -->
    <div class="col-md-4" >

        <p class="totalp"><span class="balnc" >BALANCE DUE </span><br> <span class="price" >PRs <span class="b-due">0.00</span></span></p>

	</div><!-- End of col-md-4 -->

  </div><!-- End of 1st Row -->

				<div class="row spac"> <!-- Start of 2st Row -->
					<div class="col-md-3">
						<label>Billing Address</label>
                                                <textarea cols="30" rows="4" id="cus-address" name="customer_address"></textarea>
					</div><!-- End of 1 col-md-3 -->
					<div id="terms" class="col-md-3">
						<label>Terms</label>
			<div class="reciept">
                            <select onchange="date()" class="Term" id="termt" name="term">
                                            <option value="0">Pay due with Receipt</option>
                                            <option value="15">Net 15</option>
                                             <option value="30">Net 30</option>
                                        </select>
 					 		</div><!-- End of dropdown -->

					</div><!-- End of 2 col-md-3 -->
					<div class="col-md-3">

						<label>Invoice Date</label>

                                                    <input type="text"  id="invoice_date" class="datepicker" name="invoice_date" placeholder="mm/dd/yy">
                                                </div><!-- End of 3 col-md-3 -->

					<div class="col-md-3">
						<label>Due Date</label>

                                                <input type="text" class="datepicker" id="due_date" name="due_date" placeholder="mm/dd/yy">

					</div><!-- End of 4 col-md-3 -->
				</div><!-- End of 2st Row -->
			<!-- RADIO BUTTON -->
			<div class="row">
			<div class="col-md-12">
				<h2 class="title2">PAYMENT METHOD</h2>
                                <input type="radio" onclick="javascript:yesnoCheck();"  name="yesno" id="yesCheck" value="2"> &nbsp;Partial Pay &nbsp; &nbsp;
                                <input type="radio" onclick="javascript:yesnoCheck();" name="yesno" id="noCheck" value="1" > &nbsp;Cash Pay&nbsp; &nbsp;
                                <input type="radio" onclick="javascript:yesnoCheck();" name="yesno" id="noCheck1" value="0"> &nbsp;Credit Pay
				</div>
				  <div id="ifYes" style="visibility:hidden">
                                      Enter Details <input type="text" id="acc"  onkeyup="update_balance(this.value)"  name="partial_amount">
    			</div>
			</div>
						<!-- RADIO BUTTON -->

			<div class="row clearfix spac">
    	<div class="col-md-12 table-responsive">
			<table class="table table-bordered table-hover table-sortable" id="tab_logic">
				<thead>
					<tr >
						<th class="text-center">
							#
						</th>
						<th class="text-center">
							Product/Services
						</th>
						<th class="text-center">
							Description
						</th>
						<th class="text-center">
							QTY
						</th>
    					<th class="text-center">
							Rate
						</th>
						<th class="text-center">
							Amount
						</th>
        				<th class="text-center" >Action
						</th>

					</tr>
				</thead>
				<tbody id="tbod">
    				<tr id="tr-0" data-id="0" class="hidden">
                                    <td data-name="id" id="td-0-0"><span name="id">0</span></td>
						<td data-name="product" id="td-0-1">
						    <input type="text" name="product0"   required class="productTags" placeholder="Product" class="form-control"/>
						</td>
						<td data-name="desc" id="td-0-2">
						    <input type="text" name="desc0" placeholder="Description" class="form-control"/>
						</td >
						<td data-name="qty" id="td-0-3" >
						    <input name="qty0" placeholder="Quantity" type="number" onkeyup="get_price(this)" class="form-control">
						</td>
    				<td data-name="rate" id="td-0-4">
                                    <input name="rate0" pattern="[0-9]{10}" placeholder="Rate" type="number" onkeyup="get_price(this)" class="form-control">
						</td>
						<td data-name="amount" id="td-0-5">
                                                    <input readonly="readonly" name="amount0" type="number" placeholder="Amount" class="form-control">
						</td>
                        <td data-name="del" id="td-0-6">
                            <button type="button" name="del0" class="btn btn-danger row-remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </td>
					</tr>
				</tbody>
			</table>
		</div><!-- End of col 12  -->
	</div><!-- End of row for table -->
			<div class="tablebtns">
	<a id="add_row" class="btn btn-primary pull-left btn-style">Add Row</a><!-- Button to add row  -->
	<a id="clear_row" class="btn btn-primary pull-left btn-style">Clear Row</a><!-- Button to add row  -->
</div><!-- End of tablebtns  -->
			<div class="tabletotals">
			<p class="total" >Total &nbsp; &nbsp; PRs <span id="p-total">0.00</span></p>
			<p class="total" >Balance Due &nbsp; &nbsp; PRs <span class="b-due">0.00</span></p>

			</div><!-- End of TABLE TOTAL  -->
			<!-- START OF Last ROW  -->
			<div class="row spac">
			 <div class="col-sm-4">
				 <label>Message Displaying on Invoice</label>
                                 <textarea cols="30" rows="5" name="invoice_message"></textarea>
				</div><!-- End of col-md-4  -->
			 <div class="col-sm-4">
				 <label>Message Displaying on Statment</label>
                                 <textarea cols="30" rows="5" name="statement_message"></textarea>
				</div><!-- End of col-md-4  -->
			 <div class="col-sm-4">

  					<input type="file" multiple >
  						<p>Drag your files here or click in this area.</p>
  <div id="upload_prev" class="col-sm-6"></div>

				</div><!-- End of col-md-4  -->

			</div>
                        <div class="btnn" >
  							<button type="submit " class="btn btn-primary btn-style">Save Invoice</button>
 						 </div>
			<!-- End of LAST ROW  -->
                        </form>
</div><!-- End of container fluid -->
<!--                <form method="POST" action="{{ action('SaleController@store') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <label for="name">{{ __('Product name') }}</label>

                        <select name="product_id" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0" required>
                            <option value="">{{ __('Select product') }}</option>
                            @foreach ($allproducts->where('user_id', auth()->user()->id) as $product)
                                <option value="{{ $product->id }}">{{ $product->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('product_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('product_id') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="name">{{ __('To customer') }}</label>

                        <select name="customer_id" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0" required>
                            <option value="">{{ __('Select customer') }}</option>
                            @foreach ($allcustomers->where('user_id', auth()->user()->id) as $customer)
                                <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('customer_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('customer_id') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="quantity">{{ __('Quantity') }}</label>

                        <input id="quantity" type="text" class="form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}" name="quantity" value="{{ old('quantity') }}" required>
                        @if ($errors->has('quantity'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('quantity') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="rate">{{ __('Rate') }}</label>

                        <input id="rate" type="text" class="form-control{{ $errors->has('rate') ? ' is-invalid' : '' }}" name="rate" value="{{ old('rate') }}" required>
                        @if ($errors->has('rate'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('rate') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="invoice">{{ __('Invoice') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                        <input id="invoice" type="text" class="form-control{{ $errors->has('invoice') ? ' is-invalid' : '' }}" name="invoice" value="{{ old('invoice') }}">
                        @if ($errors->has('invoice'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('invoice') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="invoice">{{ __('Note') }}<small class="font-italic ml-1">{{ __('(optional, max 20 char)') }}</small></label>

                        <input id="note" type="text" class="form-control{{ $errors->has('note') ? ' is-invalid' : '' }}" name="note" value="{{ old('note') }}">
                        @if ($errors->has('note'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('note') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="name">{{ __('Upload file') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                        <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                            <input type="file" name="file" id="file">
                            @if ($errors->has('file'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    </div>

                </form>-->
            </div>
        </div>
    </div>

</div>
<script>
    var newdate;
    var invoice_data= {};
    var total = 0;
    function get_total(){
        debugger;
        total = 0;
        for(var i =0 ;i <$('#tbod tr').length;i++){
            if($('#td-'+i+'-'+'5').find('input').val()!='' && $('#td-'+i+'-'+'1').find('input').val()!=''){
                total += parseInt($('#td-'+i+'-'+'5').find('input').val());
            }
        }
        $('#p-total').text(total);
        update_balance();
    }
    function get_price(obj){
        debugger;
       var rate = 0;
       var row = obj.parentElement.id.split('-')[1];
       if($('#td-'+row+'-'+'3').find('input').val()!=''  && $('#td-'+row+'-'+'4').find('input').val()!=''){
            rate  = parseInt($('#td-'+row+'-'+'3').find('input').val()) * parseInt($('#td-'+row+'-'+'4').find('input').val());
            $('#td-'+row+'-'+'5').find('input').val(rate);
            get_total();
        }

    }
   function apply_autocomplete(){
       $( ".productTags" ).autocomplete({
                 source: productTags,
                  minLength: 0,
                  open: function( event, ui ) {

                  },
//                max:5,
//                scroll:true,
                 select:function(e,ui){
                     debugger;
                     if(ui.item.value=='add'){
                         $('#add-product-btn').trigger('click');
                        // $('#close-btn-invoice').trigger('click');
                         $("#cus-email").val('');
                         $(this).val('');
                         this.setAttribute('product_id',0);
                     }else{
                         var temp = {};
                         $(this).val(ui.item.label);
                         this.setAttribute('product_id',ui.item.value);
                         get_total();
                         return false;
                    }
                 },
                 change: function (event, ui) {
                     if (!ui.item) {
                        this.value = ''; }
                     else{
                        // return your label here
                     }
                 },
                 focus: function(event, ui){
                      event.preventDefault();
                        $(this).val(ui.item.label);
                 },
                 response: function (e, ui) {
                  ui.content.unshift(
                    {label:"Add New", value:"add"}
                  );
                }
            }).focus(function(){
        if (this.value == ""){
            $(this).autocomplete("search");
        }
    });

   }
    function getCustomer(id){
        for(var i =0;i<customers.length ; i++){
            if(customers[i].id == id){
                var temp = {};
                temp.id = customers[i].id;
                temp.name = customers[i].name;
                temp.email = customers[i].email;
                temp.phone = customers[i].phone;
                temp.address = customers[i].address;
                return temp;
            }
        }
        return false;
    }
    function getProduct(id){
        for(var i =0;i<products.length ; i++){
            if(products[i].id == id){
                var temp = {};
                temp.id = products[i].id;
                temp.name = products[i].name;
                temp.costprice = products[i].costprice;
                temp.sellingprice = products[i].sellingprice;
                return temp;
            }
        }
        return false;
    }
      function customer_autocomplete(){
            $( "#customerTags" ).autocomplete({
                 source: customerTags,
                 minLength: 0,
//                 max:5,
//                 scroll:true,
                 select:function(e,ui){
                     debugger;
                     if(ui.item.value=='add'){
                         $('#add-customer-btn').trigger('click');
//                         $('#close-btn-invoice').trigger('click');
                         $(this).val('');
                         $("#cus-email").val('');
                         $("#cus-address").val('');
                         this.setAttribute('customer_id',0);
                     }else{
                         var temp = getCustomer(ui.item.value);
                        // invoice_data.customer_id = ui.item.value;
                         $("#cus-email").val(temp.email);
                         $("#cus-address").val(temp.address);
                         $(this).val(ui.item.label);
                         this.setAttribute('customer_id',ui.item.value);
                         return false;
                     }
                 },
                 change: function (event, ui) {
                     if (!ui.item) {
                        this.value = ''; }
                     else{
                        // return your label here
                     }
                 },
                 focus: function(event, ui){
                      event.preventDefault();
                        $(this).val(ui.item.label);
                 },
                     response: function (e, ui) {
                      ui.content.unshift(
                        {label:"Add New", value:"add"}
                      );
                    }
            }).focus(function(){
        if (this.value == ""){
            $(this).autocomplete("search");
        }
    });
}
    $(document).ready(function (){
      $('#upload_prev').hide();
        var myDate = new Date();
        var dateString = new Date(myDate.getTime() - (myDate.getTimezoneOffset() * 60000 ))
                            .toISOString()
                            .split("T")[0];
        $('input[name=yesno]').change(update_balance);
            setTimeout(function(){
            apply_autocomplete();
            customer_autocomplete();
        },1000);
        $('#new-sale').on("scroll", function() {
            $('input').trigger('blur');
            $('.datepicker').datepicker("hide");
        });
          $( "#invoice_date" ).val(dateString);
          $( "#due_date" ).val(dateString);
    });

   var obj ={};
   var obj2 ={};
   var products_data =[];
   var customer_data ={};
   var products = [];
   var productTags = [];
   var customerTags = [];
   var customers = [];
   @foreach ($allcustomers->where('user_id', auth()->user()->id) as $customer)
    obj ={};
    obj.id = {{ $customer->id }};
    obj.name ='{{ $customer->name }}';
    obj.email ='{{ $customer->email }}';
    obj.phone ='{{ $customer->phone }}';
    obj.address ='{{ $customer->address }}';
    customers.push(obj);
   @endforeach

function update_balance(){
   var check =  parseInt($('input[name=yesno]:checked').val());
   if(check == 2){
        value = $('#acc').val();
        var tem_value = total - parseInt(value);
        if(tem_value < 0 || value == ''){
            $(".b-due").text('0.00');

        }else{
            $(".b-due").text(tem_value);
        }
    }else{
        $(".b-due").text('0.00');

    }
}
$(document).ready(function() {

  $('#noCheck').change(function(){
    debugger;

      $('#due_date').val('');
      if(this.checked)
          $('#terms').hide();
      else
          $('#terms').show();

  });
  $('#noCheck1').change(function(){
    debugger;
          $('#terms').show();
 $("#termt").select2().select2("val", 0);
  });
  $('#yesCheck').change(function(){
    debugger;
          $('#terms').show();

  });
  });
function date(){
  debugger;
  var value = $('#termt :selected').val();
  var myDate = new Date();
//add a day to the date
if(value == 15){
myDate.setDate(myDate.getDate() + 15);
  var dateString = new Date(myDate.getTime() - (myDate.getTimezoneOffset() * 60000 ))
                      .toISOString()
                      .split("T")[0];
  console.log(dateString);
  $('#due_date').val(dateString);
}
else if (value == 30) {
  myDate.setDate(myDate.getDate() + 30);
    var dateString = new Date(myDate.getTime() - (myDate.getTimezoneOffset() * 60000 ))
                        .toISOString()
                        .split("T")[0];
    console.log(dateString);
    $('#due_date').val(dateString);


}
else if (value == 0) {
  myDate.setDate(myDate.getDate() + 0);
    var dateString = new Date(myDate.getTime() - (myDate.getTimezoneOffset() * 60000 ))
                        .toISOString()
                        .split("T")[0];
    console.log(dateString);
    $('#due_date').val(dateString);


}
}

$("input:file").change(function () {
  debugger;
  $('#upload_prev').show();
    var filenames = '';
    for (var i = 0; i < this.files.length; i++) {
         $("#upload_prev").append('<span>'+'<div class="filenameupload">' + this.files[i].name +'</div>'+'</span>');
    }
    //$("#files_show").html('<ul>' + filenames + '</ul>');
});
</script>
