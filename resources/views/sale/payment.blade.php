<div class="modal fade" id="new-amount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="font-weight-bold">{{ __('Add Payment') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body label1">
                <form id="paymentaddajax" onsubmit="return submit_form_data_payment();return false; " method="POST">

                    <div class="row">
                    <div class="form-group col-md-6">
                             <label for="pay-cus-name">{{ __('Customer Name') }}</label>

                        <input id="pay-cus-name" readonly="readonly" type="text" class="addinv form-control{{ $errors->has('pay-cus-name') ? ' is-invalid' : '' }}" name="customer_name" value="">
                        @if ($errors->has('pay-cus-name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('pay-cus-name') }}</strong>
                        </span>
                        @endif

                    </div>
                    <div class="form-group col-md-6">
                        <label for="pay-no">{{ __('Invoice Id') }}</label>

                        <input id="pay-no" readonly="readonly" type="text" class="addinv form-control{{ $errors->has('pay-no') ? ' is-invalid' : '' }}" name="invoiceid" value="" >
                        @if ($errors->has('pay-no'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('pay-no') }}</strong>
                        </span>
                        @endif
                    </div>

                    </div>
                    <div class="row">

                            <div class="form-group col-md-6">
                           <label for="pay-total">{{ __('Total Amount') }}</label>

                                <input id="pay-total" readonly="readonly" type="text" class="addinv form-control{{ $errors->has('pay-total') ? ' is-invalid' : '' }}" name="Total" value="" >
                                @if ($errors->has('pay-total'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('pay-total') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="add_amount">{{ __('Receive Payment') }}</label>

                                <input id="add_amount" type="text" class="form-control{{ $errors->has('add_amount') ? ' is-invalid' : '' }}" name="add_amount" value="" required>
                                @if ($errors->has('add_amount'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('add_amount') }}</strong>
                                </span>
                                @endif
                            </div>
                    </div>
                    <div class="row">
                          <div class="form-group col-md-6">
                                <label for="invoice_date1">{{ __('Invoice Date') }}</label>

                                <input id="invoice_date1" readonly="readonly" type="text" class="addinv form-control{{ $errors->has('invoice_date1') ? ' is-invalid' : '' }}" name="invoice_date" value="">
                                @if ($errors->has('invoice_date1'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('invoice_date1') }}</strong>
                                </span>
                                @endif
                          </div>
                                <div class="form-group col-md-4">
                                <label for="#due_date1">{{ __('Due Date') }}</label>

                                <input id="due_date1" readonly="readonly" type="text" class="addinv form-control{{ $errors->has('#due_date1') ? ' is-invalid' : '' }}" name="#due_date1" value="">

                                @if ($errors->has('#due_date1'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('#due_date1') }}</strong>
                                </span>
                                @endif

                          </div>
                        <div class="form-group col-md-2">
                                     <p id="days_left"> </p>
                                </div>
                    </div>
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                        <button type="button" id="paymentclose" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
