@extends('layouts.app')
@section('content')

<div class="container">
  <h2>Add Bulk Suppliers</h2>
  <table class="table table-bordered table-hover table-sortable" id="suppliertable">
    <thead>
      <tr>
        <th>Name</th>
        <th>E-Mail</th>
        <th>Address</th>
        <th>Phone</th>
        <th>Remove</th>

      </tr>
    </thead>
    <tbody>
      <tr>
        <td><input type="text" placeholder="Name" class="form-control" style="width: 70%" ></td>
        <td><input type="text" placeholder="E-mail" class="form-control" style="width: 70%" ></td>
        <td><input type="text" placeholder="Address" class="form-control" style="width: 70%" ></td>
        <td><input type="text" placeholder="Phone" class="form-control" style="width: 70%" ></td>
        <td><button id="del" class="btn btn-danger" >
  <i class="fas fa-times" style="font-size: 20px;padding-top: 3px;"></i></button></td>
      </tr>
    </tbody>
  </table>
<div class="container">
  <div class="row">
  <div class="col-md-6">
<button type="button" class="btn btn-danger" id="addrows">Add Row</button>
</div>
<div class="col-md-6">
  <button type="button" class="btn btn-primary" id="save">Save Products</button>
</div>
</div>
</div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function(){
  $("#addrows").click(function(){
     $('#suppliertable').append('<tr><td><input type="text" placeholder="Name" class="form-control" style="width: 70%" ></td><td><input type="text" placeholder="E-mail" class="form-control" style="width: 70%" ></td><td><input type="text" placeholder="Address" class="form-control" style="width: 70%" ></td><td><input type="text" placeholder="Phone" class="form-control" style="width: 70%" ></td><td><button id="del" class="btn btn-danger" ><i class="fas fa-times" style="font-size: 20px;padding-top: 3px;"></i></button></td></tr>');
     });

       $("#save").click(function(){
add_table();
// save_table_data();
});

$('#suppliertable').on('click', '#del', function(){
    $(this).closest ('tr').remove ();
});

var myobjcart = 0;
 var cart = {};
 cart.supplier =[];
 function add_table(){
     debugger;
      cart.supplier =[];
        var table  = $('#suppliertable tbody')[0].children;
     console.log(table);
      for(var key = 0 ;key<table.length;key++){
          var obj = {};
          console.log($(this));

             obj.name  = table[key].cells[0].children[0].value;
            obj.email =table[key].cells[1].children[0].value;
            obj.address =table[key].cells[2].children[0].value;
            obj.phone =table[key].cells[3].children[0].value;
cart.supplier.push(obj);
      }

      myobjcart = cart;

      $.ajax({
        type: "POST",
        url : "savebulksupplier",
        data :{ "_token": "{{ csrf_token() }}","pro":myobjcart },
         "dataType": "json",
        success:function(response){
          debugger;

          if(response == 1){
          swal("Customers Added!", "", "success");
          window.setTimeout(function() {
      window.location.href = "/Suppliers-list";
}, 2000);
        }

        },

      });
 }


 });

</script>
@endsection
