@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="card card-body">
        <h4 class="font-weight-bold">{{ __('Suppliers') }}</h4>
        <hr>
        <table class="table" id="suppliers">
            <thead>
                <tr>
                    <th>{{ __('Name') }}</th>
                    <th>{{ __('Payable') }}</th>
                    <th>{{ __('Paid') }}</th>
                    <th>{{ __('Due') }}</th>
                    <th>{{ __('Action') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($suppliers as $supplier)

                    <?php
                        $payable = 0;
                        $total = 0;

                        foreach ($supplier->purchase as $p)
                        {
                            $payable = $p->quantity * $p->rate;
                            $total += $payable;
                        }
                    ?>
                    <tr>
                        <td><a href="{{ url('/supplier/' . $supplier->id) }}">{{ $supplier->name }}</a></td>
                        <td>{{ $currency->currency . $total }}</td>
                        <td>{{ $currency->currency . $supplier->payable->sum('amount') }}</td>
                        <td>{{ $currency->currency . ($total - $supplier->payable->sum('amount')) }}</td>
                        <td>
                            <form method="POST" action="{{ action('SupplierController@destroy', ['id' => $supplier->id]) }}" class="confirmation">
                                <input type="hidden" name="_method" value="DELETE" />
                                @csrf
                                <a href="{{ url('/supplier/' . $supplier->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#suppliers").DataTable();
    $(".confirmation").on("submit", function(){
        return confirm("This will delete the item. Click OK to confirm.");
    });
</script>
@endsection
