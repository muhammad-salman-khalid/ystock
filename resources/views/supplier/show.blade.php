@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card card-body mb-4">
            <h4 class="font-weight-bold">{{ $suppliers->name }}</h4>
            <hr>
            <p><span class="text-muted">{{ __('Email: ') }}</span>{{ $suppliers->email }}</p>
            <p><span class="text-muted">{{ __('Phone: ') }}</span>{{ $suppliers->phone }}</p>
            <p><span class="text-muted">{{ __('Address: ') }}</span>{{ $suppliers->address }}</p>
        </div>

        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">{{ __('Purchase') }}</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">{{ __('Payment') }}</a>
            </div>
        </nav>
        <div class="card">
            <div class="card-body">
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <h4 class="my-3">{{ __('Purchase history from ' . $suppliers->name) }}</h4>
                        <hr>
                        <table class="table" id="purchases">
                            <thead>
                            <tr>
                                <th>{{ __('Date') }}</th>
                                <th>{{ __('Product') }}</th>
                                <th>{{ __('Quantity') }}</th>
                                <th>{{ __('Rate') }}</th>
                                <th>{{ __('Total') }}</th>
                                <th>{{ __('Invoice') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($purchases as $purchase)
                                @if($purchase->supplier_id == $suppliers->id)
                                    <tr>
                                        <td>{{ date('d M, Y', strtotime($purchase->created_at)) }}</td>
                                        <td><a href="{{ url('/product/' . $purchase->product->id) }}">{{ $purchase->product->name }}</a></td>
                                        <td>{{ $purchase->quantity }}</td>
                                        <td>{{ $currency->currency . $purchase->rate }}</td>
                                        <td>{{ $currency->currency . number_format(($purchase->quantity * $purchase->rate), 2, '.', ',') }}</td>
                                        <td>{{ $purchase->invoice }}</td>
                                        <td>
                                            <form method="POST" action="{{ action('PurchaseController@destroy', ['id' => $purchase->id]) }}" class="confirmation">
                                                <input type="hidden" name="_method" value="DELETE" />
                                                @csrf
                                                <a href="{{ url('/purchase/' . $purchase->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                                <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <h4 class="my-3">{{ __('Payment history to ' . $suppliers->name) }}</h4>
                        <hr>
                        <table class="table" id="payable">
                            <thead>
                                <tr>
                                    <th>{{ __('Date') }}</th>
                                    <th>{{ __('Product') }}</th>
                                    <th>{{ __('Amount') }}</th>
                                    <th>{{ __('Method') }}</th>
                                    <th>{{ __('Invoice') }}</th>
                                    <th>{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($payables as $payable)
                                    @if($payable->supplier_id == $suppliers->id)
                                        <tr>
                                            <td>{{ date('d M, Y', strtotime($payable->created_at)) }}</td>
                                            <td><a href="{{ url('/product/' . $payable->product->id) }}">{{ $payable->product->name }}</a></td>
                                            <td>{{ $currency->currency . $payable->amount }}</td>
                                            <td>{{ $payable->method }}</td>
                                            <td>{{ $payable->invoice }}</td>
                                            <td>
                                                <form method="POST" action="{{ action('PayableController@destroy', ['id' => $payable->id]) }}" class="confirmation">
                                                    <input type="hidden" name="_method" value="DELETE" />
                                                    @csrf
                                                    <a href="{{ url('/payable/' . $payable->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                                    <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $("#purchases").DataTable();
          $("#payable").DataTable();
        $(".confirmation").on("submit", function(){
            return confirm("This will delete the item. Click OK to confirm.");
        });
    </script>
@endsection
