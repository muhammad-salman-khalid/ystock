@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="card card-body">
        <h4 class="font-weight-bold">{{ __('Vendors') }}</h4>
        <hr>
        <table class="table" id="vendors">
            <thead>
                <tr>
                    <th>{{ __('Name') }}</th>
                    <th>{{ __('Payable') }}</th>
                    <th>{{ __('Paid') }}</th>
                    <th>{{ __('Due') }}</th>
                    <th>{{ __('Action') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($vendors as $vendor)
                    <tr>
                        <td><a href="{{ url('/vendor/' . $vendor->id) }}">{{ $vendor->name }}</a></td>
                        <td>{{ $currency->currency . number_format($vendor->bill->sum('amount'), 2, '.', ',') }}</td>
                        <td>{{ $currency->currency . number_format($vendor->vendorpayable->sum('amount'), 2, '.', ',') }}</td>
                        <td>{{ $currency->currency . number_format(($vendor->bill->sum('amount') - $vendor->vendorpayable->sum('amount')), 2, '.', ',') }}</td>
                        <td>
                            <form method="POST" action="{{ action('VendorController@destroy', ['id' => $vendor->id]) }}" class="confirmation">
                                <input type="hidden" name="_method" value="DELETE" />
                                @csrf
                                <a href="{{ url('/vendor/' . $vendor->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#vendors").DataTable();
    $(".confirmation").on("submit", function(){
        return confirm("This will delete the item. Click OK to confirm.");
    });
</script>
@endsection
