@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card card-body mb-4">
            <h4 class="font-weight-bold">{{ $vendors->name }}</h4>
            <hr>
            <p><span class="text-muted">{{ __('Email: ') }}</span>{{ $vendors->email }}</p>
            <p><span class="text-muted">{{ __('Phone: ') }}</span>{{ $vendors->phone }}</p>
            <p><span class="text-muted">{{ __('Address: ') }}</span>{{ $vendors->address }}</p>
        </div>

        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">{{ __('Bill') }}</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">{{ __('Payment') }}</a>
            </div>
        </nav>
        <div class="card">
            <div class="card-body">
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <h4 class="my-3">{{ __('Billing from ' . $vendors->name) }}</h4>
                        <hr>
                        <table class="table" id="bills">
                            <thead>
                            <tr>
                                <th>{{ __('Date') }}</th>
                                <th>{{ __('Expense') }}</th>
                                <th>{{ __('Amount') }}</th>
                                <th>{{ __('Invoice') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($bills as $bill)
                                @if($bill->vendor_id == $vendors->id)
                                    <tr>
                                        <td>{{ date('d M, Y', strtotime($bill->created_at)) }}</td>
                                        <td><a href="{{ url('/expense/' . $bill->expense->id) }}">{{ $bill->expense->name }}</a></td>
                                        <td>{{ $currency->currency . $bill->amount }}</td>
                                        <td>{{ $bill->invoice }}</td>
                                        <td>
                                            <form method="POST" action="{{ action('BillController@destroy', ['id' => $bill->id]) }}" class="confirmation">
                                                <input type="hidden" name="_method" value="DELETE" />
                                                @csrf
                                                <a href="{{ url('/bill/' . $bill->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                                <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <h4 class="my-3">{{ __('Payments to ' . $vendors->name) }}</h4>
                        <hr>
                        <table class="table" id="payable">
                            <thead>
                            <tr>
                                <th>{{ __('Date') }}</th>
                                <th>{{ __('Expense') }}</th>
                                <th>{{ __('Amount') }}</th>
                                <th>{{ __('Method') }}</th>
                                <th>{{ __('Invoice') }}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($vendorpayables as $payable)
                                @if($payable->vendor_id == $vendors->id)
                                    <tr>
                                        <td>{{ date('d M, Y', strtotime($payable->created_at)) }}</td>
                                        <td><a href="{{ url('/expense/' . $payable->expense->id) }}">{{ $payable->expense->name }}</a></td>
                                        <td>{{ $currency->currency . $payable->amount }}</td>
                                        <td>{{ $payable->method }}</td>
                                        <td>{{ $payable->invoice }}</td>
                                        <td>
                                            <form method="POST" action="{{ action('VendorPayableController@destroy', ['id' => $payable->id]) }}" class="confirmation">
                                                <input type="hidden" name="_method" value="DELETE" />
                                                @csrf
                                                <a href="{{ url('/vendorpayment/' . $payable->id . '/edit') }}"><i class="fa fa-pencil-alt text-success"></i></a>
                                                <button type="submit" class="btn btn-link btn-sm" title="Delete"><i class="fas fa-times-circle text-danger"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $("#bills").DataTable();
        $("#payable").DataTable();
        $(".confirmation").on("submit", function(){
            return confirm("This will delete the item. Click OK to confirm.");
        });
    </script>
@endsection
