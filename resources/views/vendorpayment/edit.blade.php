@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card card-body">
            <h4 class="font-weight-bold">{{ __('Vendor payment') }}</h4>
            <hr>

            <form method="POST" action="{{ action('VendorPayableController@update', ['id' => $payables->id]) }}">
                @csrf

                <input name="_method" type="hidden" value="PATCH">

                <div class="form-group">
                    <label for="name">{{ __('Expense name') }}</label>

                    <select name="expense_id" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0" required>
                        <option value="{{ $payables->expense_id }}">{{ $payables->expense->name }}</option>
                        @foreach ($allexpenses->where('user_id', auth()->user()->id) as $e)
                            <option value="{{ $e->id }}">{{ $e->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('expense_id'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('expense_id') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="name">{{ __('To vendor') }}</label>

                    <select name="vendor_id" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0" required>
                        <option value="{{ $payables->vendor_id }}">{{ $payables->vendor->name }}</option>
                        @foreach ($allvendors->where('user_id', auth()->user()->id) as $v)
                            <option value="{{ $v->id }}">{{ $v->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('vendor_id'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('vendor_id') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="amount">{{ __('Amount') }}</label>

                    <input id="amount" type="text" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{ $payables->amount }}" required>
                    @if ($errors->has('amount'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('amount') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="name">{{ __('Payment method') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                    <select name="method" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0">
                        <option value="{{ $payables->method }}">{{ $payables->method }}</option>
                        @foreach ($paymentmethods as $m)
                            <option value="{{ $m->name }}">{{ $m->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('method'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('method') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="invoice">{{ __('Invoice') }}<small class="font-italic ml-1">{{ __('(optional)') }}</small></label>

                    <input id="invoice" type="text" class="form-control{{ $errors->has('invoice') ? ' is-invalid' : '' }}" name="invoice" value="{{ $payables->invoice }}">
                    @if ($errors->has('invoice'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('invoice') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
                    <a href="{{ url('/home') }}" class="btn btn-secondary">{{ __('Home') }}</a>
                </div>

            </form>
        </div>
    </div>
@endsection
