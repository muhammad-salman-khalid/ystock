<?php

Auth::routes();

Route::group(['middleware' => ['web']], function () {

    Route::prefix('/admin')->group(function () {
        Route::get('/login', 'AdminLoginController@showLoginForm')->name('admin.login');
        Route::post('/login', 'AdminLoginController@login')->name('admin.login.submit');

        Route::get('/user/{user}/edit', 'AdminController@useredit');
        Route::patch('/user/{user}', 'AdminController@userupdate');
        Route::get('/', 'AdminController@index')->name('admin.dashboard');

        Route::post('/user', 'AdminController@store');
        Route::get('/{admin}/edit', 'AdminController@edit');
        Route::patch('/{admin}', 'AdminController@update');
        Route::delete('/user/{user}', 'AdminController@destroy');
    });

    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('/user', 'UserController');
    Route::resource('/product', 'ProductController');
    Route::get('/product-list', 'ProductController@list');
    Route::resource('/expense', 'ExpenseController');
    Route::get('/Suppliers-list', 'SupplierController@list');
    Route::resource('/supplier', 'SupplierController');
    Route::resource('/customer', 'CustomerController');
    Route::get('/customer-list', 'CustomerController@list');
    Route::resource('/vendor', 'VendorController');
    Route::get('/bulkadd', 'ProductController@import');
    Route::post('savebulk', 'ProductController@savebulk')->name('product.list');
    Route::post('/sale-save', 'SaleController@sale_save');
    Route::get('/bulkcustomer', 'CustomerController@bulkcustomer');
    Route::post('/bulkcustomersave', 'CustomerController@bulkcustomeradd');
    Route::get('/bulksupplier', 'SupplierController@showbulk');
    Route::post('/savebulksupplier', 'SupplierController@addbulk');
    Route::post('/addproduct','ProductController@add');
    Route::post('/addcustomer','CustomerController@add');
    Route::get('/fetchpro_cus','CustomerController@fetchall');
    Route::get('/invoicepreview/{id}','InvoiceController@viewinvoice');
    Route::get('/aging_list','CustomerController@aging_list');
    Route::get('/aging_list_sup','SupplierController@aging_list_sup');
    Route::get('/aging/{id}','AgingController@aging_view');
    Route::get('/aging_sup/{id}','AgingController@aging_view_sup');
    Route::get('/manage_inventory','InventoryController@index');
    Route::post('/purchase-save', 'PurchaseController@purchase_save');
    Route::resource('/purchase', 'PurchaseController');
    Route::resource('/sale', 'SaleController');
    Route::resource('/bill', 'BillController');
    Route::post('/amountadd', 'SaleController@paymentadd');

    Route::post('/catadd', 'CategoryController@index');    
    Route::post('/prodcatadd', 'ProductCategoryController@index');    
    Route::get('/get_product_category', 'ProductController@getproductcategory');    
    Route::get('/get_expense_category','ExpenseController@getexpensecategory');
    Route::post('/amountaddsup', 'PurchaseController@paymentadd');

    Route::resource('/payable', 'PayableController');
    Route::resource('/receivable', 'ReceivableController');
    Route::resource('/vendorpayment', 'VendorPayableController');

    Route::resource('/employee', 'EmployeeController');
    Route::resource('/payroll', 'PayrollController');

    Route::get('/report/vendor-review', 'ReportController@vendorReview');
    Route::get('/report/supplier-review', 'ReportController@supplierReview');
    Route::get('/report/customer-review', 'ReportController@customerReview');
    Route::get('/report/product-report', 'ReportController@productReport');
    Route::get('/report/monthly-report', 'ReportController@monthlyReport');
    Route::get('/report/profit', 'ReportController@profit');
    Route::get('/report', 'ReportController@index');

    Route::any('{query}', function(){ return redirect('/home'); })->where('query', '.*');
});
